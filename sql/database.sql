
--
--
-- O nome do banco de dados deve ser alterado para cada projeto
--
--

-- Database: "serpro.maisacessados"

-- DROP DATABASE "serpro.maisacessados";



CREATE DATABASE "serpro.maisacessados"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       LC_COLLATE = 'pt_BR.UTF-8'
       LC_CTYPE = 'pt_BR.UTF-8'
       CONNECTION LIMIT = -1;