Introdução
============

Esse módulo tem a função de prover funções muito utilizadas durante a
instalação de produtos que utilizem o setuphandlers.py: criação de
conteúdo, transição de workflows, criação de índices, dentre outros. Dê
uma lida nos métodos presentes em serpro.setuphandlersutils.utils para
ver os recursos.

Instalação
==========
Coloque como dependência no seu metadata.xml:

  <dependencies>
    <dependency>profile-serpro.setuphandlersutils:default</dependency>
  </dependencies>

Utilização
==========
Importe a classe:

    from serpro.setuphandlersutils.utils import SetuphandlersUtils

Estando no método "def postInstall(context):", instancie:

    setuphandlers_utils = SetuphandlersUtils(context)
    
E utilize as funções disponíveis. Um exemplo:

    # setPortalProperties já testa se a propriedade existe ou não, se
    # existir não faz nada.
    properties = [{'name':'teste', 'value': 't', 'type': 'boolean'},
                  {'name':'teste', 'value': 'f', 'type': 'string'}]
    setuphandlers_utils.setPortalProperties('teste_properties', properties,  
        'Propriedades de teste')

    root_site = context.getSite()
    pasta_info = {'type_name': 'Folder', 'id': 'teste', 'title': 'Fóruns',  
        'exclude_from_nav': True, 'transition': 'publish'}

    #So cria se ja nao existir.
    setuphandlers_utils.criarConteudo(root_site, pasta_info)
