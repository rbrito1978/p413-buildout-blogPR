# -*- coding: utf-8 -*-
from exceptions import AttributeError
from Acquisition import aq_inner
from zope.app.component.hooks import getSite
from zope.interface import alsoProvides

from Products.ATContentTypes.lib import constraintypes
from Products.CMFPlone.utils import _createObjectByType
from plone.app.workflow.browser.sharing import SharingView
from Products.Archetypes.utils import shasattr # To avoid acquisition
from Products.CMFPlone.Portal import PloneSite
from Products.CMFCore.utils import getToolByName

from serpro.setuphandlersutils.config import PROJECTNAME, GENERIC_SYSTEM_USER

import logging
logger = logging.getLogger('serpro.setuphandlersutils')

class SetuphandlersUtils(object):
    """Classe contendo funções frequentemente utilizadas nos setuphandlers de
    outros produtos."""

    def __init__(self, context):
        self.context = context
        # Essa situação só existe quando o módulo está sendo chamado
        # diretamente por bin/instance run SCRIPT.
        if not isinstance(self.context, PloneSite):
            try:
                self.portal = context.getSite()
            except AttributeError:
                self.portal = getSite()
        else:
            self.portal = self.context

        self.portal_registration = self.portal.portal_registration

    def is_not_profile(self, marker_filename):
        """Retorna se não está sendo aplicado o profile do próprio produto.
        Isso é feito pra evitar uma recursão infinita. Maiores informações:
        http://web.archiveorange.com/archive/v/FPm17F4YCYi2gHULdE1w

        @marker_filename: Colocar o nome completo do arquivo, incluindo
        extensão.

        """
        return self.context.readDataFile(marker_filename) is None

    def instalarDependencias(self, dependencias):
        """Instala as dependencias do produto."""
        quickinstaller = self.portal.portal_quickinstaller
        logger.info('===== INSTALLING DEPENDENCIES (ONLY IF NEEDED)=====')
        for dependency in dependencias:
            # Para nao instalar o proprio serpro.setuphandlersutils.
            if dependency != PROJECTNAME:
                if quickinstaller.getProductVersion(dependency) is None:
                    raise Exception('Dependency %s not found. Cannot continue.' % (dependency))
                if not quickinstaller.isProductInstalled(dependency):
                    logger.info("Installing dependency %s:" % dependency)
                    quickinstaller.installProduct(dependency)

    def criarConteudo(self, local, conteudos, use_generic_user_creator=True, language=""):
        """Cria os conteudos recebidos como parametro.

        @conteudos: [

                     {'id': 'teste', 'title': 'Teste', 'type_name': 'Folder', 'transition': '',
                      'locally_allowed_types': 'Folder', 'exclude_from_nav': True, 'default_page': '',
                      'interface', ''}

                     {'id': 'teste2', 'title': 'Teste2', 'type_name': 'Folder', 'transition': 'publish',
                      'templateId': 'custom_folder_listing',
                      'locally_allowed_types': ['Folder', 'Alias']}]

        A chave 'transition' aceita uma lista pois pode ser que você deseje um
        estado que necessite de várias transições (pode ser que um conteúdo
        necessite passar pro aprovação por exemplo). Assim, ao passar uma lista,
        serão feitas as transições na ordem que estiverem na lista.

        Se passar apenas um conteudo, o único objeto criado será retornado.

        """
        if not isinstance(conteudos, list):
            conteudos = [conteudos]

        path = '/'.join(local.getPhysicalPath())
        pc = self.portal.portal_catalog
        for conteudo in conteudos:
            obj = pc(id=conteudo['id'], path={'query': path, 'depth': 1})
            if not obj:
                exclude_from_nav = conteudo.get('exclude_from_nav', False)

                newObj = _createObjectByType(conteudo['type_name'], local, conteudo['id'],
                    language=language, excludeFromNav=exclude_from_nav)

                newObj.setTitle(conteudo['title'])

                if conteudo.get('transition', ''):
                    if not isinstance(conteudo['transition'], list):
                        conteudo['transition'] = [conteudo['transition']]
                    for transition in conteudo['transition']:
                        self.setTransitionWorkflow(newObj, transition)

                locally_allowed_types = conteudo.get('locally_allowed_types', '')
                if locally_allowed_types and shasattr(newObj, 'canSetConstrainTypes'):
                    if not isinstance(conteudo['locally_allowed_types'], list):
                        conteudo['locally_allowed_types'] = [conteudo['locally_allowed_types']]
                    self.setConstraints(newObj, locally_allowed_types)

                if conteudo.get('interface', ''):
                    if not isinstance(conteudo['interface'], list):
                        interfaces = [conteudo['interface']]
                    else:
                        interfaces = conteudo['interface']
                    for interface in interfaces:
                        alsoProvides(newObj, interface)

                # Pro Plone 3, default_page serve para selecionar uma template
                # como "visão" padrão de uma pasta. Se quiser pro Plone 4,
                # utilize 'templateId'.
                if conteudo.get('default_page', ''):
                    newObj.setDefaultPage(conteudo['default_page'])

                if conteudo.get('templateId', ''):
                    newObj.setLayout(conteudo['templateId'])

                if use_generic_user_creator:
                    newObj.setCreators(GENERIC_SYSTEM_USER)

                try:
                    newObj.unmarkCreationFlag()
                except AttributeError:
                    pass # dexterity objs não possuem o método unmarkCreationFlag
                newObj.reindexObject()

                # Pediu pra criar apenas um objeto, então o retorna.
                if len(conteudos) == 1:
                    return newObj

    def setTransitionWorkflow(self, object, transition):
        """docstring for setTransition"""
        pw = self.portal.portal_workflow
        pw.doActionFor(object, transition)
        object.reindexObjectSecurity()

    def setConstraints(self, container, locally_allowed_types):
        """Seta as constraints do container."""
        container.setConstrainTypesMode(constraintypes.ENABLED)
        container.setLocallyAllowedTypes(locally_allowed_types)
        container.setImmediatelyAddableTypes(locally_allowed_types)

    def excluirInicial(self, excluir=['news', 'events', 'front-page', 'Members']):
        """Exclui alguns conteudos iniciais que um Plone Site possui na raiz
        do site."""
        pathPortal = '/'.join(self.portal.getPhysicalPath())
        pc = self.portal.portal_catalog
        results = pc(id=excluir, path={'query': pathPortal, 'depth': 1})
        idExcluir = [result.id for result in results]
        self.portal.manage_delObjects(idExcluir)

    def toggleShowAllParents(self, toggle=False):
        """Altera a opção 'showAllParents' em navtree_properties: funciona para
        não mostrar o próprio item no menu de navegação, se ele estiver
        configurado para não aparecer. Veja http://dev.plone.org/ticket/8109
        para entender a motivação e o funcionamento."""
        navtree_properties = self.portal.portal_properties.navtree_properties
        navtree_properties.manage_changeProperties(showAllParents=toggle)

    def desabilitarTipos(self, tipos=[]):
        """Desabilita os tipos passados por parâmetro, setando 'Implicity
        addable' como False. Útil para desabilitar tipos padrão do Plone
        que não são utilizados."""
        #ptype: Products.CMFCore.TypesTool.FactoryTypeInformation
        if not isinstance(tipos, list):
            tipos = [tipos]

        for tipo in tipos:
            t = getattr(self.portal.portal_types, tipo)
            if t:
                t.global_allow = False

    def setupTextIndexNG3(self, languages=('en',), metatypes=['ZCTextIndex'],
        needed=('SearchableText',), encoding='', extra={}):

        if not encoding:
            encoding = self.portal.portal_properties.site_properties.default_charset

        catalog = self.portal.portal_catalog
        indexes = catalog.getIndexObjects()
        all_ids = list()
        source_names = dict()
        for idx in indexes:
            if (idx.meta_type in metatypes) and (idx.getId() in needed):
                all_ids.append(idx.getId())
                source_names[idx.getId()] = idx.getIndexSourceNames()

        for id in all_ids:
            catalog.manage_delIndex(id)

            extra = {'languages': languages,
                   'fields' : source_names[id],
                   'default_encoding' : encoding,
                   'splitter_casefolding': extra.get('splitter_casefolding', True),
                   'dedicated_storages': extra.get('dedicated_storages', True),
                   'use_converters': extra.get('use_converters', True),
                   'index_unknown_languages': extra.get('index_unknown_languages', True),
                   'storage': extra.get('storage', 'txng.storages.default'),
                   'ranking': extra.get('ranking', False)
                   }

            catalog.manage_addIndex(id, 'TextIndexNG3', extra=extra)
            catalog.manage_reindexIndex([id, ], self.portal.REQUEST)


    def criarIndexes(self, indexes):

        class Extra:
            pass

        idsIndexes = self.portal.portal_catalog.Indexes.objectIds()

        for index in indexes:
            if index[0] not in idsIndexes:
                extra = None
                if index[1] == 'ZCTextIndex':
                    extra = Extra()
                    extra.lexicon_id = 'plone_lexicon'
                    extra.index_type = 'Okapi BM25 Rank'

                self.portal.portal_catalog.addIndex(index[0], index[1], extra)

    def criarMetadatas(self, metadatas):

        idsMetadatas = self.portal.portal_catalog.schema()

        for metadata in metadatas:
            if metadata not in idsMetadatas:
                self.portal.portal_catalog.addColumn(metadata)

    def gerenciarPortalCatalog(self, indexes, metadatas=[]):
        """ Cria os Indexes e Metadatas do produto.

        @indexes: [('nome', 'tipo'),('nome', 'tipo')]
        """
        self.criarIndexes(indexes)
        if metadatas:
            self.criarMetadatas(metadatas)

    def incluirRegraLocalWorkflow(self, folders, policy_in, policy_below, update_role_mappings=False):
        """Inclui a regra local de workflow nos diretorios passados por
        parametro. É necessário possuir o produto CMFPlacefuLWorkflow
        instalado.
        NOTA: Cuidado se tiver um portal com muitos objetos se for
        utilizar o parâmetro update_role_mappings."""
        #Logica inspirada em
        #https://svn.openplans.org/svn/opencore/tags/0.13.0/opencore/configuration/setuphandlers.py
        pwf_tool = self.portal.portal_placeful_workflow
        #Recebe tanto um objeto como uma lista de objetos.
        if not isinstance(folders, list):
            folders = [folders]

        for folder in folders:
            wf_config = pwf_tool.getWorkflowPolicyConfig(folder)
            if wf_config is None:
                folder.manage_addProduct['CMFPlacefulWorkflow'].manage_addWorkflowPolicyConfig()
                wf_config = pwf_tool.getWorkflowPolicyConfig(folder)
                wf_config.setPolicyIn(policy=policy_in)
                wf_config.setPolicyBelow(policy=policy_below)
                # Cuidado ao pedir essa atualização se possuir muitos objetos.
                if update_role_mappings:
                    self.portal.portal_workflow.updateRoleMappings()

    def atribuirRegrasCompartilhamento(self, folder, grupos_roles, herdar_permissoes=False):
        """Atribui as regras de compartilhamento no diretorio passado como
        parâmetro. A variável grupos_roles tem a estrutura comumente
        utilizada na constante GROUPS presente em config.py.

        @grupos_roles: [
                       ('ID GRUPO', ['ID ROLE', 'ID ROLE']),
                       ('ID GRUPO', ['ID ROLE', 'ID ROLE'])
                       ('ID GRUPO', 'ID ROLE'),
                       ('ID GRUPO', []),
                       ]
        """

        def criaVariavelSettings(grupos_roles):
            """Cria a variavel settings para uso na funcao update_role_settings."""
            settings = []
            for grupo in grupos_roles:
                setting = {'type': 'group', 'id': grupo[0],
                           'roles': list(grupo[1])}
                settings.append(setting)
            return settings

        sharing_view = SharingView(folder, self.portal.REQUEST)

        sharing_view.update_inherit(status=herdar_permissoes)

        #Verificar plone.app.workflow-1.2-py2.4.egg/plone/app/workflow/browser/sharing.py
        #para entender a estrutura da variavel 'settings'.
        settings = criaVariavelSettings(grupos_roles)

        reindex = sharing_view.update_role_settings(settings, reindex=False)
        if reindex:
            aq_inner(folder).reindexObjectSecurity()

        folder.reindexObject()

    def criarGrupos(self, grupos):
        """Cria os grupos do produto e ja atribui as roles dos mesmos.
        Exemplo da variável grupos:
        grupos = [('Administradores',('Administrador',))
                  ('Gestor',('Gestores',)]
        """
        portal_groups = self.portal.portal_groups
        acl_users = self.portal.acl_users
        for grupo in grupos:
            group = portal_groups.getGroupById(grupo[0])
            if not group:
                portal_groups.addGroup(grupo[0], title=grupo[0])
                group = portal_groups.getGroupById(grupo[0])
            for role in grupo[1]:
                acl_users.portal_role_manager.assignRoleToPrincipal(role, grupo[0])

    def cleanDefaultRoles(self, keep=[]):
        """ Remove as Roles criadas pelo Plone exceto as que
            forem especificadas
        """
        pas = self.portal.acl_users
        role_manager = pas.portal_role_manager
        for role in set(['Reviewer', 'Contributor', 'Site Administrator', 'Reader', 'Editor', 'Member']) - set(keep):
            try:
                role_manager.removeRole(role)
            except:
                pass # não achei uma forma mais bonita de testar se a role existe antes de tentar remove-la

    def setPortalProperties(self, property_sheet_name, properties, property_sheet_title=''):
        """Cria uma property sheet ou apenas uma propriedade na tool
        portal_properties. Se aquela propriedade já existir, não faz nada.

        @properties: [{'name':'teste', 'value': True, 'type': 'boolean'},
                      {'name':'teste2', 'value':' novo', 'type': 'string'}]"""

        portal_properties = self.portal.portal_properties

        if getattr(portal_properties, property_sheet_name, None) is None:
            portal_properties.addPropertySheet(property_sheet_name, property_sheet_title)

            property_sheet = getattr(portal_properties, property_sheet_name)

            for property in properties:
                if not property_sheet.hasProperty(property['name']):
                    property_sheet._setProperty(property['name'], property['value'], property['type'])

    def setMetaTypesNotToList(self, types):
        """Adiciona, no campo metaTypesNotToList, os tipos que não devem ser
        listados na navegação. Isso não é feito pelo GS porque senão teria de
        manter uma lista de todos os tipos do portal sempre que fossem
        adicionados e pra evitar que eles fossem sobrescritos de alguma forma."""
        if not isinstance(types, list):
            types = [types]

        navtree_properties = self.portal.portal_properties.navtree_properties
        metaTypesNotToList = list(navtree_properties.getProperty('metaTypesNotToList'))
        for f in types:
            if f not in metaTypesNotToList:
                metaTypesNotToList.append(f)
        navtree_properties.manage_changeProperties(metaTypesNotToList=metaTypesNotToList)

    def setTypesNotSearched(self, types):
        """Adiciona, no campo types_not_searched, os tipos presentes em types.
        Isso não é feito pelo propertiestool.xml porque senão teria de manter
        uma lista de todos os tipos do portal sempre que fossem adicionados e
        pra evitar que eles fossem sobrescritos de alguma forma."""
        if not isinstance(types, list):
            types = [types]

        site_properties = self.portal.portal_properties.site_properties
        typesNotSearched = list(site_properties.getProperty('types_not_searched'))
        for f in types:
            if f not in typesNotSearched:
                typesNotSearched.append(f)
        site_properties.manage_changeProperties(types_not_searched=typesNotSearched)

    def limpaCacheCssJs(self, css=True, js=True):
        """Limpa o cache dos registros de css e de js"""
        if css:
            self.portal.portal_javascripts.cookResources()
        if js:
            self.portal.portal_css.cookResources()

    def removerActionsContentType(self, portal_type, action_id):
        """Remove a action do content-type passada por parâmetro. portal_type é o id
        do portal_type na tool."""
        #ptype: Products.CMFCore.TypesTool.FactoryTypeInformation
        for ptype in self.portal.portal_types.objectValues():
            if ptype.id == portal_type:
                idxs = [idx_act[0] for idx_act in enumerate(ptype.listActions()) if idx_act[1].id == action_id]
                if idxs:
                    ptype.deleteActions(idxs)
                break

    def removerCamposFormularioFCKEditor(self):
        """Remove todos os itens relativos a criação de formulários em HTML,
        como Form, Checkbox, Radio, etc."""
        portal_properties = self.portal.portal_properties
        try:
            fckeditor_properties = getattr(portal_properties, 'fckeditor_properties')
            fckeditor_properties.manage_changeProperties(
                fck_custom_toolbar="""[
             ['Source','DocProps','-','Save','Preview','-','Templates'],
             ['Cut','Copy','Paste','PasteText','PasteWord','-','Print','rtSpellCheck'],
             ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
             ['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
             ['OrderedList','UnorderedList','-','Outdent','Indent'],
             ['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
             ['Link','Unlink','Anchor'],
             ['Image','Flash','flvPlayer','Table','Rule','SpecialChar','UniversalKey','PageBreak','Smiley'],
             '/',
             ['Style','FontFormat','FontName','FontSize'],
             ['TextColor','BGColor'],
             ['FitWindow','-','About']
            ]"""
            )
        except AttributeError:
            """A versão FCKEditor utilizada não possui um profile, e geralmente
            o FCKEditor não é declarado como uma dependência do produto, por isso
            o try: except: pass caso não esteja instalado."""
            pass

    def setaPadraoLogin(self, padrao):
        """Seta um novo padrão para o login de usuários se o atual padrão
        for o default de Plone.
        Padrão default: ^\w[\w\.\-@]+\w$

        A FUNÇÃO manage_editIDPattern UTILIZADA NESSA FUNÇÃO, ALTERA TAMBÉM O
        PADRÃO DE NOME DE GRUPOS. UTILIZE ESSA FUNÇÃO COM CUIDADO. TESTE A
        INCLUSÃO DE GRUPOS.
        """
        padrao_atual = self.portal_registration.getIDPattern()
        if not padrao_atual:
            self.portal_registration.manage_editIDPattern(padrao)

    def removePropriedades(self, tool, props):
        """Remove as propriedades props da tool.
        props deve ser uma lista com os ids das propriedades.
        """
        if hasattr(tool, 'manage_delProperties'):
            props_adeletar = [ prop for prop in props
                              if tool.hasProperty(prop)]
            tool.manage_delProperties(props_adeletar)

    def removePropsGrupos(self):
        """Deixa os grupos somemente com nome e descrição removendo as outras
        propriedades

        Warning: be aware that removing 'title' without re-adding it might be dangerous.
        """
        self.removePropriedades(self.portal.portal_groupdata,
                                ['email'])

    def setVersionedTypes(self, types_to_version):
        """Seta os tipos de conteúdo que devem ser versionados. Código obtido de
        http://plone.org/documentation/manual/developer-manual/archetypes/referencemanual-all-pages
        Não se esqueça de editar o diff_tool.xml no seu profile.
        """
        try:
            portal_repository = self.portal.portal_repository
            versionable_types = list(portal_repository.getVersionableContentTypes())
            from Products.CMFEditions.setuphandlers import DEFAULT_POLICIES
            for type_id in types_to_version:
                if type_id not in versionable_types:
                    # use append() to make sure we don't overwrite any
                    # content-types which may already be under version control
                    versionable_types.append(type_id)
                    # Add default versioning policies to the versioned type
                    for policy_id in DEFAULT_POLICIES:
                        portal_repository.addPolicyForContentType(type_id, policy_id)
            portal_repository.setVersionableContentTypes(versionable_types)
        # em plone 4.1 nao eh possivel importar default_policies da forma acima.
        # a buscar solucao.
        except:
            pass


    def naoRemoverUtilitiesNoReinstall(self, reinstall, cascade):
        """Remove utilities e adapters do cascade para não remove-los ao
        reinstalar um produto. Ver descrição do problema em:

        https://wiki.serpro/projetos/plone/documentacao/erros/
        ComponentLookupErrorDepoisDeTentarUsarUmaUtilityQueEraDependenciaDe
        OutroProduto/?searchterm=ComponentLookupError

        Para ulilizar esse método crie o arquivo Extensions/install.py no
        produto.

        Nele crie o método beforeUninstall com o código:


       from serpro.setuphandlersutils.utils import SetuphandlersUtils


       def beforeUninstall(portal, reinstall, product, cascade):

           setuphandlers_utils = SetuphandlersUtils(portal)
           return setuphandlers_utils.naoRemoverUtilitiesNoReinstall(reinstall,
                                                                     cascade)
        """
        if reinstall:
            return '', filter(lambda c: c not in ('utilities', 'adapters'),
                              cascade)
        else:
            return '', cascade

    def setupRegistration(self, custom_id_pattern=''):
        """altera a expressao regular utilizada pra login: id_pattern

        A FUNÇÃO manage_editIDPattern UTILIZADA NESSA FUNÇÃO, ALTERA TAMBÉM O
        PADRÃO DE NOME DE GRUPOS. UTILIZE ESSA FUNÇÃO COM CUIDADO. TESTE A
        INCLUSÃO DE GRUPOS.
        """
        member_id_pattern = self.portal_registration.getIDPattern()
        if (member_id_pattern != custom_id_pattern) and (custom_id_pattern != ''):
            self.portal_registration.manage_editIDPattern(custom_id_pattern)

    def configuraPortalCalendar(self, portal_types, use_session=False, show_states=None, firstweekday=6):
        """Altera as configurações do portal calendar
           portal_types deve ser uma tupla com os tipos de conteudo a serem marcados no portlet de calendario
        """
        self.portal.portal_calendar.edit_configuration(portal_types, use_session, show_states, firstweekday)

    def setWorkflowByPass(self, site, workflowTitle):
        """ código python para marcar o checkbox "Manager role bypasses guards"
            já que pelo xml não funciona.
        """
        wft = getToolByName(site, 'portal_workflow')
        sw = getattr(wft, workflowTitle, None)
        if sw is not None:
            sw.setProperties(title=workflowTitle, manager_bypass=1)


    def setupRSS(self, site, id, title, limitNumber, itemCount, customViewFields, criterion_type, criterion_state, criterion_sort, transitions):
        """ Esta função cria um portal_type do tipo Topic na raiz do site.
            Diferente do objeto "Plone Site", o Topic tem RSS habilitado nativo, além de ser possível
            criar os filtros necessários para atender os requisitos do RSS.
            O import foi feito dentro da função para que a dependência do produto serpro.protegido seja
            apenas para quando for utilizar esta função.
        """
        from serpro.protegido.interfaces import IProtegido

        portal_workflow = getToolByName(site, 'portal_workflow')
        if id not in site.contentIds():
            site.invokeFactory('Topic', id=id, title=title, limitNumber=limitNumber, itemCount=itemCount, customViewFields=customViewFields, language='pt-br', excludeFromNav=True)
            newObj = getattr(site, id, None)
            if newObj is not None:
                criterion = newObj.addCriterion('portal_type', 'ATPortalTypeCriterion')
                criterion.setValue(criterion_type)
                criterion = newObj.addCriterion('review_state', 'ATSimpleStringCriterion')
                criterion.setValue(criterion_state)
                newObj.setSortCriterion(criterion_sort, reversed=True)
                for transition in transitions:
                    portal_workflow.doActionFor(newObj, transition)
                alsoProvides(newObj, IProtegido)

