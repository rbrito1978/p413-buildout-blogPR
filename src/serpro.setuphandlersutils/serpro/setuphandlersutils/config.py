# -*- coding: utf-8 -*-

"""Common configuration constants
"""

PROJECTNAME = 'serpro.setuphandlersutils'

# ID utilizado para CREATOR de itens criados pelo sistema.
GENERIC_SYSTEM_USER = 'sistema'
