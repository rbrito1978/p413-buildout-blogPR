# -*- coding: utf-8 -*-
"""Definition of the Banner content type
"""

from zope.interface import implements, directlyProvides


from Products.Archetypes.atapi import *
    
from Products.ATContentTypes.content import schemata
from Products.ATContentTypes.content.image import ATImage
from Products.ATContentTypes.content.image import ATImageSchema
from Products.ATContentTypes.interface.image import IATImage

from serpro.banners.interfaces import IBanner
from serpro.banners.config import PROJECTNAME

from Products.validation import V_REQUIRED

from Products.ATContentTypes.configuration import zconf
from Products.Archetypes.atapi import StringField
from Products.Archetypes.atapi import StringWidget

from AccessControl import ClassSecurityInfo
from Products.CMFCore.permissions import View
from Products.CMFCore.permissions import ModifyPortalContent
from Products.ATContentTypes import ATCTMessageFactory as _

import urlparse
from urllib import quote

from serpro.banners import bannersMessageFactory

BannerSchema = ATImageSchema.copy() + Schema((
   
   # Devido principalmente ao mantis: https://mantis.serpro.gov.br/view.php?id=111520
   # que questiona a existencia do campo imageCapition e a nao obrigatoriedade do campo remoteUrl
   # alem de discussoes internas no setor sobre o mesmo assunto, o campo imageCaption passa a 
   # ficar invisivel, evitando assim corromper o zodb em sites que ja possuem o produto serpro.banners instalado.
   # O campo remoteUrl passa a ser required e ter valor padrao "http://" seguindo funcionamento do tipo Link
   
   StringField('imageCaption',
        required = False,
        searchable = True,
        widget = StringWidget(
            description = '',
            label = _(u'label_image_caption', default=u'Image Caption'),
            size = 40,
            visible={"view" : "invisible", "edit" : "invisible"},
        )
        ),

    StringField('remoteUrl',
            required=True,
            searchable=True,
            primary=False,
            default = "http://",
            validators = (),
            widget = StringWidget(
                label = bannersMessageFactory(u'label_url', default=u'URL'),
                description = bannersMessageFactory(u'description_url', default=u'Enter an absolute link (http://, https://) or relative link (./, ../)'),
                )
                ),

    ))


BannerSchema['title'].required=True
#BannerSchema.moveField('imageCaption', after='image')

# Set storage on fields copied from ATContentTypeSchema, making sure
# they work well with the python bridge properties.


schemata.finalizeATCTSchema(BannerSchema, moveDiscussion=False)

class Banner(ATImage):
    """Description of the Example Type"""
    implements(IATImage,IBanner)

    meta_type = "Banner"
    schema = BannerSchema
    
    # -*- Your ATSchema to Python Property Bridges Here ... -*-
    security = ClassSecurityInfo()
    
    security.declareProtected(ModifyPortalContent, 'setRemoteUrl')
    def setRemoteUrl(self, value, **kwargs):
        """remute url mutator
        """
        if value:
            value = urlparse.urlunparse(urlparse.urlparse(value))
        self.getField('remoteUrl').set(self, value, **kwargs)

    security.declareProtected(View, 'remote_url')
    def remote_url(self):
        """CMF compatibility method
        """
        return self.getRemoteUrl()

    security.declarePrivate('cmf_edit')
    def cmf_edit(self, remote_url=None, **kwargs):
        if not remote_url:
            remote_url = kwargs.get('remote_url', None)
        self.update(remoteUrl = remote_url, **kwargs)

    security.declareProtected(View, 'getRemoteUrl')
    def getRemoteUrl(self):
        """Sanitize output
        """
        value = self.Schema()['remoteUrl'].get(self)
        if not value: value = '' # ensure we have a string
        return quote(value, safe='?$#@/:=+;$,&%')


registerType(Banner, PROJECTNAME)
