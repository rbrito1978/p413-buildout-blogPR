"""Common configuration constants
"""

PROJECTNAME = 'serpro.banners'

ADD_PERMISSIONS = {
    # -*- extra stuff goes here -*-
    'Banner': 'serpro.banners: Add Banner',
}

IMAGE_EXTENSIONS = [{'extension': 'jpeg', 'mime-type':'image/jpeg'},
                    {'extension': 'jpg', 'mime-type':'image/jpeg'},
                    {'extension': 'gif', 'mime-type':'image/gif'},
                    {'extension': 'png', 'mime-type':'image/png'}]
