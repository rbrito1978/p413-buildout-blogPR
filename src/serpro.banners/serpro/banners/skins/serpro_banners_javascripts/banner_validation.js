/* O campo de url terá adicionado 'http' se for um link externo e comecar com
 * 'www.'
 */
jq(document).ready(function(){

    function common_domain(full_domain) {
        //Verifica se um domínio possui prefixos e sufixos comuns.
        var common_domains = ['www.', '.com', '.net', '.com.br', '.net.br',  
            '.gov', '.gov.br'];

        for (var i = 0; i < common_domains.length; i++){
            if(full_domain.indexOf(common_domains[i]) != -1){
                return true;
            }
        }
        return false;
    }

    var edit = jq('form[name="edit_form"]');
    
    //Se for do tipo link.
    if ((edit.attr("id") == 'banner-base-edit')){

        //Se for undefined, não está num form de edição.
        if(edit.attr("name") == 'edit_form'){

            jq('input[name="form.button.save"]').click(function(){

                var remoteUrl = jq('#remoteUrl').val();
                var is_url = remoteUrl !== "";

                //Verifica se o campo url está preenchido.
                if (is_url){
                    if(common_domain(remoteUrl)){
                        if(remoteUrl.indexOf('http') == -1){
                            jq('#remoteUrl').attr("value", "http://" + remoteUrl);
                        }
                    }
                }

                return true;

            });

        }

    }
});
