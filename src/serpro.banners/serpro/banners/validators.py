#-*- coding: utf-8 -*-
# validation workarounds here

from Products.validation import V_REQUIRED, validation

from serpro.validators.file import ExtensionValidator

from zope.component import adapts

# Importe suas interfaces: podem ser do próprio sistema como
# from Products.ATContentTypes.interface.ATINTERFACE import IATINTERFACE
# ou do seu produto:
from serpro.banners.interfaces.banner import IBanner
from serpro.banners import config
from serpro.banners.browser.interfaces import ISerproBanners

# Classe pronta para adicionar validadores dinâmicos.
from serpro.validators.schema import ValidatorsModifier, RequiredModifier

# Essa classe aparece apenas uma vez.
# ATENÇÂO: As classes de validadores são chamadas TRÊS VEZES, portanto cuidado
# com a implementação inserida nessa utilização do archetypes.schemaextender.
class ValidatorsModifierSerproBanners(ValidatorsModifier):
    layer = ISerproBanners


# Essa classe aparece de novo para CADA INTERFACE que você deseja adaptar.
# ATENÇÃO: adapts(Interface1, Interface2) não funciona nessa estrutura!
# se necessitar dessa estrutura, veja "PersonBioForm" em
# http://garbas.github.com/plone-z3c.form-tutorial/plone_z3cform.html
# e crie sua própria classe ValidatorsModifier.
# Se quiser adaptar a todos os content-types, utilize a interface
# Products.CMFCore.interfaces.IContentish
class ValidatorsBannerModifier(ValidatorsModifierSerproBanners):
    """Define a interface do tipo que terá validador(es) adicionado(s) a field(s).

    Através do uso de adapters, conterá a definição de qual interface será
    adaptada e qual(is) validador(es) em determinado(s) field(s).

    'field_validators' é um dicionário, com a chave sendo o id do field e o
    valor sendo OBRIGATORIAMENTE uma tupla com outras tuplas, contendo os
    validadores e o modo (V_REQUIRED ou V_SUFFICIENT).

    Se essa estrutura não for obedecida, será lançada uma exceção.
    """
    adapts(IBanner)
    field_validators = {'image': (('isValidBannerExtension', V_REQUIRED),),
                        'remoteUrl': (('isURL', V_REQUIRED),)}

validation.register(ExtensionValidator('isValidBannerExtension',
    extensions=config.IMAGE_EXTENSIONS))

