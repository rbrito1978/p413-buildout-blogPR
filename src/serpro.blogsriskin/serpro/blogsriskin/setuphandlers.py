# -*- coding: utf-8 -*-
from serpro.blogsriskin.config import *

def instalarDependencias(portal):
    """Instala as dependencias do produto."""
    quickinstaller = portal.portal_quickinstaller
    for dependency in DEPENDENCIES:
        if not quickinstaller.isProductInstalled(dependency):
            print "Installing dependency %s:" % dependency
            quickinstaller.installProduct(dependency)
        else:            
            if dependency in DEPENDENCIES_REINSTALL:
                print "Reinstalling dependency %s:" % dependency
                quickinstaller.reinstallProducts(dependency)
    

def setupVarious(context):

    # Ordinarily, GenericSetup handlers check for the existence of XML files.
    # Here, we are not parsing an XML file, but we use this text file as a
    # flag to check that we actually meant for this import step to be run.
    # The file is found in profiles/default.

    if context.readDataFile('serpro.blogsriskin_various.txt') is None:
        return

    # Add additional setup code here
    
    portal = context.getSite()
    
    # instalando produtos dependencia
    instalarDependencias(portal)
    
    # cria blogsri_base_properties    
    CSSManager_css = getattr(portal,'CSSManager_css',None)
    if CSSManager_css:
        mapping = [m[0] for m in CSSManager_css.getmapping()]
        if 'blogsri_base_properties' not in mapping:
            CSSManager_css.manage_addToMapping(key='blogsri_base_properties',val='blogsri_base_properties')
