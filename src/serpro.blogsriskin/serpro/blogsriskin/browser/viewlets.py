from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.app.layout.viewlets.common import ViewletBase
from serpro.barraacessibilidade.viewlets.serpro_barraacessibilidade import BarraAcessibilidade
from plone.app.layout.viewlets.common import LogoViewlet

class BarraAcessibilidade(BarraAcessibilidade):
    index = ViewPageTemplateFile('serpro_barraacessibilidade.pt')

class LogoViewlet(LogoViewlet):
    render = ViewPageTemplateFile('logo.pt')
