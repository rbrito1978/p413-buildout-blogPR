(function($) {
  $(document).ready(function() {
    var pagina_url = window.location.pathname;


    //alteracao de texto
    $('#barraacessibilidade-apply_contrast a,#barraacessibilidade-remove_contrast a').text('Contraste');

    //criar, remover, alterar classes
    $('#portal-column-two .portletWrapper:odd').addClass('odd');
    $('.LSBox .hiddenStructure').removeClass('hiddenStructure');
    $('dl.conteudo li:last').addClass('last');
    $('.post:first').addClass('firstPost');
    $('#content').addClass('margens');

    	
	//resolver problema de borda arredondada no ie8
	$('#mini_banners_list').corner("left 7px");
	$('#portal-searchbox').corner("right 7px");
	$('#portal-globalnav').corner("7px");
	$('#portal-columns').corner("7px");
	$('.portlet').corner("7px");
	$('#portal-footer').corner("7px");
	
	//alteracao de atributos
    $('.searchButton').attr({value:'Ok'});

  });
})(jQuery);
