# -*- coding: utf-8 -*-

"""Common configuration constants
"""

PROJECTNAME = 'serpro.curtir'

"""
'id':'':
{
 'title':'',
 'url':'',
 'icon':''
 }
"""

BOOKMARKS = { 'delicious':
             {
             'title':'Delicious',
             'url':'http://del.icio.us/post?title={title}&url={url}',
             'icon':'icoDelicious.gif',
             },

             'google-bookmarks':
             {
             'title':'Google Bookmarks',
             'url':'http://google.com/bookmarks/mark?op=edit&bkmk={url}&title={title}',
             'icon':'icoGoogle-bookmarks.gif'
             },

             'twitter':
             {
             'title':'Twitter',
             'url':'http://twitter.com/intent/tweet?source=sharethiscom&text={title}&url={url}',
             'icon':'btn_tweet.png'
             },

        # O facebook está utilizando um novo botão 'Curtir'. O botão
        # compartilhar que era utilizado aqui está deprecated.
        # Documentação do novo botão:
        # https://developers.connect.facebook.com/docs/reference/plugins/like/
             'facebook':
             {
             'title':'Facebook',
             'url':'https://www.facebook.com/sharer.php?u={url}',
             'icon':'btn_face.png'
             },

             'digg':
             {
             'title':'Digg',
             'url':'http://digg.com/submit?phase=2&url={url}',
             'icon':'icoDigg.gif'
             },

            'myspace':
            {
             'title':'Myspace',
             'url':'http://www.myspace.com/index.cfm?fuseaction=postto&t={title}&c=&u={url}&l=5',
             'icon':'icoMyspace.gif'
             },

            'microsoft':
            {
             'title':'Microsoft',
             'url':'http://favorites.live.com/quickadd.aspx?marklet=1&mkt=en-us&url={url}&title={title}&top=1',
             'icon':'icoMicrosoft.gif'
             },

            'linkedin':
            {
             'title':'Linkedin',
             'url':'http://www.linkedin.com/shareArticle?mini=true&url={url}&title={title}&source={portal}&summary={description}',
             'icon':'btn_ink.png'
             },

            'technorati':
            {
             'title':'Technorati',
             'url':'http://www.technorati.com/search/{url}',
             'icon':'icoTechnorati.gif'
             },

            'google-plus':
             {
             'title':'Google +',
             'url':'https://m.google.com/app/plus/x/?v=compose&content={title}%20{url}',
             'icon':'btn_gplus.png'
             },

            }

