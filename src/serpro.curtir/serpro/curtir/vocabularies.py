from zope.interface import implements

try:
    from zope.schema.interfaces import IVocabularyFactory
except ImportError:
    from zope.app.schema.vocabulary import IVocabularyFactory

from zope.schema.vocabulary import SimpleVocabulary
from zope.schema.vocabulary import SimpleTerm
from serpro.curtir.config import BOOKMARKS

class SerproCurtirProvidersVocabulary(object):
    """
    """
    implements(IVocabularyFactory)

    def __call__(self, context):
        context = getattr(context, 'context', context)
        itens = [ SimpleTerm(key, key, key) for key in BOOKMARKS.keys()]
        return SimpleVocabulary(itens)

SerproCurtirProvidersVocabularyFactory = SerproCurtirProvidersVocabulary()