#################
serpro.curtir
#################

DE6SW

Contribuidores
##############

Glauter Vilela <glauter.vilela@serpro.gov.br>

Dependências
############

Plone 3.3.5+


Objetivo
########

O produto "serpro.curtir" foi desenvolvido a partir do projeto "37148pempreendedor" com o objetivo de criar
uma lista de links para compartilhamento de informação em redes sociais que pudesse ser reaproveitável
em projetos futuros.

Funcionamento
#############

Viewlet
=======

O produto insere uma viewlet com links de compartilhamento nas redes sociais selecionadas (bookmarks)
nos tipos de conteúdo selecionados (Notícia, Evento, Página).
Por exemplo os links Twitter e Google + no tipo de conteúdo News Item.

Configuração
============

Toda a configuração pode ser feita através do painel de controle "plone_control_panel" via configlet
SERPRO Curtir (Bookmarks) "@@serprocurtir_configlet".

Armazenamento da configuração
=============================

A configuração (bookmarks_id e portal_types_id) fica gravada no portal_properties em:

  /portal_properties/serpro_curtir_properties

Lista de bookmarks disponíveis
==============================

A primeira versão do produto contemplou os seguintes bookmarks:
Delicious, Google Bookmarks, Twitter, Facebook, Digg, Myspace, Microsoft, Linkedin, Technorati, Google

Os bookmarks disponíveis encontram-se no "config.py" na raiz do produto sob forma de dicionário:

BOOKMARKS ={
             'id':'':
                {
                 'title':'',
                 'url':'',
                 'icon':''
                 },
           }

Novos bookmarks serão inseridos aí.

Parâmetros na url de compartilhamento
=====================================

A url de compartilhamento pode receber parâmetros padronizados que serão subistituídos 
na renderização da viewlet.
Ex.:
    'twitter':
             {
             'title':'Twitter',
             'url':'http://twitter.com/intent/tweet?source=sharethiscom&text={title}&url={url}',
             'icon':'icoTwitter.gif'
             },

São suportados os seguintes parâmetros:

  {url} - url do objeto (Notícia, Evento, Página)
  
  {title} - título do objeto (Notícia, Evento, Página)
  
  {description} - descrição do objeto (Notícia, Evento, Página)
  
  {portal} - título do site