# -*- coding: utf-8 -*-

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getMultiAdapter
from plone.app.layout.viewlets.common import ViewletBase
from serpro.curtir.config import BOOKMARKS


class CurtirViewlet(ViewletBase):
    index = ViewPageTemplateFile('curtir.pt')

    def getBookmarks(self, itens=[]):
        """
        Retorna os bookmarks selecionados
        """
        context = self.context
        request = self.request

        plone_tools = getMultiAdapter((context, request), name='plone_tools')

        portal_properties = plone_tools.properties()

        if not hasattr(portal_properties, 'serpro_curtir_properties'):
            return []

        portal_state = getMultiAdapter((context, request), name='plone_portal_state')
        context_state = getMultiAdapter((context, request), name='plone_context_state')

        portal_title = portal_state.portal_title()
        portal_url = portal_state.portal_url()

        object_title = context_state.object_title()
        object_url = context_state.object_url()
        description = context.description


        bookmarks_id = portal_properties.serpro_curtir_properties.bookmarks_id

        bookmarks = []
        for b_id in bookmarks_id:
            if BOOKMARKS.has_key(b_id):
                bookmark = BOOKMARKS[b_id]
                bookmarks.append({'id': b_id,
                                  'title':bookmark['title'],
                                  'url':bookmark['url'].replace('{url}', object_url)\
                                                       .replace('{title}', object_title)\
                                                       .replace('{portal}', portal_title)\
                                                       .replace('{description}', description),
                                  'icon':'%s/%s' % (portal_url, bookmark['icon'])
                                  })
        return bookmarks

    def getPortalTypes(self):
        """Retorna lista de portal_types selecionados
        """
        context = self.context
        request = self.request

        plone_tools = getMultiAdapter((context, request), name='plone_tools')

        portal_properties = plone_tools.properties()

        if not hasattr(portal_properties, 'serpro_curtir_properties'):
            return []

        portal_types = portal_properties.serpro_curtir_properties.portal_types_id

        return portal_types
