# -*- coding: utf-8 -*-

from zope.schema import Tuple
from zope.schema import Choice
from zope.component import adapts
from zope.interface import Interface
from zope.interface import implements

from Products.CMFPlone.interfaces import IPloneSiteRoot
from Products.CMFPlone.utils import getToolByName
from Products.CMFDefault.formlib.schema import ProxyFieldProperty
from Products.CMFDefault.formlib.schema import SchemaAdapterBase

from zope.formlib.form import FormFields
from plone.app.controlpanel.form import ControlPanelForm
from zope.app.form.browser.itemswidgets import MultiSelectWidget as BaseMultiSelectWidget
from zope.app.form.browser.itemswidgets import OrderedMultiSelectWidget as BaseOrderedMultiSelectWidget

from serpro.curtir import _


class MultiSelectWidget(BaseMultiSelectWidget):
    
    def __init__(self, field, request):
        """Initialize the widget.
        """
        super(MultiSelectWidget, self).__init__(field,
            field.value_type.vocabulary, request)


class OrderedMultiSelectWidget(BaseOrderedMultiSelectWidget):
    
    def __init__(self, field, request):
        """Initialize the widget.
        """
        super(OrderedMultiSelectWidget, self).__init__(field,
            field.value_type.vocabulary, request)


class IProvidersSchema(Interface):

    bookmarks_id = Tuple(
        title=_(u'Bookmarks Id'),
        description=_(u'help_selected_bookmarks_id',
            default=u"Selecione os Ids dos bookmarks que deseja utilizar.",
        ),
        value_type=Choice(vocabulary="plone.app.vocabularies.SerproCurtirProviders")
    )
    
    portal_types_id = Tuple(
        title=_(u'Portal Types'),
        description=_(u'help_portal_types',
            default=u"Selecione os portal types onde a viewlet deve aparecer.",
        ),
        value_type=Choice(vocabulary="plone.app.vocabularies.ReallyUserFriendlyTypes")
    )
    


class ProvidersControlPanelAdapter(SchemaAdapterBase):
    
    adapts(IPloneSiteRoot)
    implements(IProvidersSchema)

    def __init__(self, context):
        super(ProvidersControlPanelAdapter, self).__init__(context)
        portal_properties = getToolByName(context, 'portal_properties')
        self.context = portal_properties.serpro_curtir_properties

    bookmarks_id= ProxyFieldProperty(IProvidersSchema['bookmarks_id'])
    portal_types_id = ProxyFieldProperty(IProvidersSchema['portal_types_id'])
   
    

class ProvidersControlPanel(ControlPanelForm):
    
    form_fields = FormFields(IProvidersSchema)
    form_fields['bookmarks_id'].custom_widget = OrderedMultiSelectWidget
    form_fields['portal_types_id'].custom_widget = MultiSelectWidget


    label = _('SERPRO Curtir (Bookmarks)')
    description = _('Selecao de bookmarks disponiveis para os portal types.')
    form_name = _('Providers')
