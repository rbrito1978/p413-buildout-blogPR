Introduction
============

Instalar pacote dependência
***************************

libpqxx-dev (Dependência psycopg2)

sudo apt-get install libpqxx-dev





Configuração do buildout.cfg da instância
*****************************************


Deve ser feita a seguinte configuração no "buildout.cfg" pois o produto "ZPsycopgDA" deve ser incluído na pasta "products" da instância.


parts =
    ...
    extra
    zpsycopgda


############################################
# psycopg2 ZPyscopgDA

[extra]
recipe = plone.recipe.distros
urls =
    http://pypi.python.org/packages/source/p/psycopg2/psycopg2-2.4.5.tar.gz
version-suffix-packages =
     psycopg2-2.4.5.tar.gz

[zpsycopgda]
recipe = plone.recipe.command
command = [ ! -d ${buildout:directory}/products/ZPsycopgDA ] && cp -pR ${extra:location}/psycopg2/ZPsycopgDA ${buildout:directory}/products

############################################





Criação do Banco de dados no Postgresql
***************************************

Criar banco de dados no Postgresql e executar os dois scripts sql (acessos.sql e views.sql) 
contidos na pasta "sql" localizada na raiz do produto.

Para criação do banco de dados também há um script modelo chamado database.sql.
Cabe ressaltar que o banco deve ser configurado com utf-8 para evitar problemas de encode com o Zope.

Criar grupo e definir permissões:

- Select e Insert na tabela "acessos"

- All na sequência "acessos_idacesso_seq"

- Select nas visões "acessos_dia", "acessos_semana", "acessos_mes" e "acessos_ano".


Normalmente no Postgresql você primeiramente cria um grupo e define as permissões nas tabelas/visões/sequências.
Depois cria um usuário e inclui este usuário no grupo criado no passo anterior.
Este usuário/senha será usado na string de conexão com o banco de dados.





String de conexão com o banco de dados
**************************************

Após instalar o produto serpro.maisacessados no Plone Site, completar a string de conexão com
o banco de dados contida nas propriedades do objeto "Psycopg2_database_connection" localizado
na raiz do site.

A conexão com o banco de dados possui o seguinte formato:

dbname=bancox host=localhost user=userx password=passx

Depois de completar a string de conexão clicar em "Open Connection".




Configuração do produto serpro.maisacessados
********************************************

A configuração do produto pode ser feita através do "control_panel".

Poderão ser indicados tipos de conteúdo e estados que deverão ter a visualização contada.
Poderá também incluir ids de objetos que não devem ser contatos.
Por fim a contagem como um todo pode ser ativada ou desativada.




Utilizando o portlet Mais Acessados
***********************************

O portlet mais acessados tem o objetivo de listar o titulo com link e quantidade de acessos
dos N objetos mais acessados no site.

Podem ser definidos:

- O título do portlet.
  Por padrão é definido o valor "Most accessed" que é traduzido automaticamente para pt-br ou es.
  
- Os tipos de conteúdo que entrarão na listagem.

- A quantidade de itens que devem ser exibidos no portlet.

- Exibir ou não a quantidade de acessos que teve cada item.