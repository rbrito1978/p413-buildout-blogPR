from setuptools import setup, find_packages
import os

version = '0.3.1'

setup(name='serpro.maisacessados',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Framework :: Plone :: 4.1"
        "Programming Language :: Python",
        ],
      keywords='SERPRO MAIS ACESSADOS',
      author='DE614',
      author_email='',
      url='',
      license='SERPRO',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'psycopg2==2.4.5',
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      paster_plugins=["ZopeSkel"],
      )
