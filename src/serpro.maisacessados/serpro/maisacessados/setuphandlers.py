# -*- coding: utf-8 -*-
from zope.component import getUtility, getMultiAdapter
from Products.CMFCore.utils import getToolByName


import logging
logger = logging.getLogger('serpro.maisacessados: setuphandlers')



def createZpsycopg2DatabaseConection(site):
    """Cria objeto do tipo Z Psycopg 2 Database Connection na raiz do site
       Para cada projeto o a connection_string deverá ser completada.
    """
    if not hasattr(site,'Psycopg2_database_connection'):
        site.manage_addProduct['ZPsycopgDA'].manage_addZPsycopgConnection(
                  id='Psycopg2_database_connection', 
                  title='Z Psycopg 2 Database Connection',
                  connection_string='dbname= host= user= password=',           
                  check = 0, # Connect immediately 
                  zdatetime = 1, # Use Zope's internal DateTime
                  tilevel = 2, # Transaction isolation level: Serializable 
                  encoding = 'utf-8')



def createInsertAcesso(site):
    """Cria objeto do tipo ZSQLMethod na raiz do site.
       Esse objeto será o responsável pelo insert na tabela 'acessos'
    """
    if not hasattr(site,'insert_acesso'):
        site.manage_addProduct['ZSQLMethods'].manage_addZSQLMethod(
              id='insert_acesso',
              title='insert acesso',
              connection_id='Psycopg2_database_connection',
              arguments='uidObjeto pathObjeto tipoObjeto idiomaObjeto',
              template="""INSERT INTO acessos("uidobjeto","pathobjeto","tipoobjeto","idiomaobjeto") 
                          VALUES (<dtml-sqlvar uidObjeto type="string">,
                                  <dtml-sqlvar pathObjeto type="string">,
                                  <dtml-sqlvar tipoObjeto type="string">, 
                                  <dtml-sqlvar idiomaObjeto type="string">
                                 );
                      """
              )
        site.insert_acesso.manage_advanced(max_rows=1, max_cache=100, cache_time=0, class_name="", class_file="")



def createSelectAcessosDia(site):
    """Cria objeto do tipo ZSQLMethod na raiz do site.
       Esse objeto será o responsável pelo select na view 'acessos_dia'
    """
    if not hasattr(site,'select_acessos_dia'):
        site.manage_addProduct['ZSQLMethods'].manage_addZSQLMethod(
              id='select_acessos_dia',
              title='select acessos_dia',
              connection_id='Psycopg2_database_connection',
              arguments='tipoObjeto idiomaObjeto',
              template="""SELECT "uidobjeto","totalacessos"
                          FROM acessos_dia 
                          WHERE <dtml-sqltest tipoObjeto column="tipoobjeto" type="string" multiple>
                          AND <dtml-sqltest idiomaObjeto column="idiomaobjeto" type="string" multiple>
                          ORDER BY totalacessos DESC;
                      """
              )
        site.select_acessos_dia.manage_advanced(max_rows=100, max_cache=100, cache_time=0, class_name="", class_file="")


def createSelectAcessosSemana(site):
    """Cria objeto do tipo ZSQLMethod na raiz do site.
       Esse objeto será o responsável pelo select na view 'acessos_semana'
    """
    if not hasattr(site,'select_acessos_semana'):
        site.manage_addProduct['ZSQLMethods'].manage_addZSQLMethod(
              id='select_acessos_semana',
              title='select acessos_semana',
              connection_id='Psycopg2_database_connection',
              arguments='tipoObjeto idiomaObjeto',
              template="""SELECT "uidobjeto","totalacessos"
                          FROM acessos_semana 
                          WHERE <dtml-sqltest tipoObjeto column="tipoobjeto" type="string" multiple>
                          AND <dtml-sqltest idiomaObjeto column="idiomaobjeto" type="string" multiple> 
                          ORDER BY totalacessos DESC;
                      """
              )
        site.select_acessos_semana.manage_advanced(max_rows=100, max_cache=100, cache_time=0, class_name="", class_file="")


def createSelectAcessosMes(site):
    """Cria objeto do tipo ZSQLMethod na raiz do site.
       Esse objeto será o responsável pelo select na view 'acessos_mes'
    """
    if not hasattr(site,'select_acessos_mes'):
        site.manage_addProduct['ZSQLMethods'].manage_addZSQLMethod(
              id='select_acessos_mes',
              title='select acessos_mes',
              connection_id='Psycopg2_database_connection',
              arguments='tipoObjeto idiomaObjeto',
              template="""SELECT "uidobjeto","totalacessos"
                          FROM acessos_mes 
                          WHERE <dtml-sqltest tipoObjeto column="tipoobjeto" type="string" multiple>
                          AND <dtml-sqltest idiomaObjeto column="idiomaobjeto" type="string" multiple> 
                          ORDER BY totalacessos DESC;
                      """
              )
        site.select_acessos_mes.manage_advanced(max_rows=100, max_cache=100, cache_time=0, class_name="", class_file="")


def createSelectAcessosMes(site):
    """Cria objeto do tipo ZSQLMethod na raiz do site.
       Esse objeto será o responsável pelo select na view 'acessos_mes'
    """
    if not hasattr(site,'select_acessos_mes'):
        site.manage_addProduct['ZSQLMethods'].manage_addZSQLMethod(
              id='select_acessos_mes',
              title='select acessos_mes',
              connection_id='Psycopg2_database_connection',
              arguments='tipoObjeto idiomaObjeto',
              template="""SELECT "uidobjeto","totalacessos"
                          FROM acessos_mes 
                          WHERE <dtml-sqltest tipoObjeto column="tipoobjeto" type="string" multiple>
                          AND <dtml-sqltest idiomaObjeto column="idiomaobjeto" type="string" multiple> 
                          ORDER BY totalacessos DESC;
                      """
              )
        site.select_acessos_mes.manage_advanced(max_rows=100, max_cache=100, cache_time=0, class_name="", class_file="")
 
def createSelectAcessosAno(site):
    """Cria objeto do tipo ZSQLMethod na raiz do site.
       Esse objeto será o responsável pelo select na view 'acessos_ano'
    """
    if not hasattr(site,'select_acessos_ano'):
        site.manage_addProduct['ZSQLMethods'].manage_addZSQLMethod(
              id='select_acessos_ano',
              title='select acessos_ano',
              connection_id='Psycopg2_database_connection',
              arguments='tipoObjeto idiomaObjeto',
              template="""SELECT "uidobjeto","totalacessos"
                          FROM acessos_ano 
                          WHERE <dtml-sqltest tipoObjeto column="tipoobjeto" type="string" multiple>
                          AND <dtml-sqltest idiomaObjeto column="idiomaobjeto" type="string" multiple> 
                          ORDER BY totalacessos DESC;
                      """
              )
        site.select_acessos_ano.manage_advanced(max_rows=100, max_cache=100, cache_time=0, class_name="", class_file="")       

def postInstall(context):
    """Called as at the end of the setup process. """  
    site = context.getSite()
    # criando conexao com o postgresql
    createZpsycopg2DatabaseConection(site)
    # criando ZSQLMethod insert_acesso
    createInsertAcesso(site)
    # criando ZSQLMethod select_acessos_dia
    createSelectAcessosDia(site)
    # criando ZSQLMethod select_acessos_semana
    createSelectAcessosSemana(site)
    # criando ZSQLMethod select_acessos_mes
    createSelectAcessosMes(site)
    # criando ZSQLMethod select_acessos_ano
    createSelectAcessosAno(site)