from zope.interface import Interface
# -*- Additional Imports Here -*-


class ISerproMaisAcessadosLayer(Interface):
    """ A layer specific to this product. 
        Is registered using browserlayer.xml
    """