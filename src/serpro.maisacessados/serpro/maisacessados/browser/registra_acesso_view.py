# -*- coding: utf-8 -*-

from zope.interface import Interface
from Products.Five.browser import BrowserView
from zope.interface import implements

from Products.CMFCore.utils import getToolByName
from zope.component import getMultiAdapter


class IRegistraAcessoView(Interface):
    """View"""
    
    def enabled(self):
        """Verifica se o registro de acesso está habilitado"""


class RegistraAcessoView(BrowserView):
    implements(IRegistraAcessoView)
    
    def enabled(self):
        """Verifica se o registro de acesso está habilitado"""
        context = self.context
        request = self.request
        
        edicao = ['folder_contents','edit','atct_edit','base_edit','@@manage-content-rules','@@sharing']
        
        plone_context_state = getMultiAdapter((context,request), name='plone_context_state')
        current_url = plone_context_state.current_page_url()
        
        # se está editando o objeto não é registrado o acesso
        for item in edicao:
            if current_url.find(item) != -1:
                return False  
        
        plone_tools = getMultiAdapter((context,request), name='plone_tools')  
                 
        portal_properties = plone_tools.properties()
        serpro_maisacessados_properties = getattr(portal_properties,'serpro_maisacessados_properties',None)
        
        # se não são encontradas as propriedades
        if not serpro_maisacessados_properties:
            return False
        
        # se a propriedade ativar for False não é registrado  o acesso
        ativar = serpro_maisacessados_properties.enabled        
        if not ativar:
            return False
        
        # se o id for rejeitado não é registrado  o acesso
        ids_rejeitados = serpro_maisacessados_properties.reject_ids
        if context.getId() in ids_rejeitados:
            return False
        
        tipos = serpro_maisacessados_properties.portal_types_id      
        return (context.portal_type in tipos)  
  
    
    def __call__(self):
        """Registra o acesso"""        
        
        if not self.enabled():
            return None
        
        context = self.context
        request = self.request
        
        
        uid = context.UID()
        
        # evitando contar o F5
        if request.cookies.has_key('serpro_maisacessados'):
            if request.cookies.get('serpro_maisacessados') == uid:
                return None
        
        tipo = str(context.portal_type)
        path = '/'.join(context.getPhysicalPath())
        idioma = str(context.Language())
        
        # registra o acesso
        insert_acesso = getToolByName(context,'insert_acesso')
        insert_acesso(uidObjeto=uid,tipoObjeto=tipo,pathObjeto=path,idiomaObjeto=idioma)
        
        # cria o cookie para evitar contar o F5
        # como não tem expires o cookie é apagado ao fechar o browser    
        request.RESPONSE.setCookie('serpro_maisacessados', uid, path='/')
        return None
        
