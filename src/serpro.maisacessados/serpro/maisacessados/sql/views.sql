-- View: acessos_dia

-- DROP VIEW acessos_dia;

CREATE OR REPLACE VIEW acessos_dia AS 
 SELECT acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto", count(acessos."uidobjeto") AS totalacessos
   FROM acessos
  WHERE acessos."dataacesso"::timestamp with time zone >= (now() - '1 day'::interval)
  GROUP BY acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto";

ALTER TABLE acessos_dia OWNER TO postgres;


-- View: acessos_semana

-- DROP VIEW acessos_semana;

CREATE OR REPLACE VIEW acessos_semana AS 
 SELECT acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto", count(acessos."uidobjeto") AS totalacessos
   FROM acessos
  WHERE acessos."dataacesso"::timestamp with time zone >= (now() - '7 day'::interval)
  GROUP BY acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto";

ALTER TABLE acessos_semana OWNER TO postgres;


-- View: acessos_mes

-- DROP VIEW acessos_mes;

CREATE OR REPLACE VIEW acessos_mes AS 
 SELECT acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto", count(acessos."uidobjeto") AS totalacessos
   FROM acessos
  WHERE acessos."dataacesso"::timestamp with time zone >= (now() - '1 month'::interval)
  GROUP BY acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto";

ALTER TABLE acessos_mes OWNER TO postgres;


-- View: acessos_ano

-- DROP VIEW acessos_ano;

CREATE OR REPLACE VIEW acessos_ano AS 
 SELECT acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto", count(acessos."uidobjeto") AS totalacessos
   FROM acessos
  WHERE acessos."dataacesso"::timestamp with time zone >= (now() - '1 year'::interval)
  GROUP BY acessos."uidobjeto", acessos."pathobjeto", acessos."tipoobjeto", acessos."idiomaobjeto";

ALTER TABLE acessos_ano OWNER TO postgres;
