-- Sequence: acessos_idacesso_seq

-- DROP SEQUENCE acessos_idacesso_seq;

CREATE SEQUENCE acessos_idacesso_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE acessos_idacesso_seq OWNER TO postgres;




-- Table: acessos

-- DROP TABLE acessos;

CREATE TABLE acessos
(
  "idacesso" bigint NOT NULL DEFAULT nextval('acessos_idacesso_seq'::regclass),
  "dataacesso" date NOT NULL DEFAULT now(),
  "uidobjeto" character varying(60) NOT NULL,
  "pathobjeto" character varying(600) NOT NULL,
  "tipoobjeto" character varying(60) NOT NULL,
  "idiomaobjeto" character varying(10) NOT NULL,
  CONSTRAINT acessos_pkey PRIMARY KEY ("idacesso")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acessos OWNER TO postgres;

-- Index: "acessos_dataacesso"

-- DROP INDEX "acessos_dataacesso";

CREATE INDEX "acessos_dataacesso"
  ON acessos
  USING btree
  ("dataacesso" DESC);


-- Index: "acessos_tipoobjeto"

-- DROP INDEX "acessos_tipoobjeto";

CREATE INDEX "acessos_tipoobjeto"
  ON acessos
  USING btree
  ("tipoobjeto");


-- Index: "acessos_uidobjeto"

-- DROP INDEX "acessos_uidobjeto";

CREATE INDEX "acessos_uidobjeto"
  ON acessos
  USING btree
  ("uidobjeto");


-- Index: "acessos_idiomaobjeto"

-- DROP INDEX "acessos_idiomaobjeto";

CREATE INDEX "acessos_idiomaobjeto"
  ON acessos
  USING btree
  ("idiomaobjeto");
