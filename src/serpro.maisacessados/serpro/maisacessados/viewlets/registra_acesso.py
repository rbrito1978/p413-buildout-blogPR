# -*- coding: utf-8 -*-

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.app.layout.viewlets.common import ViewletBase

from zope.component import getMultiAdapter


class RegistraAcessoViewlet(ViewletBase):
    render = ViewPageTemplateFile('registra_acesso.pt')
    
    def enabled(self):
        """Verifica se a viewlet deve ser ativada"""
        context = self.context
        request = self.request
        
        registra_acesso_view = getMultiAdapter((context,request), name='registra_acesso_view')        
                
        return registra_acesso_view.enabled()
    
    def script(self):
        """Retorna o script js com a url correta"""
        context = self.context
        request = self.request
        
        plone_context_state = getMultiAdapter((context,request), name='plone_context_state')
        return """jq.get("%s/@@registra_acesso_view");""" % plone_context_state.object_url()
        
            
        


