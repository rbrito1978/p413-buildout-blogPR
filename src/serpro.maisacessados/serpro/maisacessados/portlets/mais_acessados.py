# -*- coding: utf-8 -*-

from plone.memoize.instance import memoize
from plone.portlets.interfaces import IPortletDataProvider
from zope.formlib import form
from zope.interface import implements
from zope import schema
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from Acquisition import aq_inner
from Products.CMFCore.utils import getToolByName
from zope.component import getMultiAdapter
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.portlets.portlets import base

from serpro.maisacessados import _


class IMaisAcessados(IPortletDataProvider):
    
    titulo = schema.TextLine(
        title=_(u'Portlet title'),
        description=_(u''),
        required=True,
        default=u"Most accessed"
        )

    portal_types_id = schema.Tuple(
        title=_(u'Portal Types'),
        description=_(u"Select the portal types.",
        ),
        required=True,
        default = ('Folder','Topic','File','Image','Link','Document','News Item','Event'),
        value_type=schema.Choice(vocabulary="plone.app.vocabularies.ReallyUserFriendlyTypes")
    )
    
    num_itens = schema.Int(
        title=_(u'Number of items'),
        description=_(u"Insert the number of items.",
        ),
        required=True,
        default=5
    )
    
    periodo = schema.Choice(
        title=_(u'Filtering period'),
        description=_(u"Select the filtering period.",
        ),
        required=True,
        default="acessos_dia",
        vocabulary=SimpleVocabulary([SimpleTerm('acessos_dia','acessos_dia',_(u'1 day')),
                                    SimpleTerm('acessos_semana','acessos_semana',_(u'1 week')),
                                    SimpleTerm('acessos_mes','acessos_mes',_(u'1 month')),
                                    SimpleTerm('acessos_ano','acessos_ano',_(u'1 year')),
                                    ]
                                    )       
                          
    )
    
    exibir_total = schema.Bool(
        title=_(u'Show the number of hits?'),
        default=True,
    )


class Assignment(base.Assignment):
    implements(IMaisAcessados)
    
    titulo = "Most Accessed"
    portal_types_id = ('Folder','Topic','File','Image','Link','Document','News Item','Event')
    num_itens = 5
    periodo = "acessos_dia"
    exibir_total = True
    
    def __init__(self, titulo=None,portal_types_id=None,num_itens=None,periodo=None,exibir_total=None):
        self.titulo = titulo
        self.portal_types_id = portal_types_id
        self.num_itens = num_itens
        self.periodo = periodo
        self.exibir_total = exibir_total

    @property
    def title(self):
        return _(u'%s' % self.data.titulo)


class Renderer(base.Renderer):
    render = ViewPageTemplateFile('mais_acessados.pt')

    @property
    def available(self):
        return len(self._data())
    
    def getMaisAcessados(self):
        return self._data()
    
       

    @memoize
    def _data(self):
        context = aq_inner(self.context)
        request = context.REQUEST
        plone_tools = getMultiAdapter((context,request), name='plone_tools') 
        plone_portal_state = getMultiAdapter((context,request), name='plone_portal_state')
        
        portal_properties = plone_tools.properties()            
        typesUseViewActionInListings = portal_properties.site_properties.typesUseViewActionInListings 
        ids_rejeitados = portal_properties.serpro_maisacessados_properties.reject_ids
        # sempre o idioma neutro mais o atual
        langs = ("","%s" % plone_portal_state.language())
        
        # vemos qual das visoes do postgresql vamos utilizar
        visao = self.data.periodo
        zsqlmethod = getToolByName(context, 'select_%s' % visao)
        records = zsqlmethod(tipoObjeto=list(self.data.portal_types_id),idiomaObjeto=langs)
        
        # uids retornados pelo postgresql
        uids = [record[0] for record in records.tuples()]
        
        # buscamos pelos uids no catalog
        catalog = plone_tools.catalog()
        itens = [dict(UID=item.UID,id=item.id,Title=item.Title,getURL=item.getURL(),portal_type=item.portal_type) \
                 for item in catalog(UID=uids)]
        
        # mantemos a ordem do postgresql que vem pelos objetos com mais acessos
        mais_acessados = []        
        for record in records.tuples():            
            for item in itens:
                if record[0] == item['UID']:
                    if item['id'] not in ids_rejeitados:
                        if item['portal_type'] in typesUseViewActionInListings:
                            item['getURL'] = '%s/view' % item['getURL']
                        item['totalacessos'] = int(record[1])
                        mais_acessados.append(item)
                    break
            if len(mais_acessados) >= self.data.num_itens:
                break            
        return mais_acessados


class AddForm(base.AddForm):
    form_fields = form.Fields(IMaisAcessados)
    label = _(u"Add Most Accessed Portlet")
    description = _(u"This portlet list most accessed objetcs.")

    def create(self, data):
        return Assignment(titulo=data.get('titulo',u"Most accessed"),
                          portal_types_id=data.get('portal_types_id',('Folder','Topic','File','Image','Link','Document','News Item','Event')),
                          num_itens=data.get('num_itens',5),
                          periodo=data.get('periodo',"acessos_dia"),
                          exibir_total=data.get('exibir_total',True))


class EditForm(base.EditForm):
    form_fields = form.Fields(IMaisAcessados)
    label = _(u"Edit Most Accessed Portlet")
    description = _(u"This portlet list most accessed objetcs.")