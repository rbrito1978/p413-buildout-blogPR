# -*- coding: utf-8 -*-

from zope.schema import Tuple, List, Bool, Choice, TextLine
from zope.component import adapts
from zope.interface import Interface, implements

from Products.CMFPlone.interfaces import IPloneSiteRoot
from Products.CMFPlone.utils import getToolByName
from Products.CMFDefault.formlib.schema import ProxyFieldProperty, SchemaAdapterBase

from zope.formlib.form import FormFields
from plone.app.controlpanel.form import ControlPanelForm
from zope.app.form.browser.itemswidgets import MultiSelectWidget as BaseMultiSelectWidget

from serpro.maisacessados import _


class MultiSelectWidget(BaseMultiSelectWidget):
    
    def __init__(self, field, request):
        """Initialize the widget.
        """
        super(MultiSelectWidget, self).__init__(field,
            field.value_type.vocabulary, request)



class ISerproMaisAcessadosSchema(Interface):
    
    portal_types_id = Tuple(
        title=_(u'Portal Types'),
        description=_(u"Select the portal types that should have the viewing record."),
        value_type=Choice(vocabulary="plone.app.vocabularies.ReallyUserFriendlyTypes")
    )
    
    reject_ids = List(
        title=_(u'Reject Ids'),
        description=_(u"Add ids of objects that not should should have the viewing record."),
        value_type=TextLine()                     
    )
    
    enabled = Bool(
        title=_(u'Enable viewing record?'),
        description=_(u"Enable or disable the viewlet that performs the call to counter views."),
    )
    


class ProvidersControlPanelAdapter(SchemaAdapterBase):
    
    adapts(IPloneSiteRoot)
    implements(ISerproMaisAcessadosSchema)

    def __init__(self, context):
        super(ProvidersControlPanelAdapter, self).__init__(context)
        portal_properties = getToolByName(context, 'portal_properties')
        self.context = portal_properties.serpro_maisacessados_properties
    
    portal_types_id = ProxyFieldProperty(ISerproMaisAcessadosSchema['portal_types_id'])
    reject_ids = ProxyFieldProperty(ISerproMaisAcessadosSchema['reject_ids'])
    enabled = ProxyFieldProperty(ISerproMaisAcessadosSchema['enabled'])
   
    

class ProvidersControlPanel(ControlPanelForm):
    
    form_fields = FormFields(ISerproMaisAcessadosSchema)
    form_fields['portal_types_id'].custom_widget = MultiSelectWidget


    label = _(u'SERPRO Most Accessed')
    description = _(u"Object's viewing record.")
    form_name = _('Config')
