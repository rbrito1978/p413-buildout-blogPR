# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import os

version = '0.6.1'

setup(name='serpro.protegido',
      version=version,
      description="Contém uma interface IProtegido, que, ao ser implementada, faz com que o objeto que a implemente não possa ser excluído: existe um subscriber pra IProtegido que impede a ação de exclusão.",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Development Status :: 4 - Beta",
        "Framework :: Plone",
        "Framework :: Plone :: 3.3",
        "Framework :: Plone :: 4.0",
        "Framework :: Zope2",
        "Intended Audience :: Developers",
        "License :: Other/Proprietary License",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.4",
        "Programming Language :: Python :: 2.6",
        ],
      keywords='',
      author='Giovanni Monteiro Calanzani',
      author_email='giovanni.calanzani@serpro.gov.br',
      url='https://www.serpro.gov.br/',
      license='other',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      paster_plugins=["ZopeSkel"],
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
