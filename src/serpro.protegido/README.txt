Introdução
==========
É muito comum em portais a criação de pastas e documentos pelo
setuphandlers.py quando se instala um produto. O problema é que muitas
vezes várias partes do site se tornam "dependentes" desses objetos
criados, e a exclusão dos mesmos poderia trazer efeitos adversos ao
portal.

Dessa forma, esse produto tem o objetivo de fornecer uma interface,
'IProtegido' e um subscriber para a mesma, que impede qualquer objeto
que implemente essa interface de ser excluído. A solução é simples
e evita criação de workflows locais muito específicos apenas para
evitar um objeto de ser excluído. Possui ainda a vantagem de ser
reversível bastando desmarcar a interface na aba "Interfaces" de um
objeto caso ele necessite ser excluído.

Esse produto já está internacionalizado em pt_BR, en e es.

Utilização
==========
Simplesmente implemente a interface no objeto que você deseja proteger:

::

    from zope.interface import alsoProvides

    from serpro.protegido.interfaces import IProtegido

    alsoProvides(MEU_OBJETO, IProtegido)

A partir desse momento, qualquer tentativa de exclusão irá mostrar uma
mensagem informativa pro usuário explicando porque ele não conseguiu
excluir.  
 
Se você tentar excluir o próprio objeto (ou seja, está no contexto
do próprio objeto e seleciona a aba "Ações" -> "Excluir"), será
apresentada uma tela de erro padrão do Plone, que você pode ignorar
pois ela só aparece porque foi utilizado um 'raise' durante um evento
e dessa forma a página de erros tem de ser mostrada.

Existe uma 'flag' em portal_properties que você pode
habilitar/desabilitar na ZMI, em 'serpro_protegido_properties', chamada
"desabilitado": marque essa property caso você deseje desabilitar o
recurso de proteção em todo o site. Isso é útil em situações
como no ambiente de desenvolvimento, que você necessita excluir um
Plone Site e não precisar entrar em cada objeto que implementa aquela
interface e remover.

**CUIDADO** ao utilizar esse recurso da flag em ambiente de produção
pois toda a proteção a quem tiver implementada a interface IProtegido
será desabilitada! Apesar do subscriber só permitir desabilitar se o
usuário for Manager, outro usuário Manager que não tenha conhecimento
das pastas de sistema do site pode excluir erroneamente sem querer
porque o aviso de que aquela pasta é protegida não aparecerá mais.

Compatibilidade
===============
Testado em:

    3.3.5
    4.0.X

Customizações
=============
Houve uma customização em 

Plone-3.3.5-py2.4.egg/Products/CMFPlone/skins/plone_scripts/folder_paste.cpy
Plone-4.0.9-py2.6.egg/Products/CMFPlone/skins/plone_scripts/folder_paste.cpy

para poder relançar uma exceção que não era relançada. Favor ler
folder_paste.cpy para mais detalhes.

Ficar atento a essa customização quando for migrar esse produto para
outras versões do Plone.

As mensagens informativos indicando a impossibilidade de exclusão são
diferentes quando o usuário é Manager e Conteudista.

TODO:
=====
- Testes unitários;
- Customização de delete_confirmation.cpy e
delete_confirmation_page.cpt para nem perguntar por confirmação se o
objeto implementar essa interface.
- Criar uma rotina de desinstalação que remova as interfaces dos
objetos.
