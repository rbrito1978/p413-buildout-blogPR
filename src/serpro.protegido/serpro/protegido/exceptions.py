# -*- coding: utf-8 -*-

DEFAULT_EX_MSG = """IProtegido object. Can't exclude! Check 'serpro.protegido'.
                If you need to completely disable the IProtegido lock, check
                serpro_protegido_properties on portal_properties and disable
                it."""

class SerproProtegidoException(Exception):
    """Exceção lançada para objeto que possui implementada IProtegido."""
