# -*- coding: utf-8 -*-

  # -*- extra stuff goes here -*-
from zope.i18nmessageid import MessageFactory

from AccessControl import ModuleSecurityInfo

SerproProtegidoMessageFactory = MessageFactory('serpro.protegido')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""

    # ModuleSecurityInfo é necessário para que seja possível acessar esses
    # objetos em skins/serpro_protegido_scripts/folder_paste.cpy.
    ModuleSecurityInfo("serpro.protegido").declarePublic("SerproProtegidoMessageFactory")

    ModuleSecurityInfo("serpro.protegido.config").declarePublic("DEFAULT_MSGID")
    ModuleSecurityInfo("serpro.protegido.config").declarePublic("DEFAULT_MSGID_MANAGER")
    ModuleSecurityInfo("serpro.protegido.config").declarePublic("MANAGER_ID")
    ModuleSecurityInfo("serpro.protegido.config").declarePublic("UNAUTHORIZED_EXCEPTION")

    ModuleSecurityInfo("serpro.protegido.exceptions").declarePublic("DEFAULT_EX_MSG")
    ModuleSecurityInfo("serpro.protegido.exceptions").declarePublic("SerproProtegidoException")
