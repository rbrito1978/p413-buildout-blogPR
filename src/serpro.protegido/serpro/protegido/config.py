# -*- coding: utf-8 -*-

try:
    # Plone 4 and higher
    import plone.app.upgrade
    PLONE_VERSION = 4
except ImportError:
    PLONE_VERSION = 3

MANAGER_ID = 'Manager'
DEFAULT_MSGID = u'cant_exclude'
DEFAULT_MSGID_MANAGER = u'info_exclusion_zmi'

PROPERTY_SHEET = {'id': 'serpro_protegido_properties',
                  'title': 'Propriedades de serpro.protegido',
                  'default_attr_id': 'desabilitado'}

# A partir do Python 2.6, "string exceptions" foram removidas, e o script
# folder_paste.cpy na versão 3.3.5 ainda utiliza algumas dessas exceções
# (Como "Unauthorized"). Dessa forma, dependendo da versão do Plone executando
# serpro.protegido (como Plone 4.0.9), o nome da exceção é diferente.
# http://docs.python.org/whatsnew/2.6.html#deprecations-and-removals
from AccessControl import Unauthorized
UNAUTHORIZED_EXCEPTION = (Unauthorized, "Unauthorized")
if PLONE_VERSION > 3:
    UNAUTHORIZED_EXCEPTION = Unauthorized
