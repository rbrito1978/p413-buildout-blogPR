# -*- coding: utf-8 -*-

from AccessControl.SecurityManagement import getSecurityManager

from Products.CMFCore.utils import getToolByName

from serpro.protegido import SerproProtegidoMessageFactory as _

from serpro.protegido.exceptions import SerproProtegidoException, \
    DEFAULT_EX_MSG

from serpro.protegido.config import DEFAULT_MSGID, DEFAULT_MSGID_MANAGER, \
    MANAGER_ID, PROPERTY_SHEET

from zope.app.component.hooks import getSite


def is_manager():
    """Retorna se o usuário é Manager."""
    user = getSecurityManager().getUser()
    return user.has_role(MANAGER_ID)

def is_desabilitado(site):
    """Retorna se a funcionalidade de proteção está desabilitada.

    Esse recurso é apenas disponibilizado para quem é Manager: isso é
    importante para evitar que um desenvolvedor marque a opção em ambiente de
    produção e a esqueça marcada."""

    if is_manager():
        # XXX: Situação que ocorre quando se está excluindo um Plone site, portanto
        # indique que a proteção está desabilitada.
        if site is None:
            return True

        portal_properties = site.portal_properties
        property_sheet = getattr(portal_properties, PROPERTY_SHEET['id'], None)
        if property_sheet is not None:
            property = property_sheet.getProperty(PROPERTY_SHEET['default_attr_id'])
            if property is not None:
                return property

    return False

def impede_exclusao(objeto, evento):
    """Impede a exclusão de objetos que implementem a interface IProtegido."""

    site = getSite()

    if not is_desabilitado(site):
        msg = _(DEFAULT_MSGID, mapping={'obj': objeto.absolute_url()})

        if is_manager():
            msg_manager = _(DEFAULT_MSGID_MANAGER)
            msg = msg_manager

        plone_utils = getToolByName(site, 'plone_utils')
        plone_utils.addPortalMessage(msg, 'error')

        raise SerproProtegidoException(DEFAULT_EX_MSG)
