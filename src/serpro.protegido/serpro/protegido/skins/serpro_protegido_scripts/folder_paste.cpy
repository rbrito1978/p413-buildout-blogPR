## Controller Python Script "folder_paste"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind state=state
##bind subpath=traverse_subpath
##parameters=
##title=Paste objects into a folder
##

#------------------------------------------------------------------------------
#CUSTOMIZADO DE Plone-3.3.5-py2.4.egg/Products/CMFPlone/skins/plone_scripts/folder_paste.cpy
#CUSTOMIZADO DE Plone-4.0.9-py2.6.egg/Products/CMFPlone/skins/plone_scripts/folder_paste.cpy
#------------------------------------------------------------------------------
# MOTIVAÇÕES
# Customização necessária pois o script folder_paste, responsável por
# colar um objeto, chama o método OFS.CopySupport.manage_pasteObjects.
# Quando esse método notifica o subscriber de serpro.protegido, o
# subscriber dá um raise, mas na sequência de folder_paste existe
# um "except" que não estipula uma exceção e, portanto, segue o
# procedimento e não aborta a transação completamente, ficando a ação
# de OFS.CopySupport.manage_pasteObjects incompleta.

# Agora, nessa customização, é relançada a exceção
# SerproProtegidoException caso ocorra no processo de colar arquivos.

import AccessControl

from Products.CMFPlone import PloneMessageFactory as _
#Agora vem de serpro.protegido.config. Ler no config.py a justificativa.
#from AccessControl import Unauthorized
from ZODB.POSException import ConflictError

from serpro.protegido import SerproProtegidoMessageFactory
from serpro.protegido.config import DEFAULT_MSGID, DEFAULT_MSGID_MANAGER, \
    MANAGER_ID, UNAUTHORIZED_EXCEPTION
from serpro.protegido.exceptions import SerproProtegidoException,\
    DEFAULT_EX_MSG

msg=_(u'Copy or cut one or more items to paste.')

if context.cb_dataValid:
    try:
        context.manage_pasteObjects(context.REQUEST['__cp'])
        from Products.CMFPlone.utils import transaction_note
        transaction_note('Pasted content to %s' % (context.absolute_url()))
        context.plone_utils.addPortalMessage(_(u'Item(s) pasted.'))
        return state
    except ConflictError:
        raise
    except ValueError: 
        msg=_(u'Disallowed to paste item(s).')
    except UNAUTHORIZED_EXCEPTION:
        msg=_(u'Unauthorized to paste item(s).')
    except SerproProtegidoException:
        # Mensagem pro usuário
        mf = SerproProtegidoMessageFactory
        msg = mf(DEFAULT_MSGID, mapping={'obj': context.absolute_url()})

        # Mensagem pro manager
        user = AccessControl.getSecurityManager().getUser()
        if user.has_role(MANAGER_ID):
            msg_manager = mf(DEFAULT_MSGID_MANAGER)
            msg = msg_manager

        context.plone_utils.addPortalMessage(msg)

        # Precisa dar o raise novamente, senão não aborta a transaction.
        raise SerproProtegidoException(msg) 
    except: # fallback
        msg=_(u'Paste could not find clipboard content.')

context.plone_utils.addPortalMessage(msg, 'error')
return state.set(status='failure')
