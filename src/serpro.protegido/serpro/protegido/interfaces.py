from zope.interface import Interface


class IProtegido(Interface):
    """Interface que define um objeto como protegido."""
