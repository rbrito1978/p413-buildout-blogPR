# -*- coding: utf-8 -*-

from serpro.protegido.config import PROPERTY_SHEET

# XXX: Esse método veio de serpro.setuphandlersutils, para evitar uma
# dependência de serpro.protegido nesse módulo.
def setPortalProperties(site, property_sheet_name, properties,
        property_sheet_title=''):
    """Cria uma property sheet ou apenas uma propriedade na tool
    portal_properties. Se aquela propriedade já existir, não faz nada.

    @properties: [{'name':'teste', 'value': True, 'type': 'boolean'},
                  {'name':'teste2', 'value':' novo', 'type': 'string'}]"""

    portal_properties = site.portal_properties

    if getattr(portal_properties, property_sheet_name, None) is None:
        portal_properties.addPropertySheet(property_sheet_name, property_sheet_title)

        property_sheet = getattr(portal_properties, property_sheet_name)

        for property in properties:
            if not property_sheet.hasProperty(property['name']):
                property_sheet._setProperty(property['name'], property['value'], property['type'])

def postInstall(context):
    """Called as at the end of the setup process. """
    site = context.getSite()

    # A property sheet foi feita para que, caso um usuário Manager queira
    # desabilitar por completo esse recurso (para excluir o Plone Site,
    # desabilitar algum produto, reinstalar) basta selecionar a opção
    # 'desabilitado' como True ao invés de remover a interface IProtegido de
    # todos os objetos que a implementam.
    properties = [{'name': PROPERTY_SHEET['default_attr_id'], 'value': False, 'type': 'boolean'}]
    setPortalProperties(site, PROPERTY_SHEET['id'], properties, PROPERTY_SHEET['title'])
