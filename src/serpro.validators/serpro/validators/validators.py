# -*- coding: utf-8 -*-
from types import FileType

from zope.deprecation import deprecated

from Products.validation.interfaces.IValidator import IValidator

from ZPublisher.HTTPRequest import FileUpload
from Acquisition import aq_base

import os


deprecated('MaxSizeValidator', 'ATENCAO: Utilize o validador oficial em \
        Products.validation. Se o problema for apenas por causa da traducao, \
        utilize o i18n-overrides.')
class MaxSizeValidator:
    """ Validador de tamanho máximo de arquivo.
    """
    __implements__ = IValidator

    def __init__(self, name, title='', description='', maxsize=0):
        self.name = name
        self.title = title or name
        self.description = description
        self.maxsize= maxsize

    def __call__(self, value, *args, **kwargs):

        # calculate size
        if isinstance(value, FileUpload) or type(value) is FileType \
          or hasattr(aq_base(value), 'tell'):
            value.seek(0, 2) # eof
            size = value.tell()
            value.seek(0)
        else:
            try:
                size = len(value)
            except TypeError:
                size = 0
        size = float(size)
        sizeMB = (size / (1024 * 1024))

        if sizeMB > self.maxsize:
            return ("Tamanho máximo do arquivo: %s MB." %
                     (self.maxsize))
        else:
            return True


deprecated('ExtensionValidator', 'ATENCAO: Esse modulo foi reposicionado em \
        serpro.validators.file.ExtensionValidator. O reposicionamento fara \
        parte de um plano de reestruturacao desse modulo uma vez que agora \
        e possivel utilizar modulos reusaveis')
class ExtensionValidator:
    """ Validador de extensao de arquivo.
    """
    __implements__ = IValidator

    def __init__(self, name, title='', description='', extensions=[]):
        self.name = name
        self.title = title or name
        self.description = description
        self.extensions= [extension.upper() for extension in extensions]


    def __call__(self, value, *args, **kwargs):

        if isinstance(value, FileUpload) or type(value) is FileType:
            extension = os.path.splitext(value.filename)[1]

        if extension.upper() not in self.extensions:
            return 'Tipo de arquivo nao permitido. \
Os tipos permitidos sao: %s' % (', '.join(self.extensions))


class MaxCharactersValidator:
    """ Validador de número de caracteres.
    """
    __implements__ = IValidator

    def __init__(self, name, maximum, title='', description=''):
        self.name = name
        self.title = title or name
        self.description = description
        if type(maximum) is not int:
            raise ValueError, "%s não é um inteiro" % maximum
        self.max= maximum


    def __call__(self, value, *args, **kwargs):

        if len(value) > self.max:
            return 'Campo deve ter no máximo: %s caracteres.' % self.max
