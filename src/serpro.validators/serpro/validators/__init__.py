# -*- coding: utf-8 -*-
import zope.i18nmessageid

MessageFactory = zope.i18nmessageid.MessageFactory('serpro.validators')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
