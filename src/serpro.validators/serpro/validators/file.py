# -*- coding: utf-8 -*-
"""Validadores para tipos do tipo FILE no Plone.

"""
from types import FileType, StringType

from Products.validation.interfaces.IValidator import IValidator

from ZPublisher.HTTPRequest import FileUpload

import zipfile

from zipfile import ZipInfo

import os

try:
    from iw.fss.FileSystemStorage import VirtualData, VirtualBinary, \
        VirtualFile, VirtualImage
except Exception:
    # Site não utiliza FileSystemStorage.
    pass

from serpro.validators.config import PLONE_VERSION

# XXX: Se estiver deletando uma imagem de um archetype, value será uma string
# 'DELETE_IMAGE'. Necessário para Plone < 4.1.4 e Products.Archetypes: < 1.7.12
DELETE_IMAGE = 'DELETE_IMAGE'

# FIXME: Em algumas situações de edição, em algumas versões de iw.fss, quando
# um objeto é editado e sequer alterado, o type do objeto que é retornado é
# 'ImplicitAcquirerWrapper' mas a instância não necessariamente será um Virtual
# do iw.fss. Uma hipótese desse problema é a versão do Zope que a SUPCD utiliza
# que não dá supote ao iw.fss mais novo (2.10.5).
IMPLICIT_TYPE = "<type 'ImplicitAcquirerWrapper'>"
# No Plone 4.0.X, o tipo de objeto que é retornado quando se está editando:
IMPLICIT_TYPE_ACQUISITION = "<type 'Acquisition.ImplicitAcquisitionWrapper'>"


class ExtensionValidator:
    """ Validador de extensao de arquivo.

    Valida pelo filename e, opcionalmente, pelo mimetype (é necessário passar o
    mimetype por parâmetro no momento do registro do validador para que
    funcione corretamente).

    A lista de extensões válidas enviada como parâmetro no instanciamento desse
    validador pode vir minúsculo, maiúsculo, com ou sem ponto: o validador
    trata cada uma dessas condições de forma transparente.

    Se desejar validar também o mimetype junto com a extensão, passe a variável
    'extensions' no formato:

        [{'extension': 'jpeg', 'mime-type':'image/jpeg'},
        {'extension': 'jpg', 'mime-type':'image/jpeg'},
        {'extension': 'flv', 'mime-type':['application/x-flash-video',
            'video/x-flv']}]

    OBS: Em alguns sistemas operacionais ou em sistemas que não tem o
         openoffice instalado:

         - O ods é reconhecido como application/x-zip ou
           application/octet-stream
         - O xlsx é reconhecido como application/x-zip

        Em alguns sistemas operacionais ou em sistemas que não tem o Exel
        instalado:

        - O xls é reconhecido como application/msword ou
          application/vnd.ms-office

    Veja que é possível passar uma lista de mime-types válidos, caso a extensão
    possa ter mais de um mimetype mas ainda não se sabe  o motivo nem a
    influência de alguma plataforma (como diferenças entre browsers).

    Tanto uma lista de strings quanto uma lista de dicionário nessa estrutura é
    válido: a diferença é que se você passar apenas uma lista com as extensões,
    o mimetype sequer será levado em conta. O validador trata isso de forma
    transparente. Ou seja, 'extensions' no formato:

        ['jpg', '.gif', '.FLV']

    Também será válido.

    Para receber uma lista atualizada da grande maioria dos mimetypes hoje,
    acesse: http://www.iana.org/assignments/media-types/index.html

    Para uma lista dos documentos microsoft:
    http://filext.com/faq/office_mime_types.php

    Para uma lista open office:
    http://www.openoffice.org/framework/documentation/mimetypes/mimetypes.html

    É importante ressaltar que não existe uma lista completa de mimetypes na
    internet, portanto muitos códigos não estarão presentes nessas urls, sendo
    necessário um trabalho extra por parte do implementador.

    """

    if PLONE_VERSION <= 3:
        __implements__ = IValidator

    if PLONE_VERSION > 3:
        from zope.interface import implements
        implements(IValidator)

    def __init__(self, name, title='', description='', extensions=[],
            zip_internal_validation=True):
        self.name = name
        self.title = title or name
        self.description = description

        self.mime = None
        try:
            # Veio um dicionário, com mimetype.
            self.extensions = [extension['extension'].replace('.', '').lower()
                for extension in extensions]
            self.mime_types = extensions
            self.mime = True
            self.msg_mime = u"""A extensão '%s' é válida, mas o mime-type '%s'
                do arquivo não é válido pois para essa extensão é (são)
                esperado(s) o(s) mime-type(s) '%s'."""
        except TypeError:
            # Veio uma lista e não um dicionário com mime type: portanto nem
            # valide o mimetype.
            self.extensions = [extension.replace('.', '').lower() for extension
                in extensions]

        # Indica se é para verificar extensões de arquivos que estiverem
        # dentro de um zip.
        self.zip_internal_validation = zip_internal_validation
        self.msg = 'Tipo de arquivo nao permitido. Os tipos permitidos sao: \
            %s.' % ', '.join(self.extensions)

    def _getFilename(self, value):
        """Retorna o filename extension de um file.

        Já retorna a extensão sem o ponto.
        """
        if isinstance(value, FileUpload) or type(value) is FileType \
            or isinstance(value, ZipInfo):
            return os.path.splitext(value.filename)[1][1:].strip().lower()

        # É apenas uma string (provavelmente apenas contendo o nome a extensão
        # de um objeto).
        if isinstance(value, StringType):
            return os.path.splitext(value)[1][1:].strip().lower()

        raise Exception("Can't get a valid extension!")

    def _checkValidMime(self, value, extension):
        """Checa se é preciso checar mime, e se precisar, já retorna se é
        válido ou não: se retorna None é porque é válido/não necessita de
        verificação."""
        if self.mime is not None:

            # XXX: Por quê a chamada de magic no método e não no módulo?
            # A chamada tem de ser aqui, dentro do método, e não no início do
            # módulo, porque por algum motivo, se o módulo for importado e
            # permanecido em memória no momento que o Zope é levantado, ocorrem
            # erros de segfault que simplesmente matam o processo Python onde
            # roda o Zope! Esse erro é aleatório e só para alguns tipos...
            import magic
            self.mime = magic.Magic(mime=True)

            # Se for um atributo de um zip, ele já vem pra esse método como str
            # não necessitando chamar novamente o método read().
            try:
                mime = self.mime.from_buffer(value.read())
            except AttributeError:
                mime = self.mime.from_buffer(value)

            # XXX: é necessário 'limpar' novamente self.mime, ou dá erro.
            # Não sei se tem a ver com o erro descrito em
            # https://bugzilla.redhat.com/show_bug.cgi?id=807434, mas ocorre um
            # erro parecido com relação a não sair de forma 'limpa' do processo
            # python:
            #  Exception TypeError: "'NoneType' object is not callable" in
            # <bound method Magic.__del__ of <magic.Magic instance at
            # 0xe72be0c>> ignored
            # se self.mime não for 'limpo'.
            self.mime = True

            for i in self.mime_types:
                # Se a extensão passada por parâmetro é a que está na lista de
                # mime-types, então o mime-type tem de ser o mesmo!
                if i['extension'] == extension:

                    if isinstance(i['mime-type'], list):
                        not_mime = mime not in i['mime-type']
                        valid_mime = ', '.join(i['mime-type'])
                    else:
                        not_mime = mime != i['mime-type']
                        valid_mime = i['mime-type']

                    if not_mime:
                        if not self._is_application_x_empty(mime):
                            return self.msg_mime % (extension, mime,
                                valid_mime)
                    else:
                        return None

                    # A extensão já foi encontrada para ser validada.
                    break

    def _is_application_x_empty(self, mime):
        """
        XXX: O validador é chamado várias vezes, e por algum motivo que foge de
        minha compreensão, ele vem depois da primeira validação, em apenas
        alguns tipos, com mime-type 'application/x-empty' ou 'application/empty'
        (visto em casos onde o sistema era CentOS) mesmo sendo um
        arquivo perfeitamente válido. Assim, se vier como x-empty, em tese já
        foi validado porque em
        Products.ATContentTypes-2.0.7-py2.6.egg/Products/ATContentTypes/lib/validators.py
        já existe o NonEmptyFileValidator, que é chamado e validado antes mesmo
        do meu validador. Assim, não consigo entender o motivo de vir vazio
        em ExtensionValidator.

        Como pra entrar nesse validador foi necessário passar primeiro pelo
        NonEmptyFileValidator, e só aparece 'application/x-empty' depois que
        foi corretamente validado uma vez pelo menos, acredito que validar se
        é 'application/x-empty' nesse contexto não traz maiores problemas.
        """
        return mime == 'application/x-empty' or mime == 'application/empty'

    def _is_delete_image(self, value):
        """Retorna se é apenas uma imagem sendo deletada."""
        if isinstance(value, StringType):
            return value == DELETE_IMAGE

    def __call__(self, value, *args, **kwargs):

        # Site não utiliza FileSystemStorage ou versão antiga do mesmo. Ver
        # comentário sobre IMPLICIT_TYPE no início do módulo.
        if (IMPLICIT_TYPE == str(type(value)) or
            IMPLICIT_TYPE_ACQUISITION == str(type(value))):
            return True

        try:
            if (isinstance(value, VirtualFile) or
               isinstance(value, VirtualData) or
               isinstance(value, VirtualBinary) or
               isinstance(value, VirtualImage) or
               self._is_delete_image(value)):
                # Arquivo já está no filesystem, portanto já foi validado. Essa
                # situação ocorre quando um usuário está editando um objeto no
                # administrativo.
                return True
        except Exception:
            # Site não utiliza FileSystemStorage
            pass

        extension = self._getFilename(value)

        if extension in self.extensions:
            if extension == 'zip' and self.zip_internal_validation:
                # Nova lista sem a extensão zip que será utilizada para validar
                # os itens dentro do zip: um zip não pode ter um zip dentro
                # dele mesmo.
                no_zip_extensions = self.extensions[:]
                no_zip_extensions.remove('zip')

                zipe = zipfile.ZipFile(value)
                for file in zipe.infolist():
                    extensao = self._getFilename(file)
                    if extensao not in no_zip_extensions:
                        zipe.close()
                        ext_permitidas = ', '.join(no_zip_extensions)
                        msg = """O arquivo zip não pode conter arquivos do tipo
                            %s. Extensões permitidas: %s.'""" % (extensao,
                            ext_permitidas)
                        return msg
                    else:
                        # A extensão pode até ser válida, mas tem de analisar o
                        # mimetype.
                        valid_mime = self._checkValidMime(zipe.read(file),
                            extensao)
                        if valid_mime is not None:
                            return valid_mime
                zipe.close()
            else:
                # Se não for zip apenas valide o mimetype.
                valid_mime = self._checkValidMime(value, extension)
                if valid_mime is not None:
                    return valid_mime

            # Extensao e valida, e se foi zip mas chegou aqui, tambem é valido.
            return True

        # Se chegou aqui, não foi validado e retorna a mensagem padrão de erro.
        return self.msg
