# -*- coding: utf-8 -*-

from zope.interface import Interface

class IValidatorsView(Interface):

    def isCPF(cpf):
        """Verifica se o cpf é valido"""

    def isCNPJ(cnpj):
        """Verifica se o cnpj é valido"""
