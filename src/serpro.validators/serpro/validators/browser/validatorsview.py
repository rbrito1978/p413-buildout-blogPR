# -*- coding: utf-8 -*-

from Products.Five import BrowserView

from zope.interface import implements

from serpro.validators.browser.interfaces import IValidatorsView

class ValidatorsView(BrowserView):
    implements(IValidatorsView)

    def isCPF(self, value):
        
        if value.isdigit():
            if len(value) == 11:
                # valida se for CPF
                a = int(value[0]) 
                b = int(value[1])
                c = int(value[2])
                d = int(value[3])
                e = int(value[4])
                f = int(value[5])
                g = int(value[6])
                h = int(value[7])
                i = int(value[8])
                j = int(value[9])
                k = int(value[10])
    
                if a == b == c == d == e == f == g == h:
                    return False

                if k == j == i == h == g == f == e == d:
                    return False
    
                soma1 = ( a*10 + b*9 + c*8 + d*7 + e*6 + f*5 + g*4 + h*3 + i*2 )
                resto = soma1 % 11
                if resto == 0 or resto == 1:
                    d1 = 0
                else:
                    d1 = 11 - resto
     
                soma2 = ( a*11 + b*10 + c*9 + d*8 + e*7 + f*6 + g*5 + h*4 +i*3 \
                          + d1*2 )
                resto = soma2 % 11
                if resto == 0 or resto == 1:
                    d2 = 0
                else:
                    d2 = 11 - resto
     
                if d1 != int(value[9]) or d2 != int(value[10]):
                    return False
            else:
                return False
        else:
            return False
        
        return True

    def isCNPJ(self, cnpj):
        """Verifica se o cnpj é valido"""
        
        cnpj = ''.join([c for c in cnpj if c.isdigit()])
        if len(cnpj) != 14:
            return False
        elif len(cnpj) == 14:
            vtemp = [int(cnpj[:1]) for i in list(cnpj[:8])]
            cnpj2 = [int(i) for i in list(cnpj[:8])]
  
            if cnpj2 == vtemp:
                return False
  
            tmp = cnpj[:12]
            ltmp = [int(i) for i in list(tmp)]
            temp = [int(i) for i in list(cnpj)]
            prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
  
            while len(ltmp) < 14:
                R = sum([x*y for (x, y) in zip(ltmp, prod)])%11
                if R >= 2:
                    f = 11 - R
                else:
                    f = 0
                ltmp.append(f)
                prod.insert(0,6)
            if temp != ltmp:
                return False
        return True