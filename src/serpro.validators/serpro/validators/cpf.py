# -*- coding: utf-8 -*-

import re

from Products.validation.interfaces.IValidator import IValidator

from serpro.validators.config import PLONE_VERSION

class CPFValidator:
    """Verifica se o valor informado é um CPF válido
    """

    if PLONE_VERSION <= 3:
        __implements__ = IValidator

    if PLONE_VERSION > 3:
        from zope.interface import implements
        implements(IValidator)

    def __init__(self, name):
        self.name = name
        self.default_invalid_message = "'%s' não é um número de CPF válido"

    def __call__(self, value, *args, **kwargs):
        value = self._removeMaskChars(value)
        return self.isCPF(value)

    def _removeMaskChars(self, value):
        """Remove caracteres tradicionalmente utilizados como máscara.
        Remove os caracteres -. que estiverem na string.
        """
        regex = re.compile("[-\.]")
        return re.sub(regex, "", value)

    def isCPF(self, value):
        """Verifica se é um CPF válido"""
        if value.isdigit():
            if len(value) == 11:
                # valida se for CPF
                a = int(value[0]) 
                b = int(value[1])
                c = int(value[2])
                d = int(value[3])
                e = int(value[4])
                f = int(value[5])
                g = int(value[6])
                h = int(value[7])
                i = int(value[8])
                j = int(value[9])
                k = int(value[10])
    
                if a == b == c == d == e == f == g == h:
                    return 'Sequência inválida: %s' % (self.default_invalid_message % str(value))

                if k == j == i == h == g == f == e == d:
                    return 'Sequência inválida: %s' % (self.default_invalid_message % str(value))
    
                soma1 = ( a*10 + b*9 + c*8 + d*7 + e*6 + f*5 + g*4 + h*3 + i*2 )
                resto = soma1 % 11
                if resto == 0 or resto == 1:
                    d1 = 0
                else:
                    d1 = 11 - resto
     
                soma2 = ( a*11 + b*10 + c*9 + d*8 + e*7 + f*6 + g*5 + h*4 +i*3 \
                          + d1*2 )
                resto = soma2 % 11
                if resto == 0 or resto == 1:
                    d2 = 0
                else:
                    d2 = 11 - resto
     
                if d1 != int(value[9]) or d2 != int(value[10]):
                    return self.default_invalid_message % str(value)
            elif len(value) < 11:
                return 'Dígitos a menos: %s' % (self.default_invalid_message % str(value))
            else:
                return 'Dígitos a mais: %s' % (self.default_invalid_message % str(value))
        else:
            return self.default_invalid_message % str(value)
        return True
