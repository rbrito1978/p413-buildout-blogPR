# -*- coding: utf-8 -*-

from types import StringType

# Interfaces obtidas do produto archetypes.schemaextender, que irá
# fazer toda a "mágica" de estender o schema com novos validadores
# sem customizá-lo
from archetypes.schemaextender.interfaces import ISchemaModifier, \
    IBrowserLayerAwareExtender

from Products.validation import ValidationChain, V_REQUIRED, V_SUFFICIENT

from zope.interface import implements

# ATENÇÂO: As classes de validadores são chamadas TRÊS VEZES, portanto cuidado
# com a implementação inserida nessa utilização do archetypes.schemaextender.
class ValidatorsModifier(object):
    """Estende o schema adicionando um validador.

    Essa classe pegará as outras classes definidas em schema.py, que
    herdarão de ValidatorsModifier, geralmente uma classe para cada
    interface que se deseja modificar o schema.

    Assim, irá estender o schema das interfaces adaptadas, baseado no(s)
    field(s) e no(s) validador(es) customizado(s) escolhido(s).

    A variável 'field_validators' presente nas classes que adaptam às
    interfaces é um dicionário que contém o(s) field(s), o(s) validadore(s)
    para aquele(s) field(s) e o modo (sufficient ou required).

    Não se esqueça de, no momento que definir uma classe em seu schema.py que
    herde de ValidatorsModifier, de definir o atributo 'layer'.

    """

    # Não esqueça de implementar a interface IBrowserLayerAwareExtender,
    # ou essa alteração no schema afetará TODOS os Plone site de uma
    # mesma instância do seu Zope!!!
    implements(IBrowserLayerAwareExtender, ISchemaModifier)

    # Essa layer deve ser adicionada para quem utilizar esse módulo.
    # layer = IThemeSpecific

    def __init__(self, context):
        self.context = context

    def _validateValidatorsStructure(self, field):
        """Valida a estrutura de validadores que serão estendidos no schema.

        Os validadores tem de ser uma 'tupla de tuplas':

            {'field_name': (('validator_name', mode),)}

        ou

            {'field_name': (('validator_name', mode),('validator_name', mode))}

        isso é necessário para poder indicar o modo do validador, se é
        V_REQUIRED ou V_SUFFICIENT. Esse método poderia ser mais 'conivente'
        com vários formatos, mas como schema.py altera os schemas, é
        interessante cobrar o mesmo formato sempre para evitar problemas."""
        valid_modes = [V_REQUIRED, V_SUFFICIENT]
        # Independentemente de ter apenas um validador, tem de ter uma tupla
        # dentro de outra tupla.
        if isinstance(self.field_validators[field], tuple):
            for validator in self.field_validators[field]:
                # Tem de ter dois campos: o nome do validador e o modo.
                if not len(validator) == 2:
                    return False
                else:
                    # Já testou se tem 2 campos, testa se o primeiro é string
                    # (o nome do validador) e se o segundo é um modo válido
                    if (not isinstance(validator[0], StringType) or
                        not validator[1] in valid_modes):
                        return False
            return True
        return False

    def fiddle(self, schema):
        for field in self.field_validators.keys():
            if not self._validateValidatorsStructure(field):
                structure = "{'field_name': (('validator_name', mode),)}"
                msg = 'Invalid validators structured! "%s" needed.'
                raise Exception('%s%s' % (msg, structure))

            # Checa se o schema realmente possui esse field. Importante para
            # caso o implementador digite um id incorreto nas classes, evite
            # "corromper" um schema com um field inexistente.
            if schema.get(field, None) is not None:

                # Necessário copiar o field do schema. Veja
                # archetypes.schemaextender-2.0.4-py2.4.egg/archetypes/schemaextender/interfaces.py
                # Se isso não for feito (a cópia do field), a alteração do
                # field estará presente em TODOS os Plone Site de uma mesma
                # instância, independente de ter uma browserlayer ou não.
                f = schema[field].copy()

                # Por ser um dicionário, o(s) validadore(s) são o valor da chave
                # do próprio field.
                validators = self.field_validators[field]

                # Essa situação parte da premissa de que esse field já possui
                # validadores, e você está apenas adicionando mais um.
                try:
                    # Para todas as formas diferentes de adição de validadores
                    # (insert, insertRequired, append, etc) veja o módulo
                    # Products.validation-1.6.4-py2.4.egg/Products/validation/chain.py

                    # Validadores já presentes no schema.
                    schema_validators = f.validators[:]

                    # Pega todos os nomes dos validadores já registrados para
                    # esse field.
                    v_names = []
                    if len(schema_validators) > 0:
                        for r_validator in schema_validators[0]:
                            v_names.append(r_validator.name)

                    for validator in validators:
                        # Não há nenhum validador.
                        if len(schema_validators) == 0:
                            f.validators.append(validator[0], validator[1])
                        else:
                            # Já há validadores, portanto verifique se os que
                            # você vai cadastrar já não existem em
                            # field.validators.
                            # É importante ressaltar que campos não requeridos
                            # possuem os seguintes problemas quando não estão
                            # sendo editados através do base_edit:
                            # http://dev.plone.org/ticket/7589
                            # http://plone.293351.n2.nabble.com/Inline-validation-doesn-t-work-when-required-False-in-plone-3-1-1-td337845.html
                            # http://comments.gmane.org/gmane.comp.web.zope.plone.archetypes.general/89
                            # http://copilotco.com/mail-archives/plone-users.2008/msg08110.html
                            # http://opensourcehacker.com/2008/06/12/plone-kss-javascript-field-validation-and-the-cup-of-wtf/
                            # http://copilotco.com/mail-archives/plone-developers.2007/msg02861.html
                            # http://plone-regional-forums.221720.n2.nabble.com/dzug-zope-eigenen-validator-in-archetypes-td224825.html
                            if validator not in v_names:
                                # Há uma situação interessante: um campo não
                                # requerido tem como primeiro validador, 'isEmpty'
                                # como V_SUFFICIENT, ou seja, se estiver vazio, ok
                                # pois ele não é requerido. Acontece que há
                                # situações em que você preenche um campo, e assim
                                # torna o outro dinamicamente necessário (exemplo,
                                # numa notícia, a imagem não é obrigatória, mas se
                                # você adicionar alguma, obrigatoriamente tem de
                                # adicionar uma legenda): nessa situação, como o
                                # isEmpty é V_SUFFICIENT, se ele passa por esse
                                # validador, não vai pro outro que indique essa
                                # obrigatoriedade, portanto preciso colocar o meu
                                # validador antes dele.
                                if getattr(self, 'conditional', False):
                                    f.validators.insert(validator[0], validator[1])
                                else:
                                    f.validators.append(validator[0], validator[1])

                # Ainda não possui nenhum validador.
                except AttributeError:
                    # Não possui ainda um "ValidationChain" em 'validators' e sim
                    # apenas uma tupla; Uma tupla não possui o método
                    # 'append', portanto necessita instanciar um novo
                    # "ValidationChain".

                    # Agradecimento ao pdb e ao link
                    # https://weblion.psu.edu/trac/weblion/changeset/871
                    # por ter dado uma luz sobre essa situação.

                    # O nome do chain_name está sendo criado tomando como base
                    # a lógica em Products.Archetypes.Field
                    chain_name = 'Validator_%s' % (field)

                    # Todos serão adicionados seguindo o modo (required ou
                    # sufficiente).
                    f.validators = ValidationChain(chain_name,
                        validators=validators)

                schema[field] = f
            else:
                msg = "This schema doesn't have the field %s" % (field)
                raise Exception(msg)


class RequiredModifier(object):
    """Estende o schema definindo um campo como required.

    Utilize essa classe para poder falar que um campo é requerido. Exemplo:
    campo título do tipo File.

    Essa classe pegará as outras classes definidas em schema.py, que
    herdarão de RequiredModifier, geralmente uma classe para cada
    interface que se deseja modificar o schema.

    Assim, irá estender o schema das interfaces adaptadas, baseado no(s)
    field(s) escolhidos.

    Não se esqueça de, no momento que definir uma classe em seu schema.py que
    herde de RequiredModifier, de definir o atributo 'layer'.

    """

    # Não esqueça de implementar a interface IBrowserLayerAwareExtender,
    # ou essa alteração no schema afetará TODOS os Plone site de uma
    # mesma instância do seu Zope!!!
    implements(IBrowserLayerAwareExtender, ISchemaModifier)

    required = True

    def __init__(self, context):
        self.context = context

    def fiddle(self, schema):
        for field in self.fields:
            # Checa se o schema realmente possui esse field. Importante para
            # caso o implementador digite um id incorreto nas classes, evite
            # "corromper" um schema com um field inexistente.
            if schema.get(field, None) is not None:
                # Necessário copiar o field do schema. Veja
                # archetypes.schemaextender-2.0.4-py2.4.egg/archetypes/schemaextender/interfaces.py
                # Se isso não for feito (a cópia do field), a alteração do
                # field estará presente em TODOS os Plone Site de uma mesma
                # instância, independente de ter uma browserlayer ou não.
                f = schema[field].copy()
                f.required = self.required
                schema[field] = f
            else:
                msg = "This schema doesn't have the field %s" % (field)
                raise Exception(msg)


class NotRequiredModifier(RequiredModifier):
    """Estende o schema definindo um campo como required."""
    required = False


class HiddenModifier(object):
    """Estende o schema escondendo os campos definidos.

    Utilize essa classe para esconder um campo do schema. Exemplo:
    campo título do tipo File.

    Essa classe pegará as outras classes definidas em schema.py, que
    herdarão de HiddenModifier, geralmente uma classe para cada
    interface que se deseja modificar o schema.

    Assim, irá estender o schema das interfaces adaptadas, baseado no(s)
    field(s) escolhidos.

    Não se esqueça de, no momento que definir uma classe em seu schema.py que
    herde de RemoveModifier, de definir o atributo 'layer'.

    """

    # Não esqueça de implementar a interface IBrowserLayerAwareExtender,
    # ou essa alteração no schema afetará TODOS os Plone site de uma
    # mesma instância do seu Zope!!!
    implements(IBrowserLayerAwareExtender, ISchemaModifier)

    visible = 'invisible'

    def __init__(self, context):
        self.context = context

    def fiddle(self, schema):
        for field in self.fields:
            # Checa se o schema realmente possui esse field. Importante para
            # caso o implementador digite um id incorreto nas classes, evite
            # "corromper" um schema com um field inexistente.
            if schema.get(field, None) is not None:
                # Necessário copiar o field do schema. Veja
                # archetypes.schemaextender-2.0.4-py2.4.egg/archetypes/
                # schemaextender/interfaces.py
                # Se isso não for feito (a cópia do field), a alteração do
                # field estará presente em TODOS os Plone Site de uma mesma
                # instância, independente de ter uma browserlayer ou não.
                f = schema[field].copy()
                f.widget.visible = {'view': self.visible,
                                    'edit': self.visible}
                schema[field] = f
            else:
                msg = "This schema doesn't have the field %s" % (field)
                raise Exception(msg)


class ShowModifier(HiddenModifier):
    """Estende o schema mostrando os campos definidos."""

    visible = 'visible'
