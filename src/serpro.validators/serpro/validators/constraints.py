# -*- coding: utf-8 -*-

from zope import schema
from serpro.validators import MessageFactory as _


class SoEspacosErro(schema.ValidationError):
    __doc__ = _(u"Only spaces are not allowed.")


def verifica_espacos(value):
    """Constraint que verifica se um valor é composto somente de espaços"""

    if not value.strip():
        raise SoEspacosErro
    return True
