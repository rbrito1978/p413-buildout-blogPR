# -*- coding: utf-8 -*-

"""Validador condicional: há situações em que um campo só deve ser validado
caso outro campo, da mesma instância, tenha sido preenchido (exemplo: num
objeto notícia, o campo "legenda da imagem" só deve ser obrigatório caso
o campo de imagem esteja preenchido).

O módulo apenas verifica se o field dependente está vazio ou não: caso
necessite de condições muito específicas (o valor preenchido deve seguir alguma
regra por exemplo), crie um validador customizado no seu produto levando como
base esse validador.
"""

from Products.validation.interfaces.IValidator import IValidator

from serpro.validators.config import PLONE_VERSION

from ZPublisher.HTTPRequest import FileUpload


class ConditionalValidator:
    """Verifica se um field passado por parâmetro de uma instância foi
    preenchido ou não, e se tiver sido preenchido, obriga que um outro field
    associado possua um valor."""

    if PLONE_VERSION <= 3:
        __implements__ = IValidator

    if PLONE_VERSION > 3:
        from zope.interface import implements
        implements(IValidator)

    def __init__(self, name, title='', description='', dependable_fields=None,
            custom_function=None, errormsg=''):
        self.name = name
        self.title = title or name

        if dependable_fields is None:
            raise Exception("You need to pass at least a field as argument")

        if not isinstance(dependable_fields, list):
            dependable_fields = [dependable_fields]
        self.dependable_fields = dependable_fields

        self.description = description

        if errormsg:
            self.msg = errormsg
        else:
            self.msg = u"""Campo(s) é(são) obrigatório(s) se o(s) campo(s) '%s'
                não for(em) vazio(s)! Se o campo '%s' for um arquivo que você
                escolhe da sua máquina, você precisará anexá-lo novamente!"""

    def __call__(self, value, *args, **kwargs):
        instance = kwargs.get('instance', None)
        request = kwargs.get('REQUEST', None)
        if request is not None:
            invalid_fields = []
            # Fields obrigatorios que influenciam outros
            for field in self.dependable_fields:
                field_obj = self._getFieldObjFromRequest(request, field)
                # Se o campo necessário está preenchido:
                if not self._isEmpty(field_obj):
                    # E o campo associado NÃO está preenchido, então mostre o
                    # erro:
                    if not value or value == '' or value is None:
                        label = self._getFieldLabelTranslated(instance, field)
                        invalid_fields.append(label)
                    else:
                        return True

            if not invalid_fields:
                return True

            invalid_fields_str = ', '.join(invalid_fields)
            return self.msg % (invalid_fields_str, invalid_fields_str)
        #TODO: Colocar kss nessa secao.

    def _getFieldObjFromRequest(self, request, field):
        """Obtem o field dependente do request, com correções caso o field seja
        do tipo File: nessa situação, geralmente são adicionados sufixos como
        '_file', '_delete', etc."""
        field_obj = request.get(field, None)
        if field_obj is None:
            # O field é do tipo arquivo, e nessa situação, é dado um
            # 'append' no nome mas com '_file' se estiver sendo
            # adicionado.
            field_name = field + '_file'
            field_obj = request.get(field_name, None)
            if field_obj is None:
                # O field deve ser do tipo arquivo, mas está sendo
                # editado, portanto recebe '_delete'.
                field_name = field + '_delete'
                field_obj = request.get(field_name, None)
                if field_obj is None:
                    raise Exception("Can't get the field!")
        return field_obj

    def _isEmpty(self, field):
        """Retorna se o valor do field não é vazio."""
        if isinstance(field, FileUpload):
            return not field.__nonzero__()
        else:
            if field is not None and field != '':
                return False
        return True

    def _getFieldLabelTranslated(self, instance, field_id):
        """Retorna o título (label) de um field."""
        field_id = field_id.replace('_file', '')
        label = instance.Schema()._fields[field_id].widget.Label(instance)
        return instance.translate(label)
