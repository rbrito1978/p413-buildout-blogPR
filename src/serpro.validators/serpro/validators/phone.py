# -*- coding: utf-8 -*-
"""Validadores de vários tipos de telefones.

Possui variações brasileiras (como uso de DDD) opcionais.
"""
import re

from Products.validation.interfaces.IValidator import IValidator


class PhoneValidator:
    """Validador de telefones.

    Se for passado um valor com caracteres geralmente utilizados em
    máscaras (como ., (, ), -) eles serão removidos de forma transparente
    a validação seguirá normalmente.

    Possui variações brasileiras, que, de acordo com o name passado pelo
    registro do validador (Products.validation.validation.register), define
    qual variação será utilizada. Lembrando que "isPhoneNumber" é sempre
    executada, já prevendo apenas números inteiros, e que se nenhum nome
    da variações for passado como parâmetro, apenas "isPhoneNumber" será
    utilizado:

    "isPhoneNumber": Valida se contém apenas inteiros positivos, num tamanho
    razoável (15 caracteres). Esse valor é arbitrário, pois esse validador
    validará qualquer tipo de telefone de qualquer país;

    "isBrazilDDD": Verifica dois números inteiros positivos, tirando o 00.
    No futuro, poderá existir uma tabela com os valores válidos.

    "isBrazilPhoneNumber": Verifica oito números inteiros positivos, tirando
    00000000. No futuro, poderá existir uma tabela com os ranges válidos,
    inclusive por DDD.

    "isBrazilPhoneNumberDDD": Verifica dez números inteiros positivos, tirando
    0000000000. No futuro, poderá existir uma validação dessa variação que leve
    em conta isBrazilDDD e isBrazilPhoneNumber.
    """

    __implements__ = IValidator

    def __init__(self, name, title='', description=''):
        self.name = name
        self.title = title or name
        self.description = description
        self.variations = ['isPhoneNumber', 'isBrazilDDD',
            'isBrazilPhoneNumber', 'isBrazilPhoneNumberDDD']
        self.default_invalid_message = "'%s' não é um número de telefone válido"

    def __call__(self, value, *args, **kwargs):
        value = self._removeMaskChars(value)
        if self.name not in self.variations:
            return self.isPhoneNumber(value)
        else:
            variation = getattr(self, self.name)
            return variation(value)

    def _removeMaskChars(self, value):
        """Remove caracteres tradicionalmente utilizados como máscara.

        Remove os caracteres ()-. que estiverem na string.
        """
        regex = re.compile("[\(\)-\.]")
        return re.sub(regex, "", value)

    def isPhoneNumber(self, value):
        """Valida se o número e inteiro positivo e com até 15 algarismos.

        Esse valor é arbitrário, pois esse validador validará qualquer tipo de
        telefone de qualquer país.
        """

        # Verifica se é inteiro.
        try:
            value = int(value)
        except ValueError:
            return("'%s' não é um inteiro" % str(value))

        # Verifica se não é negativo.
        if value < 0:
            return("'%s' é um número negativo" % str(value))
        # Verifica se usuário não colocou '00000000'
        elif value == 0:
            return(self.default_invalid_message % str(value))
        # Verifica se o usuário colocou mais de 15 algarismos inteiros.
        elif len(str(value)) > 15:
            return(self.default_invalid_message % str(value))
        else:
            return 1

    def isBrazilDDD(self, value):
        """Valida se o número é um DDD válido."""
        msg = self.isPhoneNumber(value)
        if msg == 1:
            # DDD possui apenas dois algarismos.
            if len(value) == 2:
                return 1
        else:
            return msg
        return self.default_invalid_message % value

    def isBrazilPhoneNumber(self, value):
        """Verifica se é um número de telefone válido para padrões brasileiros.
        """
        msg = self.isPhoneNumber(value)
        if msg == 1:
            if len(value) == 10:
                return 1
        else:
            return msg
        return self.default_invalid_message % value

    def isBrazilPhoneNumberDDD(self, value):
        """Verifica se é um número de telefone válido para padrões brasileiros,
        com utilização do DDD"""
        msg = self.isPhoneNumber(value)
        if msg == 1:
            # Verifica o DDD e a parte apenas do telefone, impedindo por ex.
            # 3100000000
            if self.isBrazilDDD(value[:2]) == 1 and \
                self.isPhoneNumber(value[2:]):
                # Verifica o tamanho do telefone.
                if len(value) == 10:
                    return 1
        else:
            return msg
        return self.default_invalid_message % value
