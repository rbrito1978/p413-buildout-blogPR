INTRODUÇÃO
==========
Esse módulo tem o objetivo de fornecer validadores customizados
adicionais para utilização nos portais do Serpro.

Além disso, fornece informações sobre como evitar customização
de tipos apenas para adicionar um validador customizado, utilizando o
módulo archetypes.schemaextender e o conceito de adapters.

UTILIZAÇÂO DE VALIDADORES DINÂMICOS
===================================

Existem três formas conhecidas de utilização desse módulo:

1 - Com uso do archetypes.schemaextender, adicionando os validadores
utilizando adapters. (Código muito parecido com o produto
collective.ATClamAV e do slc.dublettefinder, é essa abordagem que  
será explicada aqui). Muito útil quando se quer usar os tipos padrão
do Plone, mas com validadores customizados (como restrição de
extensões no tipo Arquivo);

2 - Com uso do evento IObjectPostValidation, utilizando subscribers.
(Essa abordagem não será explicada aqui, favor ler 
https://www.packtpub.com/article/creating-custom-content-type-paster-plone-3)

3 - Utilização normal de registro de validadores, utilizando o
validation.register e inserção do nome do validador no campo
'validators' no field de um content-type. Forma ideal de se utilizar
essa metodologia é para tipos novos em sua pasta content.

Apenas a forma 1 será descrita a seguir. Siga **EXATAMENTE AS
INSTRUÇÕES OU SAIBA MUITO BEM O QUE ESTIVER FAZENDO PARA NÃO
SEGUI-LAS (como não utilizar o IBrowserLayerAwareExtender,
ou criar uma nova classe ValidatorsModifier com suporte a
multiadapters)**, senão você perderá seus dados, seu
trabalho, seu dinheiro, sua esposa/marido e filhos. (Não,
não sou o único a dar esse tipo de aviso. iw.fss veio antes:
http://plone.org/products/filesystemstorage/#caveats-and-pittfalls).

Recomenda-se a abordagem 2 como "backup" caso a 1 não seja possível de
ser implementada por algum motivo específico ao seu projeto.

PS: Há ainda uma outra possível vertente, que seria a alteração
do esquema em tempo de execução, mas não foi testada. Um snippet:
http://collective-docs.readthedocs.org/en/latest/content/archetypes/fields.html#dynamic-field-definitions

AVISO IMPORTANTE ANTES DE SEGUIR O TUTORIAL
===========================================
Os códigos aqui postados de exemplo podem ser copiados, mas devem ser
lidos em **SUA TOTAL INTEGRIDADE** para o real entendimento de sua
utilização e para evitar problemas futuros: praticamente toda linha
está documentada e é essencial que tudo seja lido para que se entenda
como o archetypes.schemaextender trabalha.

Recomenda-se inclusive que tudo seja lido **VÁRIAS VEZES**. No futuro,
poderá ser criada uma template para utilização no paster, mas
enquanto isso não é feito tudo deve ser lido com cuidado.

Resumo:
-------

- Cria-se uma browserlayer para que as alterações no schema sejam
restritas a apenas um Plone site;

- Instala-se o produto archetypes.schemaextender;

- Registram-se os validadores que serão utilizados no __init__.py;

- Cria-se o arquivo schema.py (ou outro nome de sua preferência, é utilizado
schema.py apenas por convenção) que importará a classe que faz as modificações
no esquema (serpro.validators.schema.ValidatorsModifier), e classes que adaptam
interfaces que terão os fields alterados para validadores;

- Edita-se o configure.zcml indicando adapters que são as classes
que adaptam interfaces em schema.py, e a inclusão do package do
archetypes.schemaextender.

INSTRUÇÕES PARA UTILIZAÇÃO DO ARCHETYPES.SCHEMAEXTENDER
=======================================================

Crie um browserlayer.xml no seu profile
---------------------------------------
Não se esqueça de criar um browserlayer.xml em seu profile!
http://plone.org/documentation/kb/customization-for-developers/browser-layers

Entenda a diferença entre uma BrowserSkin e uma BrowserLayer:
http://opkode.net/media/blog/the-difference-between-browserlayers-and-skinlayers

Hoje no Serpro muitas vezes uma BrowserSkin é utilizada ao invés de
uma BrowserLayer, aprenda sobre as diferenças para saber a melhor
forma de utilizar no seu caso.

Lembre-se de que algumas versões do paster NÃO criam o arquivo
browserlayer.xml quando você cria uma skin, portanto, se você
for utilizar a layer da skin fique atento pois a criação terá
de ser manual.

Coloque archetypes.schemaextender em seu buildout
-------------------------------------------------
Veja no site oficial do componente como adicionar em seu buildout:
http://pypi.python.org/pypi/archetypes.schemaextender/2.0.4

Devido a um problema com as últimas versões do módulo
(http://plone.org/products/collective.externalsnippet/issues/3),
utilize a versão 2.0.4 em seu versions.cfg. Esse problema só foi percebido
durante utilização do Plone 3.3.5, a partir do Plone 4.0.X esse problema não
é recorrente.

Registre os validadores que você utilizará no __init__.py
---------------------------------------------------------
Lembrando que '...' diz respeito a configurações não necessárias
para o funcionamento dessa abordagem (resumindo, código próprio que
você possui, irrelevantes para essa documentação).

Geralmente os projetos são feitos como serpro.MEUPROJETO, e será esse
o padrão utilizado durante a explicação.

Coloque esse código em serpro.MEUPROJETO/serpro/MEUPROJETO/__init__.py:

::

    ...
    # -*- coding: utf-8 -*-
    from Products.validation import validation

    # Importe os validadores que você deseja utilizar
    from serpro.validators.phone import PhoneValidator
    from serpro.validators.generic import GenericEspacoValidator

    # Registre os validadores.
    validation.register(PhoneValidator('isBrazilPhoneNumber'))
    validation.register(PhoneValidator('isBrazilPhoneDDD', size=30))
    validation.register(GenericEspacoValidator('isNotCompletelyEmpty'))
    ...

Crie um schema.py com os adapters das interfaces onde serão inseridos
---------------------------------------------------------------------
os validadores
--------------
Crie um serpro.MEUPROJETO/serpro/MEUPROJETO/schema.py. Você irá adicionar:

1 - Uma classe, para cada interface, com o(s) field(s) e
o(s) validador(es) que você deseja adicionar naquele(s)
field(s) (tem de ser um dos validadores já definidos em
serpro.MEUPROJETO/serpro/MEUPROJETO/__init__.py). Se você for utilizar o
validador serpro.validators.conditional, NÃO SE ESQUEÇA de adicionar um atributo
'conditional = True' na sua classe! Para mais detalhes dessa necessidade, leia
os comentários em serpro.validators.ValidatorsModifier;

2 - Uma classe que implementa a interface IBrowserLayerAwareExtender e
ISchemaModifier, que irá pegar as classes que você definiu no passo 1, e irá
estender os validadores ao(s) field(s) definido(s). No caso, você pode
importar serpro.validators.schema.ValidatorsModifier e criar uma classe nova
herdando de ValidatorsModifier, ou mesmo criar uma classe própria, com suas
próprias regras. Só será necessário você criar uma classe própria numa situação
MUITO específica, serpro.validators.schema.ValidatorsModifier atende
praticamente 99% dos casos de validação.

A seguir um código completo sobre a estrutura do arquivo. Copie e
cole no schema.py do seu projeto, e leia **CADA LINHA** para entender
o funcionamento, e **NÃO ESQUEÇA** de atribuir ao atributo 'layer'
da classe a sua browserlayer, senão você perderá seus dados, seu
trabalho, seu dinheiro, seu...

::

    # -*- coding: utf-8 -*-

    from Products.validation import V_REQUIRED

    from zope.component import adapts

    # Importe suas interfaces: podem ser do próprio sistema como
    # from Products.ATContentTypes.interface.ATINTERFACE import IATINTERFACE
    # ou do seu produto:
    from serpro.MEUPROJETO.interfaces.MINHAINTERFACE import IMINHAINTERFACE

    # Esse nome de uma browserlayer geralmente é o padrão, "IThemeSpecific".
    # Lembre-se de alterar de acordo com as características do seu módulo
    # caso seja necessário!
    from serpro.MEUPROJETOSKIN.browser.interfaces import IThemeSpecific

    # Classe pronta para adicionar validadores dinâmicos.
    from serpro.validators.schema import ValidatorsModifier

    # Essa classe aparece apenas uma vez.
    # ATENÇÂO: As classes de validadores são chamadas TRÊS VEZES, portanto cuidado
    # com a implementação inserida nessa utilização do archetypes.schemaextender.
    class ValidatorsModifierMEUPROJETO(ValidatorsModifier):
        """Estende o schema adicionando um validador.

        Essa classe pegará as outras classes definidas em schema.py, que
        herdarão de ValidatorsModifierMEUPROJETO, geralmente uma classe para
        cada interface que se deseja modificar o schema.

        Assim, irá estender o schema das interfaces adaptadas, baseado no(s)
        field(s) e no(s) validador(es) customizado(s) escolhido(s).

        A variável 'field_validators' presente nas classes que adaptam às
        interfaces é um dicionário que contém o(s) field(s), o(s) validadore(s)
        para aquele(s) field(s) e o modo (sufficient ou required).
        """

        layer = IThemeSpecific


    # Essa classe aparece de novo para CADA INTERFACE que você deseja adaptar.

    # ATENÇÃO: adapts(Interface1, Interface2) não funciona nessa estrutura!
    # se necessitar dessa estrutura, veja "PersonBioForm" em
    # http://garbas.github.com/plone-z3c.form-tutorial/plone_z3cform.html
    # e crie sua própria classe ValidatorsModifier.
    # Se quiser adaptar a todos os content-types, utilize a interface
    # Products.CMFCore.interfaces.IContentish

    # CUIDADO: Herde de ValidatorsModifierMEUPROJETO e não apenas de  
    # ValidatorsModifier! É ValidatorsModifierMEUPROJETO que possui a layer!
    class ValidatorsMINHAINTERFACEModifier(ValidatorsModifierMEUPROJETO):
        """Define a interface do tipo que terá validador(es) adicionado(s) a field(s).

        Através do uso de adapters, conterá a definição de qual interface será
        adaptada e qual(is) validador(es) em determinado(s) field(s).

        'field_validators' é um dicionário, com a chave sendo o id do field e o
        valor sendo OBRIGATORIAMENTE uma tupla com outras tuplas, contendo os
        validadores e o modo (V_REQUIRED ou V_SUFFICIENT).

        Se essa estrutura não for obedecida, será lançada uma exceção.
        """
        # Não esqueça do configure.zcml... é uma entrada '<adapter' para
        # cada classe como essa que herdar de ValidatorsModifier!
        adapts(IMINHAINTERFACE)
        # Se for utilizar o validador 'conditional', você DEVE setar a
        # propriedade 'conditional = True'!
        # conditional = True
        field_validators = {'telefone': (('isBrazilPhoneNumberDDD', V_REQUIRED),)}


Edite serpro.MEUPROJETO/serpro/MEUPROJETO/configure.zcml para indicar o uso
---------------------------------------------------------------------------
do archetypes.schemaextender e registrar os adapters definidos em schema.py
---------------------------------------------------------------------------

::

    <include package="archetypes.schemaextender" />

    <!-- ADAPTERS PARA VALIDAÇÃO
        CRIE UM ADAPTER PARA CADA INTERFACE ADAPTADA. -->

    <!-- O nome MINHAINTERFACE não é necessariamente obrigatório: na verdade, 
    aqui poderia ser um valor arbitrário, mas é interessante manter o
    padrão de nomes, inclusive para manutenção. -->
    <adapter
        provides="archetypes.schemaextender.interfaces.ISchemaModifier"
        name="serpro.MEUPROJETO.MINHAINTERFACEValidator"
        factory="serpro.MEUPROJETO.schema.ValidatorsMINHA_INTERFACEModifier" />

    <!-- FIM ADAPTERS VALIDAÇÃO -->

REFERÊNCIAS PARA CRIAÇÂO DESSE MÓDULO/README
============================================

Esse solução não "brotou do nada": muita dedicação, esforço,
documentação e pesquisa com todo o tipo de combinação no google foi
necessária para você ter esse documento em mãos. "Use the source,
Luke" foi o principal lema dessa documentação. Portanto, esse código 
foi todo bem documentado, e muitas dúvidas podem ser sanadas bastando
ler o código. Esse documento agora também é seu, edite-o e melhore-o
para cada inconsistência/melhoria que perceber.

Com relação à pesquisas, uma das consultas mais eficientes durante o
perído de pesquisas foi

    "archetypes.schemaextender" custom validators

que foi pensada após muita leitura de código fonte.

archetypes.schemaextender
-------------------------
http://pypi.python.org/pypi/archetypes.schemaextender
http://www.upfrontsystems.co.za/Members/hedley/my-random-musings/archetypes-schema-extender-with-ibrowserlayerawareextender-example
http://plone.org/products/collective.atclamav
http://plone.org/products/slc.dublettefinder/releases/0.3.1

Possíveis problemas archetypes.schemaextender
---------------------------------------------
http://plone.293351.n2.nabble.com/archetypes-schemaextender-limitations-td354881.html
http://plone.293351.n2.nabble.com/archetype-schemaextender-problem-can-we-avoid-others-td6012149.html
http://plone.293351.n2.nabble.com/archetypes-schemaextender-Aweful-performance-degradation-td2091791.html
http://plone.org/products/collective.externalsnippet/issues/3
http://plone.293351.n2.nabble.com/Product-Developers-Limiting-schema-extenders-to-one-Plone-site-td357386.html
https://weblion.psu.edu/trac/weblion/changeset/871
http://nullege.com/codes/show/src@plonetheme.twinapex-1.0.5@plonetheme@twinapex@extender.py

Alternativa com uso de IObjectPostValidation, em subscribers
------------------------------------------------------------
https://www.packtpub.com/article/creating-custom-content-type-paster-plone-3

Validators: referências e tutoriais
-----------------------------------
http://plone.org/products/archetypes/documentation/old/quickref#validators
http://plone.org/documentation/manual/developer-manual/archetypes/fields/validator-reference
http://play.pixelblaster.ro/blog/archive/2006/08/27/creating-an-archetypes-validator
http://www.pererikstrandberg.se/blog/index.cgi?page=PloneArchetypesFieldValidator
http://blog.isotoma.com/2007/01/archetypes-validation-on-the-basis-of-the-value-of-more-than-one-field/

Registrando com local adapters
------------------------------
http://web.npowerseattle.org/devteam/web/plone-how-to/archetypes.schemaextender

Adapters
--------
http://plone.org/documentation/manual/developer-manual/archetypes/appendix-practicals/b-org-creating-content-types-the-plone-2.5-way/a-whirlwind-tour-of-zope-3
http://garbas.github.com/plone-z3c.form-tutorial/plone_z3cform.html
