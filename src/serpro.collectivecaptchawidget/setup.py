from setuptools import setup, find_packages
import os

version = '1.4.1'

setup(name='serpro.collectivecaptchawidget',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone :: 4.0",
        "Framework :: Plone :: 4.1",
        "License :: Other/Proprietary License",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        ],
      keywords='',
      author='',
      author_email='',
      url='',
      license='SERPRO',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'collective.captcha',
          'skimpyGimpy',
          'plone.keyring',
          'plone.z3cform',
          'zope.app.form',
          'zope.formlib',
          'zope.browserpage',
          'plone.app.registry',
      ],
      extras_require={'test': ['plone.app.testing']},
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      #setup_requires=["PasteScript"],
      paster_plugins=["ZopeSkel"],
      )
