# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger('serpro.collectivecaptchawidget')

from collective.captcha.browser import captcha
from collective.captcha.browser.captcha import Captcha
import os.path
from Globals import package_home
from skimpyGimpy import skimpyAPI
import random
from Acquisition import aq_inner
import sys

from zope.app.component.hooks import getSite


def _url(self, type):
    """Incluindo um random para sempre recarregar a imagem"""
    try:
        url = aq_inner(self.context).absolute_url()
    except AttributeError:
        #Ocorre quando se utiliza zope.formlib.
        url = getSite().absolute_url()

    return '%s/@@%s/%s?r=%s' % (
        url, self.__name__, type, random.randrange(sys.maxint))

def _audio(self):
    """Generate a captcha audio file"""
    self._verify_session()
    self._setheaders('audio/wav')
    _package_home = package_home(globals())
    if self.request.get('LANGUAGE','pt-br') == 'pt-br':
        WAVSOUNDS = os.path.join(_package_home, 'waveIndex-pt-br.zip')
    else:
        WAVSOUNDS = os.path.join(_package_home, 'waveIndex.zip')

    return skimpyAPI.Wave(self._generate_words()[0], WAVSOUNDS).data()

def applyPatches():
    """Aplicando patches"""

    # Aplicando patch no modulo collective/captcha/browser/captcha para possibilitar uso de audio em pt-br
    if Captcha is not None:
        logger.info('Patching collective.captcha.browser.captcha.Captcha.audio')
        Captcha.audio = _audio
        logger.info('Patching collective.captcha.browser.captcha.Captcha._url')
        Captcha._url = _url
        logger.info('Patching collective.captcha.browser.captcha.WORDLENGTH')
        captcha.WORDLENGTH = 5


