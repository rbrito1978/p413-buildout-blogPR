# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger('serpro.collectivecaptchawidget')

from Acquisition import aq_inner
from zope.interface import implements, Interface

from zope.schema.interfaces import IField
from zope.component import adapts, queryUtility, getMultiAdapter

from plone.registry.interfaces import IRegistry



# plone.app.discussion está presente na instância?
HAS_DISCUSSION = False
try:
    from plone.app.discussion.browser.captcha import CaptchaExtender
    from plone.app.discussion.browser.validator import CaptchaValidator
    from plone.app.discussion.interfaces import ICaptcha
    from plone.app.discussion.interfaces import IDiscussionSettings
    from plone.app.discussion.interfaces import IDiscussionLayer
    HAS_DISCUSSION = True
except ImportError:
    pass


##################################################


def _update(self):
    if self.captcha != 'disabled' and self.isAnon:
        # Add a captcha field if captcha is enabled in the registry
        self.add(ICaptcha, prefix="")
        if self.captcha == 'captcha':
            from plone.formwidget.captcha import CaptchaFieldWidget
            self.form.fields['captcha'].widgetFactory = CaptchaFieldWidget
        elif self.captcha == 'recaptcha':
            from plone.formwidget.recaptcha import ReCaptchaFieldWidget
            self.form.fields['captcha'].widgetFactory = ReCaptchaFieldWidget
        elif self.captcha == 'norobots':
            from collective.z3cform.norobots import NorobotsFieldWidget
            self.form.fields['captcha'].widgetFactory = NorobotsFieldWidget
        elif self.captcha == 'serpro':
            from serpro.collectivecaptchawidget import CaptchaFieldWidget
            self.form.fields['captcha'].widgetFactory = CaptchaFieldWidget
        else:
            self.form.fields['captcha'].mode = interfaces.HIDDEN_MODE




##################################################

try:
    from collective.z3cform.norobots.validator import WrongNorobotsAnswer
except ImportError:
    pass

try:
    from plone.formwidget.captcha.validator import WrongCaptchaCode
except ImportError:
    pass

try:
    from plone.formwidget.recaptcha.validator import WrongCaptchaCode
except ImportError:
    pass

from serpro.collectivecaptchawidget.captcha_validator import CaptchaError

def _validate(self, value):
    super(CaptchaValidator, self).validate(value)

    registry = queryUtility(IRegistry)
    settings = registry.forInterface(IDiscussionSettings, check=False)

    if settings.captcha in ('captcha', 'recaptcha', 'norobots','serpro'):
        if settings.captcha == 'serpro':
            view_name = 'captcha'
        else:
            view_name = settings.captcha
            
        captcha = getMultiAdapter((aq_inner(self.context), self.request),
                                  name=view_name)
        if not captcha.verify(input=value):
            if settings.captcha == 'norobots':
                raise WrongNorobotsAnswer
            elif settings.captcha == 'serpro':
                raise CaptchaError
            else:
                raise WrongCaptchaCode
        else:
            return True


##################################################



def applyPatches():
    """Aplicando patches"""
    if HAS_DISCUSSION:
        logger.info('Patching plone.app.discussion.browser.captcha.CaptchaExtender.update')
        CaptchaExtender.update = _update
        logger.info('Patching plone.app.discussion.browser.validator.CaptchaValidator.validate')
        CaptchaValidator.validate = _validate
    