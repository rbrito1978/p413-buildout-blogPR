# -*- coding: utf-8 -*-

import collective_captcha
import plone_app_discussion

collective_captcha.applyPatches()
plone_app_discussion.applyPatches()