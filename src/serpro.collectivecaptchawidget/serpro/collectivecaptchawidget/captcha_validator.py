# -*- coding: utf-8 -*-
from Acquisition import aq_inner


from z3c.form import validator
from zope.component import getMultiAdapter
from zope.schema import ValidationError

from serpro.collectivecaptchawidget import _


def is_kss_validation(request):
    """ XXX: Retorna se validação está sendo feita por kss.

    Precisa validar apenas se foi dado um POST e não inline, senão dá erro no
    componente de captcha. Por enquanto, a única forma de impedir a validação
    inline sem esse hack de verificar pela url que efetuou a requisição seria
    para todos os campos, mas como apenas o captcha dá esse problema, a opção
    de perguntar pela url ao invés de impedir a view de kss de ser gerada que
    foi a opção escolhida.

    Solução para todos os campos apresentada em:
    http://talk.quintagroup.com/forums/plone-captchas/911689556
    """
    return ('kss_z3cform_inline_validation' in request.getURL() or
            'kss_formlib_inline_validation' in request.getURL())

def valid_captcha(context, request, value):
    """Retorna se o captcha é válido.

    Por ser um método separado, é possível utilizar a mesma lógica em
    frameworks completamente distintos (como z3c.form e zope.formlib) de
    formulários."""
    captcha = getMultiAdapter((aq_inner(context), request), name='captcha')
    if value:
        if not captcha.verify(value):
            raise CaptchaError
        return True
    raise CaptchaError


class CaptchaError(ValidationError):
    __doc__ = _(u'captcha_error')  # @ReservedAssignment


class CaptchaValidator(validator.SimpleFieldValidator):

    def validate(self, value):
        if is_kss_validation(self.request):
            return True

        super(CaptchaValidator, self).validate(value)
        return valid_captcha(self.context, self.request, value)


class CaptchaValidatorFormlib(object):
    """Validador utilizado para framework de formulários zope.formlib."""

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def validate(self, value):
        if is_kss_validation(self.request):
            return True
        try:
            return valid_captcha(self.context, self.request, value)
        except CaptchaError:
            return False
