# -*- coding: utf-8 -*-
from z3c.form import interfaces
from zope.interface import Interface
from zope import schema
from plone.registry import field

from serpro.collectivecaptchawidget import _


class ISerproCollectiveCaptchaWidget(interfaces.IWidget):
    """Marker interface for the serpro collective captcha widget
    """


class ISerproCollectiveCaptchawidgetSettings(Interface):
    """Configurações gerais do serpro.collectivecaptchawidget
    via plone.registry.
    """

    templates = schema.List(title=_(u"Templates Captcha"),
                            description=_(u"help_templates",
                                          default=u"Templates in which the"
                           " captcha does not appear until two errors occur."),
                              required=False,
                              default=['login',
                                       'login_form',
                                       'login_failed',
                                       'logged_out'],
                              value_type=field.TextLine(title=u"Value"))
