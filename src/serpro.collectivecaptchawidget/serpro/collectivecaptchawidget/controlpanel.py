# -*- coding: utf-8 -*-

from plone.app.registry.browser import controlpanel

from serpro.collectivecaptchawidget import _
from serpro.collectivecaptchawidget.interfaces import \
    ISerproCollectiveCaptchawidgetSettings


class SerproCollectiveCaptchawidgetSettingsEditForm(
                                                controlpanel.RegistryEditForm):

    schema = ISerproCollectiveCaptchawidgetSettings
    label = _(u"Serpro CollectiveCaptchawidget settings")
    description = _(u"""""")

    def updateFields(self):
        super(SerproCollectiveCaptchawidgetSettingsEditForm,
              self).updateFields()

    def updateWidgets(self):
        super(SerproCollectiveCaptchawidgetSettingsEditForm,
              self).updateWidgets()


class SerproCollectiveCaptchawidgetControlPanel(
                                        controlpanel.ControlPanelFormWrapper):
    form = SerproCollectiveCaptchawidgetSettingsEditForm
