# -*- coding: utf-8 -*-

import unittest2 as unittest

from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles

from serpro.collectivecaptchawidget.config import PROJECTNAME
from serpro.collectivecaptchawidget.testing import INTEGRATION_TESTING

from plone.browserlayer.utils import registered_layers

class InstallTest(unittest.TestCase):

    layer = INTEGRATION_TESTING    

    def setUp(self):
        self.portal = self.layer['portal']
        self.qi = self.portal['portal_quickinstaller']

    def test_installed(self):        
        self.assertTrue(self.qi.isProductInstalled(PROJECTNAME))

    def test_dependencies_installed(self):
        #nesse caso so tem uma dependencia
        import collective.captcha
        self.assertTrue(collective.captcha is not None)
    
    def test_browserlayer_installed(self):
        layers = [l.getName() for l in registered_layers()]
        self.assertTrue('ISerproCollectiveCaptchaWidgetLayer' in layers, 'browser layer not installed')

class UninstallTest(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.qi = self.portal['portal_quickinstaller']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.qi.uninstallProducts(products=[PROJECTNAME])

    def test_uninstalled(self):
        self.assertFalse(self.qi.isProductInstalled(PROJECTNAME))


def test_suite():
    return unittest.defaultTestLoader.loadTestsFromName(__name__)