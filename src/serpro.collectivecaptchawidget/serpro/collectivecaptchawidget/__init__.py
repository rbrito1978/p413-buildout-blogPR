# -*- extra stuff goes here -*- 
from zope.i18nmessageid import MessageFactory
_ = MessageFactory('serpro.collectivecaptchawidget')

from serpro.collectivecaptchawidget.captcha_widget import SerproCollectiveCaptchaWidget
from serpro.collectivecaptchawidget.captcha_widget import CaptchaFieldWidget
from serpro.collectivecaptchawidget.captcha_validator import CaptchaValidator
  
def initialize(context):
    """Initializer called when used as a Zope 2 product."""
    # Apply patches
    import patches
