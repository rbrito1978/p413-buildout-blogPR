# -*- coding: utf-8 -*-

import zope.component
import zope.interface
from Acquisition import aq_inner
from z3c.form import widget
from z3c.form.browser import text
from interfaces import ISerproCollectiveCaptchaWidget

# Utilizado quando for zope.formlib.
from zope.app.form.browser import ASCIIWidget
from zope.app.form.interfaces import ConversionError
from zope.browserpage import ViewPageTemplateFile
from serpro.collectivecaptchawidget.captcha_validator import \
    CaptchaValidatorFormlib, CaptchaError


class SerproCollectiveCaptchaWidget(text.TextWidget):
    zope.interface.implementsOnly(ISerproCollectiveCaptchaWidget)

    def captchaImage(self):
        self.captcha = zope.component.getMultiAdapter((aq_inner(self.context),
            self.request), name='captcha')
        return self.captcha.image_tag()

    def captchaAudio(self):
        self.captcha = zope.component.getMultiAdapter((aq_inner(self.context),
            self.request), name='captcha')
        return self.captcha.audio_url()

def CaptchaFieldWidget(field, request):
    """IFieldWidget factory for SerproCollectiveCaptchaWidget."""
    return widget.FieldWidget(field, SerproCollectiveCaptchaWidget(request))


# Solução para framework zope.formlib. Adaptado de
# collective.captcha-1.7-py2.6.egg/collective/captcha/form/widget.py
# e sobrescrito através do configure.zcml. Utiliza a mesma template .pt que os
# demais frameworks de formulários (captcha_widget.pt).
class SerproCollectiveCaptchaWidgetFormlib(ASCIIWidget):
    template = ViewPageTemplateFile('captcha_widget.pt')

    def captchaImage(self):
        self.captcha = zope.component.getMultiAdapter((aq_inner(self.context),
            self.request), name='captcha')
        return self.captcha.image_tag()

    def captchaAudio(self):
        self.captcha = zope.component.getMultiAdapter((aq_inner(self.context),
            self.request), name='captcha')
        return self.captcha.audio_url()

    def __call__(self):
        self.id = self.name
        return self.template()

    def _toFieldValue(self, input):
        # É nessa classe que efetivamente ocorre a validação do campo.
        validator = CaptchaValidatorFormlib(self.context, self.request)
        if not validator.validate(input):
            raise ConversionError(CaptchaError.__doc__)
        return super(SerproCollectiveCaptchaWidgetFormlib, self)._toFieldValue(input)
