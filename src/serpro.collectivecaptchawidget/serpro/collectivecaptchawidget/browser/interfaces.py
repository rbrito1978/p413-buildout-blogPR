# -*- coding: utf-8 -*-
from zope.interface import Interface


class ISerproCollectiveCaptchaWidgetLayer(Interface):
    """ Marker interface that defines a Zope 3 browser layer
    """


class ICollectiveCaptchawidget(Interface):
    """View do serpro.collectivecaptchawidget"""

    def get_templates():  # @NoSelf
        """"Retorna os templates que não devem aparecer o captcha até que
        ocorram 2 erros."""

    def mostra_captcha():  # @NoSelf
        """Retorna True se o usuário já errou o captcha duas vezes.
        A verificação ocorre na sessão do usuário. Caso o usuário não tenha
        errado duas vezes ainda, o número de erros é incrementado.
        """
