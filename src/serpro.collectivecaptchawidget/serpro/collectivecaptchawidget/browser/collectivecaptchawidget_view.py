# -*- coding: utf-8 -*-
from plone.registry.interfaces import IRegistry
from plone.memoize.view import memoize

from Products.Five import BrowserView
from zope.interface import implements
from zope.component import getUtility

from serpro.collectivecaptchawidget.browser.interfaces import \
    ICollectiveCaptchawidget
from serpro.collectivecaptchawidget.interfaces import \
    ISerproCollectiveCaptchawidgetSettings


class CollectiveCaptchawidget(BrowserView):
    implements(ICollectiveCaptchawidget)

    @memoize
    def _get_settings(self):
        """Retorna o settings do produto"""
        registry = getUtility(IRegistry)
        return registry.forInterface(ISerproCollectiveCaptchawidgetSettings)

    @memoize
    def get_templates(self):
        """"Retorna os templates que não devem aparecer o captcha até que
        ocorram 2 erros."""
        return self._get_settings().templates

    def mostra_captcha(self):
        """Retorna True se o usuário já errou o captcha duas vezes.
        A verificação ocorre na sessão do usuário. Caso o usuário não tenha
        errado duas vezes ainda, o número de erros é incrementado.
        """

        # Templates que o captcha só aparece se dois erro ocorrerem
        templates = self.get_templates()

        validar = False
        actual_url = self.request['ACTUAL_URL']
        # Verifica se o template atual está na variável templates da
        # configuração do produto.
        for template in templates:
            if actual_url.find(template) != -1:
                validar = True
                break
        
        # Deve validar se o captcha aparece ou não
        if validar:
            sessao = self.request.SESSION
            num_erros_captcha = sessao.get('num_erros_captcha', None)
            # Verifica se a variável da sessão já exite
            if num_erros_captcha:
                sessao['num_erros_captcha'] = num_erros_captcha + 1
                return True
            else:
                ac_name = self.request.get('__ac_name', None)
                # Se tiver __ac_name no request é porque hove submit no form
                if ac_name:
                    # Se não tiver a variável na sessão cria.
                    sessao['num_erros_captcha'] = 1
                # não tem ac_name, então não foi efetuado um submit e não
                # é contado como erro
                return False
        else:
            return True
