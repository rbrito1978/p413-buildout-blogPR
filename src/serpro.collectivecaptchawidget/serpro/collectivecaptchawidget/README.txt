#################
serpro.collectivecaptchawidget
#################

DE6SW

Contribuidores
##############

Glauter Vilela <glauter.vilela@serpro.gov.br>
Giovanni Monteiro Calanzani <giovanni.calanzani@serpro.gov.br>
Wesley Barroso <wesley.lopes@serpro.gov.br>

Dependências
############

Plone 4.0.9+
collective.captcha 1.5+


Descrição
#########

O produto "serpro.collectivecaptichawidget" foi desenvolvido com objetivo realizar customizações
(patches) no produto "collective.captcha" de maneira a se tornar o produto de captcha padrão/
reaproveitável utilizado pelo setor nos novos projetos em Plone 4. 

O "serpro.collectivecaptchawidget" inclui áudio em português utilizando a voz "Liane".

É criada uma macro e validate "captcha_widget" para que o captcha possa ser inserido em
formulários de controller page template (.cpt) como o "login_form", "contact-info" e
"sendto_form". Estes três formulários já foram customizados no produto.

Foi criado widget e validator para utilização em formulários z3c.form. Foi também criado
um widget para formlib que sobrescreve o definido originalmente por collective.captcha,
assim, as traduções e as montagens de áudio em português serão utilizadas ao invés dos
originais.

O "serpro.collectivecaptichawidget" também aplica um patch no produto "plone.app.discussion" para
que este, caso instalado, também diponibilize o "serpro.collectivecaptichawidget" como opção de
captcha a ser utilizado nos formulários de adição de comentários.

Adicionada opção de somente aparecer o captcha se o usuário errar o login duas vezes.
Os forms onde essa opção é válida, pode ser configurado em:

Configurações do site -> Configurações de Produtos -> Serpro Collective Captchawidget -> Templates Captcha

Por default os templates login_form e login_failed já vem com essa opção.

Funcionamento
#############

Criação de arquivos wav
=======================

Os arquivos wav foram criados utilizando o software Balabolka e a voz Liane (disponibilizada pelo SERPRO):


LianeTTS
http://www.serpro.gov.br/servicos/downloads/lianetts/


Balabolka
Author: Ilya Morozov
WWW: http://www.cross-plus-a.com/balabolka.htm
E-mail: crossa@hotbox.ru
Note: This application is begin released as freeware.
Users with questions regarding either this EULA or the application in general,
should feel free to ask the Author. I answer all messages as soon as possible.


Criação do waveIndex-pt-br.zip
==============================

A criação do waveIndex foi feita utilizando o pacote python skimpyGimpy.
Este pacote é usado pelo collective.captcha para a geração da imagem e do audio.

:~/Desenvolvimento/skimpyGimpy_1_4$ ~/Desenvolvimento/Python-2.4.6/bin/python
Python 2.4.6 (#2, May 14 2009, 10:26:39) 
[GCC 4.2.3 (Ubuntu 4.2.3-2ubuntu7)] on linux2
Type "help", "copyright", "credits" or "license" for more information.

>>> from skimpyGimpy import waveTools
>>> result = waveTools.getIndex()
>>> result.dumpZip('waveIndex-pt-br.zip')

>>> WORD = 'testando12345'
>>> INDEXFILEPATH = 'waveIndex-pt-br.zip'
>>> WAVEFILE = 'word.wav'
>>> from skimpyGimpy import skimpyAPI
>>> waveGenerator = skimpyAPI.Wave(WORD,indexFile=INDEXFILEPATH)
>>> waveText = waveGenerator.data()
>>> waveText = waveGenerator.data(WAVEFILE)


Utilizando o captcha_widget
===========================

Formulários .cpt
-----------------
Basta incluir a chamada da macro no formulário e incluir o validate no ".metadata" do template.

Chamada da macro:

<metal:captcha use-macro="here/captcha_widget/macros/captcha" />

Inclusão do validate:

[validators]
validators = ... , captcha_validate


zope.formlib
------------
Basta adicionar no seu schema:

::

    from collective.captcha.form import Captcha

    class ICaptchaSchema(Interface):
        captcha = Captcha(title=u"Captcha",
                          description=u"",
                          required=True)

O próprio módulo serpro.collectivecaptchawidget já possui validadores que
funcionam sem maiores configurações pois a validação é feita na definição do
widget que sobrescreve o original em collective.captcha.


z3c.form
-----------------

from zope.interface import Interface
from z3c.form import form, field

from zope.interface import implements

from serpro.collectivecaptchawidget import CaptchaFieldWidget
from collective.captcha.form import Captcha


class ICaptchaSchema(Interface):
    captcha = Captcha(title=u"Captcha",
                      description=u"",
                      required=True)

class CaptchaForm(form.Form):
    implements(ICaptchaSchema)
    fields = field.Fields(ICaptchaSchema)
    fields['captcha'].widgetFactory = CaptchaFieldWidget


# Criar o validador no seu produto

from serpro.collectivecaptchawidget.captcha_validator import CaptchaValidator
from seu.produto.interfaces import IProdutoLayer

from zope.interface import Interface
from zope.component import adapts
from zope.schema.interfaces import IField


class ProdutoCaptchaValidator(CaptchaValidator):
    """Validador do captcha"""

    # Objeto, Request, Form, Field, Widget,
    adapts(Interface, IProdutoLayer, Interface, IField, Interface)


# Registar o validador no configure.zcml

  <!-- adapter do validador do captcha -->
  <zope:adapter
     factory=".validators.ProdutoCaptchaValidator"
     provides="z3c.form.interfaces.IValidator"
     />


# Registrar o validador no __init__.py do pacote

from z3c.form import validator
from seu.produto.browser.validators import ProdutoCaptchaValidator

# Registra validador para o captcha no form ICaptchaSchema
validator.WidgetValidatorDiscriminators(ProdutoCaptchaValidator,
                                        field=ICaptchaSchema['captcha'])
