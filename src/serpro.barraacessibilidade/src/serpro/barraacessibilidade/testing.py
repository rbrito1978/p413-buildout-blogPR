# -*- coding: utf-8 -*-
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

class Fixture(PloneSandboxLayer):
    defaultBases = (PLONE_FIXTURE,)
    
    def setUpZope(self, app , configurationContext):
        # Load ZCML
        import serpro.barraacessibilidade
        self.loadZCML(package=serpro.barraacessibilidade)

    def setUpPloneSite(self,portal):
        #install into Plone site using portal_setup#
        self.applyProfile(portal, 'serpro.barraacessibilidade:default')

FIXTURE = Fixture()
INTEGRATION_TESTING = IntegrationTesting(
    bases=(FIXTURE,),
    name='serpro.barraacessibilidade:Integration',
    )
FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(FIXTURE,),
    name='serpro.barraacessibilidade:Functional',
    )
