from zope.interface import Interface

class ISerproBarraAcessibilidadeLayer(Interface):
    """ Marker interface that defines a Zope 3 browser layer
    """
