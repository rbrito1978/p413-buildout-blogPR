# -*- coding:utf-8 -*-

import logging

from Products.CMFCore.utils import getToolByName

from serpro.barraacessibilidade.config import PROJECTNAME

logger = logging.getLogger(PROJECTNAME)


def postInstall(context):
    """Called as at the end of the setup process. """
    if context.readDataFile('serpro.barraacessibilidade_various.txt') is None:
        return
    
    site = context.getSite()
    
    portal_actions = getToolByName(site, 'portal_actions')
    if not hasattr(portal_actions,'serpro_barraacessibilidade_actions'):
        setup_tool = getToolByName(site, 'portal_setup')
        profile = 'profile-serpro.barraacessibilidade:initial'
        setup_tool.runAllImportStepsFromProfile(profile)
        logger.info(' actions added')
