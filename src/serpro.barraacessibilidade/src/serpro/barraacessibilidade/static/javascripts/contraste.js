function setContraste($contraste, $reset) {
    var $body = jQuery('body');
    if ($reset) {
        $body.removeClass('contraste');
        createCookie("contraste", $contraste, 365);
    }
    $body.addClass($contraste);
};

(function($) { $(function() {
        var $contraste = readCookie("contraste");
        if ($contraste) setContraste($contraste, 0);
}); })(jQuery);
