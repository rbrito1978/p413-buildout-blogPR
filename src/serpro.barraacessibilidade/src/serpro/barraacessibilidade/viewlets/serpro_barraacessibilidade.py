# -*- coding: utf-8 -*-

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getMultiAdapter
from plone.app.layout.viewlets.common import ViewletBase


class BarraAcessibilidade(ViewletBase):
    index = ViewPageTemplateFile('serpro_barraacessibilidade.pt')

    def update(self):
        context_state = getMultiAdapter((self.context, self.request),
                                        name=u'plone_context_state')
        self.serpro_barraacessibilidade_actions = context_state.actions('serpro_barraacessibilidade_actions')
