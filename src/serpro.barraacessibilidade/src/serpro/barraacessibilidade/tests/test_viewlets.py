# -*- coding: utf-8 -*-

import unittest2 as unittest

from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from zope.interface import directlyProvides

from serpro.barraacessibilidade.config import PROJECTNAME
from serpro.barraacessibilidade.testing import INTEGRATION_TESTING
from serpro.barraacessibilidade.viewlets.serpro_barraacessibilidade import BarraAcessibilidade 
from serpro.barraacessibilidade.browser.interfaces import ISerproBarraAcessibilidadeLayer



class ViewletsTest(unittest.TestCase):

    layer = INTEGRATION_TESTING    

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        

    def test_serpro_barraacessibilidade_viewlet(self):
        viewlet = BarraAcessibilidade(self.portal, self.request, None, None)
        self.assertTrue(viewlet is not None)

    def test_serpro_barraacessibilidade_actions(self):
        viewlet = BarraAcessibilidade(self.portal, self.request, None, None)
        viewlet.update()
        actions = [action['id'] for action in viewlet.serpro_barraacessibilidade_actions]
        self.assertEqual(['skip_menu','skip_content','small_text',\
                          'normal_text','large_text','apply_contrast',\
                          'remove_contrast','accessibility','sitemap'], actions)
        



def test_suite():
    return unittest.defaultTestLoader.loadTestsFromName(__name__)
