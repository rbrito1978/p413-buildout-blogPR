# -*- coding: utf-8 -*-

import unittest2 as unittest

from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles

from serpro.barraacessibilidade.config import PROJECTNAME
from serpro.barraacessibilidade.testing import INTEGRATION_TESTING

from plone.browserlayer.utils import registered_layers

class ActionsTest(unittest.TestCase):

    layer = INTEGRATION_TESTING    

    def setUp(self):
        self.portal = self.layer['portal']
        self.portal_actions = self.portal['portal_actions']

    def test_serpro_barraacessibilidade_actions_created(self): 
        categories = [c['category'] for c in self.portal_actions.listActionInfos()]
        self.assertTrue('serpro_barraacessibilidade_actions' in categories)




def test_suite():
    return unittest.defaultTestLoader.loadTestsFromName(__name__)
