# -*- coding: utf-8 -*-
#
# File: Install.py

def beforeUninstall(self, reinstall=False, product=None, cascade=[]):
    """ Prevent created content from being deleted on uninstall
    """
    items = ['actions','portalobjects']
    for item in items:
        if item in cascade:
            cascade.remove(item)

    return True, cascade
