from setuptools import setup, find_packages
import os

version = '0.10.1'

setup(name='serpro.galeriaimagens',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.6",
        "Framework :: Plone",
        "Framework :: Plone :: 4.1",
        "Environment :: Web Environment",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
        ],
      keywords='Galeria Imagens SERPRO',
      author='DEBHE/DE614',
      author_email='',
      url='',
      license='SERPRO',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      extras_require={'test': ['plone.app.testing']},
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      #setup_requires=["PasteScript"],
      paster_plugins=["ZopeSkel"],
      )
