from zope.interface import Interface
# -*- Additional Imports Here -*-


class ISerproGaleriaImagensLayer(Interface):
    """ A layer specific to this product. 
        Is registered using browserlayer.xml
    """