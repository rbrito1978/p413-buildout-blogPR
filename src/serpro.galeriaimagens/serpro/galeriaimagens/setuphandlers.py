# -*- coding: utf-8 -*-
from zope.interface import alsoProvides
from zope.component import getUtility, getMultiAdapter
from Products.CMFCore.utils import getToolByName

import logging
logger = logging.getLogger('serpro.galeriaimagens: setuphandlers')

             
def postInstall(context):
    """Called as at the end of the setup process. """  
    portal = context.getSite()
  