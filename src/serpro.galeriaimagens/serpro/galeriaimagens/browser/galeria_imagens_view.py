# -*- coding: utf-8 -*-

from os import close
import tempfile
from urllib import unquote
from zipfile import ZipFile, ZIP_DEFLATED, ZipInfo

from Products.Five.browser import BrowserView
from zope.interface import implements

from Products.statusmessages.interfaces import IStatusMessage
from Products.CMFCore.utils import getToolByName
from zope.interface import Interface
from zope.component import getMultiAdapter


from serpro.galeriaimagens import _

class IGaleriaImagensView(Interface):
    """View de audio"""
    
    def getAlbuns(self):
        """ retorna os pastas publicadas """

    def getImagens(self):
        """ retorna as imagens publicadas"""
    
    def getZip(self):
        """Retorna o album zipado se o objeto existir"""
    
    def adScript(self):
        """ retorna o javascript da galeria AD"""

class IGerarZip(Interface):
    """View de exportacao de galeria como zip"""
    
    def gerar(self):
        """geracao do zip"""

class GaleriaImagensView(BrowserView):
    
    implements(IGaleriaImagensView)    
    
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.plone_portal_state = getMultiAdapter((self.context,self.request), name='plone_portal_state')
        self.plone_tools = getMultiAdapter((self.context,self.request), name='plone_tools') 

    def getAlbuns(self):
        """ retorna as pastas publicadas """
        catalog = self.plone_tools.catalog()
        
        path = '/'.join(self.context.getPhysicalPath())
        if self.context.portal_type == 'Topic':
            query_topic = self.context.buildQuery()
            if query_topic.has_key('path'):
                if query_topic['path']['query'] != '':
                    path = query_topic['path']['query'] 
        
        query = {}
        query['portal_type']=['Folder','Topic']
        query['review_state']='published'
        query['path']={"query":path,"depth":1}
        query['sort_on']='effective'
        query['sort_order']='reverse'
        
        query_imagens = {}
        query_imagens['portal_type']=['Image',]
        query_imagens['review_state']='published'
        query_imagens['sort_on']='getObjPositionInParent'
        
        query_file = {}
        query_file['portal_type']=['File',]
        query_file['review_state']='published'


        
        albuns = []        
        for album in catalog(query):
            is_topic = album.portal_type == 'Topic'
            
            path_album = album.getPath()
            if is_topic:
                query_topic = album.getObject().buildQuery()
                if query_topic.has_key('path'):
                    if query_topic['path']['query'] != '':
                        path_album = query_topic['path']['query']
                
            query_imagens['path']={"query":path_album,"depth":1}              
            imagens = catalog(query_imagens)
            
            query_file['path']={"query":path_album,"depth":1} 
            query_file['id'] = '%s.zip' % album.getId
            files = catalog(query_file)
            
            
            item = {}
            item['Title']=album.Title
            item['getURL']=album.getURL()
            item['Description']=album.Description
            item['effective']=album.effective
            if imagens:
                item['len_images'] = len(imagens)
                item['image_url'] = imagens[0].getURL()
            else:
                item['len_images'] = None
                item['image_url'] = None 
            if files:                
                item['file_url'] = files[0].getURL()
                item['file_size'] = files[0].getObjSize
            else:
                item['file_url'] = None
                item['file_size'] = None 
                           
            albuns.append(item)
        return albuns
            
  
  
        
    def getImagens(self):
        """
        Retorna as imagens publicadas
        """ 
        catalog = self.plone_tools.catalog()        
        
        path = '/'.join(self.context.getPhysicalPath())
        if self.context.portal_type == 'Topic':
            query_topic = self.context.buildQuery()
            if query_topic.has_key('path'):
                if query_topic['path']['query'] != '':
                    path = query_topic['path']['query'] 
            
        query = {}
        query['portal_type']=['Image',]
        query['review_state']='published'
        query['path']={"query":path,"depth":1}
        query['sort_on']='getObjPositionInParent'
        
        return [{'Title':imagem.Title,'getURL':imagem.getURL(),
                 'Description':imagem.Description,
                 'effective':imagem.effective,
                 'file_size':imagem.getObjSize}
                for imagem in catalog(query)
               ]
    
    def getZip(self):
        """Retorna o album zipado se o objeto existir"""
        catalog = self.plone_tools.catalog()        
        
        path = '/'.join(self.context.getPhysicalPath())
        id_file = '%s.zip' % self.context.id
        
        query = {}
        query['portal_type']=['File',]
        query['review_state']='published'
        query['path']={"query":path,"depth":1}
        query['id']=id_file
        
        return [{'getURL':item.getURL(),'getObjSize':item.getObjSize} for item in catalog(query)][:1]


    def adScript(self):
        """ retorna o javascript da galeria AD"""
        lang = self.plone_portal_state.language()
        start_label = 'Slide Show - Play'
        stop_label = 'Stop'
        if lang == 'pt-br':
            start_label = 'Slide Show - Inciar'
            stop_label = 'Parar'
        elif lang == 'es':
            start_label = 'Slide Show - Comenzar'
            stop_label = 'Detener'        
        
        script = """
                 jq(function() {
                      var settings = {
                        loader_image: 'loader.gif',
                        start_at_index: 0,
                        update_window_hash: true,
                        description_wrapper: false,
                        thumb_opacity: 0.7,
                        animate_first_image: false,
                        animation_speed: 400,
                        width: false,
                        height: false,
                        display_next_and_prev: true,
                        display_back_and_forward: true,
                        scroll_jump: 0, // If 0, it jumps the width of the container
                        slideshow: {
                            enable: true,
                            autostart: false,
                            speed: 5000,
                            start_label: '%s',
                            stop_label: '%s',
                            stop_on_scroll: true,
                            countdown_prefix: '(',
                            countdown_sufix: ')',
                            onStart: false,
                            onStop: false
                        },
                        effect: 'slide-hori', // or 'slide-vert', 'fade', or 'resize', 'none'
                        enable_keyboard_move: true,
                        cycle: true,
                        hooks: {
                            displayDescription: false
                        },
                        callbacks: {
                            init: false,
                            afterImageVisible: false,
                            beforeImageVisible: false
                        }
                    };
                     
                    var galleries = jq('.ad-gallery').adGallery(settings=settings);
                
                    galleries[0].addAnimation('wild',
                      function(img_container, direction, desc) {
                        var current_left = parseInt(img_container.css('left'), 10);
                        var current_top = parseInt(img_container.css('top'), 10);
                        if(direction == 'left') {
                          var old_image_left = '-'+ this.image_wrapper_width +'px';
                          img_container.css('left',this.image_wrapper_width +'px');
                          var old_image_top = '-'+ this.image_wrapper_height +'px';
                          img_container.css('top', this.image_wrapper_height +'px');
                        } else {
                          var old_image_left = this.image_wrapper_width +'px';
                          img_container.css('left','-'+ this.image_wrapper_width +'px');
                          var old_image_top = this.image_wrapper_height +'px';
                          img_container.css('top', '-'+ this.image_wrapper_height +'px');
                        };
                        if(desc) {
                          desc.css('bottom', '-'+ desc[0].offsetHeight +'px');
                          desc.animate({bottom: 0}, this.settings.animation_speed * 2);
                        };
                        img_container.css('opacity', 0);
                        return {old_image: {left: old_image_left, top: old_image_top, opacity: 0},
                                new_image: {left: current_left, top: current_top, opacity: 1},
                                easing: 'easeInBounce',
                                speed: 2500};
                      }
                    );
                  });  
                 """ % (start_label,stop_label)
        return script


    
    
class GerarZip(BrowserView):
    
    implements(IGerarZip) 
    
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.plone_portal_state = getMultiAdapter((self.context,self.request), name='plone_portal_state')
        self.plone_tools = getMultiAdapter((self.context,self.request), name='plone_tools')
        self.plone_context_state = getMultiAdapter((self.context,self.request), name='plone_context_state')  
    
    def gerar(self):
        """geracao do zip"""
        catalog = self.plone_tools.catalog()
        workflow = self.plone_tools.workflow()
        portal = self.plone_portal_state.portal()
        
        path = '/'.join(self.context.getPhysicalPath())
        zip_id = '%s.zip' % self.context.id
            
        query = {}
        query['portal_type']=['Image',]
        query['review_state']='published'
        query['path']={"query":path,"depth":1}
        query['sort_on']='getObjPositionInParent'
        
        # arquivo temporario
        fd, path_fd = tempfile.mkstemp('_%s' % zip_id)
        tfile = path_fd
        close(fd)
        
        # cria o arquivo zip com as imagens 
        zip_file =  ZipFile(tfile, 'w', ZIP_DEFLATED)
        context_path = str(self.context.virtual_url_path())
                
        for imagem in catalog(query): 
            image_id = imagem.getId             
            image = portal.restrictedTraverse(imagem.getPath())            
            image_file = image.data  
            if image_file:
                image_filename = image.getImage().filename or image_id
                image_filename = unquote(image_filename)        
                zip_file.writestr(image_filename, image_file) 
        zip_file.close()
        
        # abrindo o arquivo como binario
        binary_file = open(tfile, 'rb')
        # cria um objeto do tipo File na pasta da galeria
        if not hasattr(self.context,zip_id):
            self.context.invokeFactory('File',id=zip_id,title=zip_id)
        new_obj = getattr(self.context,zip_id)
        new_obj.edit(file=binary_file)
        new_obj.reindexObject()
        # tenta publicar
        try:
            workflow.doActionFor(new_obj, "publish")
        except:
            # o objeto ja estava publicado
            pass
        binary_file.close()
        
        message = _(u"zip_successfully",mapping={'zip_id': zip_id})
        IStatusMessage(self.request).addStatusMessage(message, type='info')
        return self.request.RESPONSE.redirect('%s/folder_contents' % self.plone_context_state.object_url())
        
         
        
        
        
        