# -*- coding: utf-8 -*-
#
# File: Install.py

def beforeUninstall(portal, reinstall=False, product=None, cascade=[]):
    """ Prevent created content from being deleted on uninstall
    """
    if 'portalobjects' in cascade:
        cascade.remove('portalobjects')

    return True, cascade