Changelog
=========

0.4.2
---------------------

- Corrigindo erro do namespace_packages em setup.py.
  [giovanni.calanzani@serpro.gov.br]

0.4.0
---------------------

- Barra funcional para uso imediato através do manager 'plone.portaltop'.
  [giovanni.calanzani@serpro.gov.br]

0.1.0dev (unreleased)
---------------------

- Package created using zopeskel
  []
