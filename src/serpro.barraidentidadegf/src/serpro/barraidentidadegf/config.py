# -*- coding: utf-8 -*-

# Constantes utilizadas no produto

PROJECTNAME = 'serpro.barraidentidadegf'

BARRA_VIEWLET_NAME = 'serpro.barraidentidadegf.barra_identidade'
BARRA_VIEWLET_MANAGER = 'plone.portaltop'

# Cores hexadecimais que podem ser utilizadas como variantes de cor na barra.
# Cores obtidas de: http://epwg.governoeletronico.gov.br/barra/
# Nenhuma cor é definida como "padrão" no Manual de Identidade Visual do
# Governo Federal na Internet, mesmo assim utilizaremos a Verde.
HEX_VERDE = '#00500F' # Será utilizada como cor padrão
HEX_CINZA = '#7F7F7F'
HEX_PRETO = '#000000'
HEX_AZUL = '#004B82'
