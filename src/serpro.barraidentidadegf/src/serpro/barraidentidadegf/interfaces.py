# -*- coding: utf-8 -*-

from zope.interface import Interface

class IBarraIdentidadeGFBrowserLayer(Interface):
    """ A marker interface for the browser layer.
    """
