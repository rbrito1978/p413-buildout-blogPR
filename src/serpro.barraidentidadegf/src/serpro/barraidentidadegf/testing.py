# -*- coding: utf-8 -*-

from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import IntegrationTesting
#from plone.app.testing import FunctionalTesting
from plone.app.testing import applyProfile

from zope.configuration import xmlconfig


class SerproBarraidentidadegf(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE, )

    def setUpZope(self, app, configurationContext):
        # Load ZCML for this package
        import serpro.barraidentidadegf
        xmlconfig.file('configure.zcml',
                       serpro.barraidentidadegf,
                       context=configurationContext)


    def setUpPloneSite(self, portal):
        applyProfile(portal, 'serpro.barraidentidadegf:default')


SERPRO_BARRAIDENTIDADEGF_FIXTURE = SerproBarraidentidadegf()
SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING = \
    IntegrationTesting(bases=(SERPRO_BARRAIDENTIDADEGF_FIXTURE, ),
                       name="SerproBarraidentidadegf:Integration")
