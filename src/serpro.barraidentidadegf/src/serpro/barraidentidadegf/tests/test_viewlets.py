# -*- coding: utf-8 -*-

import unittest2 as unittest

from Acquisition.interfaces import IAcquirer

from AccessControl.ZopeGuards import guarded_hasattr

from zope.component import queryMultiAdapter, queryUtility
from zope.viewlet.interfaces import IViewletManager
# this time, we need to add an interface to the request
from zope.interface import alsoProvides

from Products.CMFCore.utils import getToolByName

from Products.Five.browser import BrowserView as View

from plone.app.viewletmanager.interfaces import IViewletSettingsStorage

from serpro.barraidentidadegf.config import BARRA_VIEWLET_NAME,\
    BARRA_VIEWLET_MANAGER, PROJECTNAME
from serpro.barraidentidadegf.interfaces import IBarraIdentidadeGFBrowserLayer
from serpro.barraidentidadegf.testing import\
    SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING


def is_viewlet_available(context, viewlet_name, viewlet):
    """Retorna se uma viewlet está disponível (visível) ou não.

    Código inspirado do método filter em BaseOrderedViewletManager, em:

    plone.app.viewletmanager-2.0.1-py2.6.egg/plone/app/viewletmanager/manager.py

    Só que ao invés de procurar por 'hidden' utilizando 'getHidden', procura
    por 'order' utilizando 'getOrder': além disso trata apenas uma view e não
    várias como no método original.

    """
    storage = queryUtility(IViewletSettingsStorage)
    if storage is None:
        return False
    skinname = context.getCurrentSkinName()
    order = frozenset(storage.getOrder(BARRA_VIEWLET_MANAGER, skinname))
    # Only return visible viewlets accessible to the principal
    # We need to wrap each viewlet in its context to make sure that
    # the object has a real context from which to determine owner
    # security.
    # Copied from Five
    if IAcquirer.providedBy(viewlet):
        viewlet = viewlet.__of__(viewlet.context)

    return viewlet_name in order and guarded_hasattr(viewlet, 'render')


class BarraIdentidadeGFViewletInstallTest(unittest.TestCase):

    layer = SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']

    def test_viewlet_is_present(self):
        """ looking up and updating the manager should list our viewlet
        """
        # our viewlet is registered for a browser layer.  Browser layers
        # are applied to the request during traversal in the publisher.  We
        # need to do the same thing manually here
        request = self.app.REQUEST
        alsoProvides(request, IBarraIdentidadeGFBrowserLayer)

        context = self.portal

        # viewlet managers also require a view object for adaptation
        view = View(context, request)

        # viewlet managers are found by Multi-Adapter lookup
        manager = queryMultiAdapter((context, request, view), IViewletManager,
                BARRA_VIEWLET_MANAGER, default=None)

        self.failUnless(manager)

        # calling update() on a manager causes it to set up its viewlets
        manager.update()

        # now our viewlet should be in the list of viewlets for the manager
        # we can verify this by looking for a viewlet with the name we used
        # to register the viewlet in zcml
        my_viewlet = [v for v in manager.viewlets
                      if v.__name__ == BARRA_VIEWLET_NAME]

        is_available = is_viewlet_available(self.portal, BARRA_VIEWLET_NAME,
            my_viewlet[0])

        self.assertTrue(is_available)

        self.failUnlessEqual(len(my_viewlet), 1)


class BarraIdentidadeGFViewletUninstallTest(unittest.TestCase):

    layer = SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.qi_tool = getToolByName(self.portal, 'portal_quickinstaller')
        self.qi_tool.uninstallProducts(products=[PROJECTNAME])

    def test_viewlet_is_not_present(self):
        """ Após a desinstalação, a viewlet não deve estar disponível.
        """
        # our viewlet is registered for a browser layer.  Browser layers
        # are applied to the request during traversal in the publisher.  We
        # need to do the same thing manually here
        request = self.app.REQUEST
        alsoProvides(request, IBarraIdentidadeGFBrowserLayer)

        context = self.portal

        # viewlet managers also require a view object for adaptation
        view = View(context, request)

        # viewlet managers are found by Multi-Adapter lookup
        manager = queryMultiAdapter((context, request, view), IViewletManager,
                BARRA_VIEWLET_MANAGER, default=None)

        self.failUnless(manager)

        # calling update() on a manager causes it to set up its viewlets
        manager.update()

        # now our viewlet should be in the list of viewlets for the manager
        # we can verify this by looking for a viewlet with the name we used
        # to register the viewlet in zcml
        my_viewlet = [v for v in manager.viewlets
                      if v.__name__ == BARRA_VIEWLET_NAME]

        # O teste pra desinstalação não pode ser o mesmo da instalação, pois
        # uma viewlet permanecerá registrada por ter sido efetuado no zcml: ela
        # só aparece em browserlayers específicas mas continua registrada.
        # Portanto, tem de testar se estar disponível pra uso ou não, e não se
        # está ou não registrada.
        is_available = is_viewlet_available(self.portal, BARRA_VIEWLET_NAME,
            my_viewlet[0])

        self.assertFalse(is_available)
