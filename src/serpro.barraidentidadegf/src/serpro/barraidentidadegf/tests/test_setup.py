# -*- coding: utf-8 -*-

import unittest2 as unittest

from Products.CMFCore.utils import getToolByName

from plone.browserlayer.utils import registered_layers

from serpro.barraidentidadegf.config import PROJECTNAME
from serpro.barraidentidadegf.testing import\
    SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING


class InstallTest(unittest.TestCase):

    layer = SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.qi_tool = getToolByName(self.portal, 'portal_quickinstaller')

    def test_browserlayer_installed(self):
        layers = [l.getName() for l in registered_layers()]
        self.assertTrue('IBarraIdentidadeGFBrowserLayer' in layers,
                'browser layer not installed')

    def test_product_is_installed(self):
        """ Validate that our products GS profile has been run and the product
            installed
        """
        installed = [p['id'] for p in self.qi_tool.listInstalledProducts()]
        self.assertTrue(PROJECTNAME in installed,
                        'package appears not to have been installed')


class UninstallTest(unittest.TestCase):

    layer = SERPRO_BARRAIDENTIDADEGF_INTEGRATION_TESTING

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.qi_tool = getToolByName(self.portal, 'portal_quickinstaller')
        self.qi_tool.uninstallProducts(products=[PROJECTNAME])

    def test_uninstalled(self):
        self.assertFalse(self.qi_tool.isProductInstalled(PROJECTNAME))

    def test_browserlayer_removed(self):
        layers = [l.getName() for l in registered_layers()]
        self.assertFalse('IBarraIdentidadeGFBrowserLayer' in layers,
                'browser layer not removed')
