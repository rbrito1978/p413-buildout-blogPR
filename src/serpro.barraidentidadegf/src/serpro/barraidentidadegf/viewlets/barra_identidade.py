# -*- coding: utf-8 -*-

from plone.app.layout.viewlets import common


class BarraIdentidadeGFViewlet(common.ViewletBase):
    """ Adiciona a Barra de Identidade do Governo Federal.
    """
