.. contents::

Introdução
============
Barra de Identidade Visual do Governo Federal na internet em sites e portais  
que utilizam Plone.

Feita utilizando como base http://epwg.governoeletronico.gov.br/barra/: cores
e imagens utilizadas nesse produto foram obtidos(as) dessa url e do 
"Manual de Identidade Visual do Governo Federal na Internet Regras de aplicação  
da Barra de Identidade.", acessado em 2012/04/03.

Como utilizar
=============
Este módulo é apenas uma Viewlet que é registrada dentro de
**plone.portaltop**, com configurações de css para garantir que fique sempre no  
topo de todo conteúdo.  
 
Com a utilização de plone.portaltop a barra é utilizável mesmo sem customizar o
arquivo **main_template**: basta instalar o produto em portal_quickinstaller  
que a Barra de Identidade Visual já ficará disponível.

Há também pequenos fixes para corrigir regras definidas pelo Plone que
interferem na barra e também caso seja utilizado "Sunburst Theme" como base  
(apesar de não ser recomendado, pois o main_template.pt dele é muito  
customizado).

Customizando o espaçamento à direita dos selos
----------------------------------------------
A barra parte da premissa que o site possui, como limite de largura, 960px:
assim, o selo "Brasil" (como definido no manual) ficará sempre alinhado com o
último elemento à direita apenas se o site **também** tiver 960px. Caso possua
limites diferentes de 960px, você deve criar um novo arquivo css, alterando o
width original da barra:

::

    #barra-brasil .barra {
        ...
        width: 960px;
        ...
    }

...para o valor desejado em seu portal.

Lembre-se de, no momento de registrar o seu novo arquivo css no cssregistry.xml
do seu profile, colocá-lo após **barraidentidadegf.css**. Utilize o atributo
insert-after:

::

    insert-after="barraidentidadegf.css"

Dessa forma, suas alterações sobrescreverão as originais da barra.

Também pode ser utilizado o arquivo ploneCustom.css, pois o css da barra é
registrado antes dele.

Alterando as cores da barra
---------------------------
A alteração de cores também deve ser feita no seu novo arquivo css. A cor
padrão da barra é a #004b82. Outras cores permitidas (favor verificar a
documentação oficial, as cores presentes nesse documento são apenas por
questões de referência):

Azul
#004b82
RGB: 0, 80, 15

Verde
#00500f
RGB: 0, 75, 130

Cinza
#7f7f7f
RGB: 127, 127, 127

Negra
#000000
RGB: 0, 0, 0

As cores devem seguir a identidade visual do portal.

Exemplo: alterando a barra para a cor negra, e com um site cujo limite de
largura será de 1024px:

::

    #barra-brasil { 
        background: #000000 url(++resource++barraidentidadegf_static/barra-brasil-v3-bgx.png) center bottom no-repeat;
    }

    #barra-brasil .barra {
        width: 1024;
    }

    #barra-brasil .ai {
        background: #000000 url(++resource++barraidentidadegf_static/ai.png) no-repeat 0 4px;

    }

    #barra-brasil .brasilgov {
        background: #000000 url(++resource++barraidentidadegf_static/brasil.png) no-repeat 0 4px;
    }

Lembre-se de, no momento de registrar o seu novo arquivo css no cssregistry.xml
do seu profile, colocá-lo após **barraidentidadegf.css**. Utilize o atributo
insert-after:

::

    insert-after="barraidentidadegf.css"

Dessa forma, suas alterações sobrescreverão as originais da barra.

Também pode ser utilizado o arquivo ploneCustom.css, pois o css da barra é
registrado antes dele.
