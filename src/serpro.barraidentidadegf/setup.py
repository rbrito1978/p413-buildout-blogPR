from setuptools import setup, find_packages

version = '0.6.1'

long_description = (
    open('README.txt').read()
    + '\n' +
    'Contributors\n'
    '============\n'
    + '\n' +
    open('CONTRIBUTORS.txt').read()
    + '\n' +
    open('CHANGES.txt').read()
    + '\n')

setup(name='serpro.barraidentidadegf',
      version=version,
      description="""Barra de Identidade Visual do Governo Federal na internet
          em sites e portais que utilizem Plone.""",
      long_description=long_description,
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Framework :: Plone :: 3.3",
        "Development Status :: 4 - Beta",
        "Framework :: Plone :: 4.0",
        "Framework :: Plone :: 4.1",
        "License :: Other/Proprietary License",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Giovanni Monteiro Calanzani',
      author_email='giovanni.calanzani@serpro.gov.br',
      url='https://www.serpro.gov.br/',
      license='other',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      paster_plugins=["ZopeSkel"],
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      extras_require={'test': ['plone.app.testing']},
      entry_points="""
      # -*- Entry points: -*-
          [z3c.autoinclude.plugin]
          target = plone
      """,
      )
