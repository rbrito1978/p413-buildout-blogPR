# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger('serpro.translations')

from Products.validation import validation

# Função original, que será chamada no fim do patch, assim, se ocorrerem
# mudanças no código original, não preciso recopiar para esse módulo.
from Products.validation.i18n import recursiveTranslate as \
    _old_recursiveTranslate

# Lista de validadores de cada módulo
from Products.validation.validators.BaseValidators import baseValidators
from Products.validation.validators.EmptyValidator import validatorList as \
    emptyValidators
from Products.validation.validators.SupplValidators import validatorList as \
    supplValidators
from Products.validation.validators.IdValidator import validatorList as \
    idValidators


def patched_recursiveTranslate(message, **kwargs):
    """Corrige o recursiveTranslate que não encontrava o REQUEST no kwargs.

    É interessante que no módulo Products.validation.validators.EmptyValidator
    já existe esse tipo de chamada para obter o REQUEST corretamente, mas por
    algum motivo nos demais validadores (como RegexValidator) isso não ocorre.
    """
    instance = kwargs.get('instance', None)
    if instance is not None:
        request = instance.get('REQUEST', None)
        if request is not None:
            kwargs['REQUEST'] = request

    return _old_recursiveTranslate(message, **kwargs)

def _get_all_validators():
    """Retorna todos os validadores de todos os módulos. Infelizmente não
    consegui uma forma de fazer from Products.validation import validators, ou
    seja, a variável em __init__.py que contém todos os validadores pois o nome
    dessa variável é o mesmo nome do módulo 'validators' dentro do pacote,
    assim, tenho de fazer todo o processo manual."""
    validators = []
    validators.extend(baseValidators)
    validators.extend(emptyValidators)
    validators.extend(supplValidators)
    validators.extend(idValidators)
    return validators

def apply_patch_recursiveTranslate(scope, original, replacement):
    """É necessário toda esse código para um simples patch numa função, porque
    os objetos que utilizam a mesma já foram instanciados em memória, portanto
    preciso reinstanciá-los novamente.

    Ou seja: desregistro os validadores, aplico o patch, reinstancio os objetos
    que contém os validadores (RegexValidator e etc), e registro os validadores
    novamente.

    'Overkill'? Sim. Espero que esse fix não seja necessário futuramente.
    """
    # Desregistra os validadores padrões, pois eles já estão instanciados
    # em memória antes de efetuar o patch, ou seja, referenciam a função
    # recursiveTranslate antiga.
    for validator in _get_all_validators():
        validation.unregister(validator.name)

    # Efetua o patch no método recursiveTranslate. Ver
    # collective.monkeypatcher.meta para entender o uso do setattr.
    setattr(scope, original, replacement)

    # Recarrega os módulos de validação que o BaseValidator importa.
    from Products.validation.validators import RegexValidator, \
        RangeValidator, BaseValidators, EmptyValidator, ExpressionValidator, \
        IdValidator, SupplValidators

    # XXX: Como o nome do arquivo é RegexValidator.py, mas existe uma
    # classe com o mesmo nome (RegexValidator), toda vez que eu faço o from
    # ... import, ele me retorna, magicamente, a CLASSE, e não o MÓDULO:
    # dessa forma, não consigo usar o reload, pois ele espera um MÓDULO.
    # Assim, preciso dar toda essa volta com o uso de .__module__ pra
    # poder passar um módulo como parâmetro para reload.
    reload(__import__(RegexValidator.__module__, fromlist=True))
    reload(__import__(RangeValidator.__module__, fromlist=True))
    reload(__import__(ExpressionValidator.__module__, fromlist=True))

    # Nesses validadores não há o problema que houve nos demais, consigo
    # importar e usar no reload sem problemas.
    reload(BaseValidators)
    reload(EmptyValidator)
    reload(IdValidator)
    reload(SupplValidators)

    # Pega novamente os validadores instanciados dos módulos após o reload
    validators = []
    validators.extend(BaseValidators.baseValidators)
    validators.extend(EmptyValidator.validatorList)
    validators.extend(IdValidator.validatorList)
    validators.extend(SupplValidators.validatorList)

    # Registra os mesmos validadores novamente pois foi efetuado o patch
    # em i18n.recursiveTranslate, assim, no reinstanciamento, vai pegar o
    # recursiveTranslate do patch e não do módulo original fazendo a tradução
    # funcionar novamente.
    for validator in validators:
        validation.register(validator)

    logger.info('Patching Products.validation.i18n.recursiveTranslate')
