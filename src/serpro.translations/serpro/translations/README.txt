#################
serpro.translations
#################

DE6SW

Contribuidores
##############

Glauter Vilela <glauter.vilela@serpro.gov.br>
Maurício Marques <mauricio.marques@serpro.gov.br>
Giovanni Calanzani <giovanni.calanzani@serpro.gov.br>

Dependências
############

Plone 3.3.5+


Objetivo
########

O produto "serpro.translations" foi desenvolvido a partir do projeto "37148pempreendedor" com o objetivo
de reunir traduções customizadas do plone ou outros produtos em um único local, sem dependências, podendo
assim ser reutilizado em diferentes projetos e ser incluído no topo do atributo "zcml" do "buildout.cfg".

Funcionamento
#############

URL de material referência
==========================

http://maurits.vanrees.org/weblog/archive/2010/10/i18n-plone-4

Configuração do buildout
========================

O produto serpro.translations deve ser incluído na lista de "eggs" e deve ser o primeiro item
da lista "zcml".
Assim as traduções incluídas via pasta "locales" terão prioridade em relação às traduções originais.

Ex.:

    [instance]
    ...
    eggs =
        Zope2
        Plone
        ${buildout:eggs}
        serpro.translations
    zcml =
        serpro.translations

Caso não coloque o serpro.translations com precedência, erros no Plone 3.3 como

    File "/Products.PlacelessTranslationService-1.5.4-py2.4.egg/Products/PlacelessTranslationService/patches.py", line 44, in handler
    domain.addCatalog(catalog)
    zope.configuration.config.ConfigurationExecutionError: exceptions.AttributeError: 'PTSTranslationDomain' object has no attribute 'addCatalog'
    in:
    File "/src/serpro.translations/serpro/translations/configure.zcml", line 19.4-19.53
    <i18n:registerTranslations directory="locales" />

irão ocorrer.
    
As traduções que utilizam a pasta i18n devem ser sobrescritas via i18n-overrides.
O i18n-overrides irá criar uma pasta i18n na raiz da(s) instância(s) com os arquivos
que irão sobrescrever as traduções originais.
Deve ser incluído novo item em "parts".

Ex.:

    parts =
         i18noverrides-serpro-translations
    
    
    [i18noverrides-serpro-translations]
    recipe = collective.recipe.i18noverrides
    source = ${buildout:directory}/src/serpro.translations/serpro/translations/i18n-overrides

Diferença entre as pastas i18n e locales
========================================

A principal diferença entre o funcionamento das pastas i18n e locales é que nos arquivos
em i18n o domínio e o idioma são definidos pelo cabeçalho do arquivo, não importando o nome
que você tenha dado ao arquivo ".po".

Ex.:

    "Project-Id-Version: Plone\n"
    "POT-Creation-Date: 2012-02-01 11:30+0000\n"
    "PO-Revision-Date: 2012-02-01 11:30+0000\n"
    "Last-Translator: DE6SW <>\n"
    "Language-Team: Plone i18n <plone-i18n@lists.sourceforge.net>\n"
    "MIME-Version: 1.0\n"
    "Content-Type: text/plain; charset=UTF-8\n"
    "Content-Transfer-Encoding: 8bit\n"
    "Plural-Forms: nplurals=1; plural=0;\n"
    "Language-Code: pt-br\n"
    "Language-Name: Português do Brasil\n"
    "Preferred-Encodings: utf-8 latin1\n"
    "Domain: plone\n"

Já com a pasta locales o que define o idioma e o domínio é a estrutura de pastas e o nome dado ao
arquivo ".po".

Ex.:

    serpro/translations/locales/pt_BR/LC_MESSAGES/collective.singing.po
