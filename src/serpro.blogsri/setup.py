from setuptools import setup, find_packages

version = '0.41.2'

long_description = (
    open('README.txt').read()
    + '\n' +
    'Contributors\n'
    '============\n'
    + '\n' +
    open('CONTRIBUTORS.txt').read()
    + '\n' +
    open('CHANGES.txt').read()
    + '\n')

setup(name='serpro.blogsri',
      version=version,
      description="",
      long_description=long_description,
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.6",
        "Framework :: Plone",
        "Framework :: Plone :: 4.1",
        "Environment :: Web Environment",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
        ],
      keywords='BLOG SRI SERPRO',
      author='debhe/de614',
      author_email='',
      url='',
      license='SERPRO',
      packages=find_packages(exclude=['ez_setup']),
      #packages=find_packages('src'),
      #package_dir = {'': 'src'},
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'collective.flowplayer>=3.0rc4',
	      'p4a.ploneaudio',
          'redturtle.video>=0.7.1',
          'collective.rtvideo.youtube>=0.1.0',
          'collective.rtvideo.vimeo>=0.1.0',
          'collective.rtvideo.metacafe>=0.1.0',
          'qi.portlet.TagClouds>=1.33',
          'Products.PlonePopoll>=2.7.3b1',
          'Products.LinguaPlone>=4.1.2',
          'Products.PloneGazette>=3.2',
          'z3c.jbot>=0.7.1',
          'serpro.setuphandlersutils',
          'serpro.validators',
          'serpro.translations',
          'serpro.protegido',
          'serpro.barraidentidadegf',
          'serpro.barraacessibilidade',
          'serpro.banners',
          'serpro.plone4fixes',
          'serpro.curtir',
          'serpro.collectivecaptchawidget',
          'serpro.galeriaimagens',
          'serpro.maisacessados',
          'serpro.blogsriskin',
      ],
      extras_require={'test': ['plone.app.testing']},
      entry_points="""
      # -*- Entry points: -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
