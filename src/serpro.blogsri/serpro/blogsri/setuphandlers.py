# -*- coding: utf-8 -*-
from zope.interface import alsoProvides
from zope.component import getUtility, getMultiAdapter

from Products.CMFCore.utils import getToolByName

from plone.portlets.interfaces import IPortletManager
from plone.portlets.interfaces import IPortletAssignment
from plone.portlets.interfaces import IPortletAssignmentMapping

from serpro.setuphandlersutils.utils import SetuphandlersUtils
from serpro.protegido.interfaces import IProtegido
from serpro.blogsri.config import *

import logging
logger = logging.getLogger('serpro.blogsri: setuphandlers')

def updatePortalPropertiesCurtir(portal, property_id, values):
    """Inicia os dados de configuração do serpro.curtir se já não estiverem
    definidos."""
    portal_properties = getToolByName(portal, 'portal_properties')
    property_sheet = getattr(portal_properties, CURTIR_PROPERTY_SHEET, None)
    if property_sheet is not None:
        if property_sheet.hasProperty(property_id):
            property_value = getattr(property_sheet, property_id)
            if not property_value or property_value is None:
                property_sheet._updateProperty(property_id, values)

def updatePortalPropertiesMaisAcessados(portal, property_id, values):
    """Inicia os dados de configuração do serpro.maisacessados"""
    portal_properties = getToolByName(portal, 'portal_properties')
    property_sheet = getattr(portal_properties, MAISACESSADOS_PROPERTY_SHEET, None)
    if property_sheet is not None:
        if property_sheet.hasProperty(property_id):
            property_sheet._updateProperty(property_id, values)

def setRootForPortletAssignment(context, portlet_manager, id_portlet, root):
    """ define a propriedade root para um portlet assignment especificado por 
    context, portlet_manager e id_portlet """
    manager = getUtility(IPortletManager, name=portlet_manager, context=context)
    mapping = getMultiAdapter((context, manager), IPortletAssignmentMapping)
    # id is portlet assignment id
    for id, assignment in mapping.items():
        if id == id_portlet:
            assignment.root = root
            return

             
def postInstall(context):
    """Called as at the end of the setup process. """  
    portal = context.getSite()
    sutils = SetuphandlersUtils(portal)
    sutils.excluirInicial()
    
    # incluindo indexes no portal_catalog
    sutils.criarIndexes([('exclude_from_nav','BooleanIndex'),])
    
    # criando os conteudos
    sutils.criarConteudo(portal,[
                    {'id':'posts','title':'Posts','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':True,'interface': IProtegido},
                                 
                    {'id':'sobre-o-blog','title':'Sobre o blog','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False},
                                 
                    {'id':'fotos','title':'Fotos','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False,'interface': IProtegido,'templateId':'album_listing'},
                                 
                    {'id':'videos','title':'Vídeos','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False,'interface': IProtegido,'templateId':'video_listing'},               
                                 
                    {'id':'audios','title':'Áudios','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False,'interface': IProtegido,'templateId':'audios_listing'},
                    
                    {'id':'perguntas-e-respostas','title':'Perguntas e respostas','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False},
                    
                    {'id':'perguntas-e-respostas','title':'Perguntas e respostas','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False},
                    
                    {'id':'criticas-e-sugestoes','title':'Críticas e sugestões','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':False},
                    
                    {'id':'cadastre-se','title':'Cadastre-se','type_name':'Link',
                        'transition':'publish','exclude_from_nav':False,},
                                      
                    {'id':'banners_sociais','title':'Banners de Redes Sociais','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':True,'interface': IProtegido},
                                 
                    {'id':'banners','title':'Banners','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':True,'interface': IProtegido},
                    
                    {'id':'enquetes','title':'Enquetes','type_name':'Folder',
                        'transition':'publish','exclude_from_nav':True,'interface': IProtegido},                     
                  ])
    
    # complementando link Cadastre-se
    cadastre_se = getattr(portal,'cadastre-se',None)
    if cadastre_se is not None:
        cadastre_se.edit(remote_url='../newsletter/NewsletterTheme_subscribeForm')
        cadastre_se.processForm()

    # Definindo a propriedade root para os portlet assignments
    setRootForPortletAssignment(portal, 'plone.rightcolumn', 'imagem_dia_portlet', '/fotos')
    setRootForPortletAssignment(portal, 'plone.rightcolumn', 'video_dia_portlet', '/videos')
    setRootForPortletAssignment(portal, 'plone.rightcolumn', 'audio_dia_portlet', '/audios')

    # Criando Topic Blog (visao do blog)
    pw = getToolByName(portal,'portal_workflow')
    uid_posts = portal['posts'].UID()
    id = 'blog'
    title = 'Blog'
    if id not in portal.contentIds():
        portal.invokeFactory('Topic', id, title=title, limitNumber=True,itemCount=3, excludeFromNav=True)                        
        newObj = getattr(portal, id, None)
        alsoProvides(newObj, IProtegido)
        if newObj is not None:
            criterion = newObj.addCriterion('path','ATPathCriterion')
            criterion.setValue([uid_posts,])
            criterion = newObj.addCriterion('portal_type','ATPortalTypeCriterion')
            criterion.setValue(['Folder','News Item','Document','Event', 'File','Link',
                                'RTRemoteVideo','RTInternalVideo', 'Audio'])
            criterion = newObj.addCriterion('review_state','ATSimpleStringCriterion')
            criterion.setValue('published')
            newObj.setSortCriterion('effective', reversed=True)        
            pw.doActionFor(newObj,'publish')
            newObj.setLayout('blogsri_view') 
            
    # configurando serpro.curtir     
    updatePortalPropertiesCurtir(portal, 'bookmarks_id', CURTIR_BOOKMARKS_IDS)
    updatePortalPropertiesCurtir(portal, 'portal_types_id',CURTIR_PORTAL_TYPES_IDS)
    
    # configurando serpro.maisacessados   
    updatePortalPropertiesMaisAcessados(portal, 'portal_types_id',MAISACESSADOS_PORTAL_TYPES_ID)
    updatePortalPropertiesMaisAcessados(portal, 'reject_ids',MAISACESSADOS_REJECT_IDS)
    
    # criando NewsletterTheme
    id = 'newsletter'
    title = 'Newsletter'
    if id not in portal.contentIds():
        portal.invokeFactory('NewsletterTheme', id, title=title)                        
        newObj = getattr(portal, id, None)
        if newObj is not None: 
            newObj.subscriber_folder_id='inscritos'
            pw.doActionFor(newObj,'publish')
            newObj.invokeFactory('NewsletterBTree', 'inscritos', title='Inscritos')
            inscritos = getattr(newObj, 'inscritos', None)
            if inscritos is not None:
                pw.doActionFor(inscritos,'publish')
    
    # reindexando indice isEnabled do PlonePopoll
    # o Popoll sempre recria o indice via catalog.xml no reinstall
    # entao se faz necessario o reindex
    pc = getToolByName(portal,'portal_catalog')
    pc.manage_reindexIndex('isEnabled')

