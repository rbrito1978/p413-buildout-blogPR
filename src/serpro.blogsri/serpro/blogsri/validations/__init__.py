# -*- coding: utf-8 -*-

# validators registration workarounds

from Products.validation import validation

from serpro.validators.file import ExtensionValidator
from serpro.blogsri import config

import schema 

validation.register(ExtensionValidator('isValidFileExtension',
    extensions=config.EXTENSIONS,
    zip_internal_validation=config.ZIP_INTERNAL_VALIDATION))

validation.register(ExtensionValidator('isValidImageExtension',
    extensions=config.IMAGE_EXTENSIONS))

validation.register(ExtensionValidator('isValidAudioExtension',
    extensions=config.AUDIO_EXTENSIONS))

