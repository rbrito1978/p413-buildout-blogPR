# -*- coding: utf-8 -*-
from Products.validation import V_REQUIRED

# validation workarounds here em content types padrao do plone
# mais nos tipos video provenientes da solucao de video do 
# produto redturtle.video
from zope.component import adapts
# Importe suas interfaces: podem ser do próprio sistema como
# from Products.ATContentTypes.interface.ATINTERFACE import IATINTERFACE
# ou do seu produto:

from Products.ATContentTypes.interface.file import IATFile
from Products.ATContentTypes.interface.image import IATImage
from Products.ATContentTypes.interface.news import IATNewsItem

from serpro.blogsri.interfaces.serproblogsrilayer import ISerproBlogSriLayer

# Classe pronta para adicionar validadores dinâmicos.
from serpro.validators.schema import ValidatorsModifier, RequiredModifier

# Essa classe aparece apenas uma vez.
# ATENÇÂO: As classes de validadores são chamadas TRÊS VEZES, portanto cuidado
# com a implementação inserida nessa utilização do archetypes.schemaextender.
class ValidatorsModifierSerproPlone4Fixes(ValidatorsModifier):
    layer = ISerproBlogSriLayer

# Utilize essa classe para poder falar que um campo é requerido. Exemplo: campo
# título do tipo File.
class RequiredModifierSerproPlone4Fixes(RequiredModifier):
    layer = ISerproBlogSriLayer


class ValidatorsImageModifier(ValidatorsModifierSerproPlone4Fixes):
    """Define a interface do tipo que terá validador(es) adicionado(s) a field(s).

    Através do uso de adapters, conterá a definição de qual interface será
    adaptada e qual(is) validador(es) em determinado(s) field(s).

    'field_validators' é um dicionário, com a chave sendo o id do field e o
    valor sendo OBRIGATORIAMENTE uma tupla com outras tuplas, contendo os
    validadores e o modo (V_REQUIRED ou V_SUFFICIENT).

    Se essa estrutura não for obedecida, será lançada uma exceção.
    """
    adapts(IATImage)
    field_validators = {'image': (('isValidImageExtension', V_REQUIRED),)}


try:
    from redturtle.video.interfaces import IRTInternalVideo
    class ValidatorsVideoModifier(ValidatorsModifierSerproPlone4Fixes):
        adapts(IRTInternalVideo)
        field_validators = {'file': (('isValidVideoExtension', V_REQUIRED),),
                            'image': (('isValidImageExtension', V_REQUIRED),),}
except ImportError:
    pass

try:
    from redturtle.video.interfaces import IRTRemoteVideo
    class ValidatorsVideoLinkModifier(ValidatorsModifierSerproPlone4Fixes): 
        adapts(IRTRemoteVideo)
        field_validators = {'image': (('isValidImageExtension', V_REQUIRED),)}
except ImportError:
    pass

class ValidatorsFileModifier(ValidatorsModifierSerproPlone4Fixes):
    adapts(IATFile)
    field_validators = {'file': (('isValidFileExtension', V_REQUIRED),)}


class ValidatorsNewsItemModifier(ValidatorsModifierSerproPlone4Fixes):
    adapts(IATNewsItem)
    field_validators = {'image': (('isValidImageExtension', V_REQUIRED),)}


class RequiredImageModifier(RequiredModifierSerproPlone4Fixes):
    adapts(IATImage)
    fields = ['title',]

class RequiredFileModifier(RequiredModifierSerproPlone4Fixes):
    adapts(IATFile)
    fields = ['title',]
