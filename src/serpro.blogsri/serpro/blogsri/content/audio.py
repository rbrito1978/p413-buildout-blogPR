# -*- coding: utf-8 -*-
from zope.interface import implements, Interface

from Products.CMFCore.permissions import View
from Products.Archetypes.atapi import *
from Products.Archetypes.atapi import Schema
from Products.ATContentTypes.content.file import ATFile
from Products.ATContentTypes.interface.file import IATFile
from Products.ATContentTypes.config import HAS_PIL
from Products.ATContentTypes.content.file import ATFileSchema
from Products.validation import V_REQUIRED
from Products.ATContentTypes.content.schemata import finalizeATCTSchema

from AccessControl import ClassSecurityInfo


from serpro.blogsri.config import PROJECTNAME

schema = ATFileSchema.copy()
schema['file'].validators = (('isNonEmptyFile', V_REQUIRED),
                             ('checkFileMaxSize', V_REQUIRED),
                             ('isValidAudioExtension',V_REQUIRED),)  
finalizeATCTSchema(schema)

class IAudio(Interface):
    """Marker interface for Audio content type"""

class Audio(ATFile):
    """O tipo audio eh uma copia do tipo file, adicionando validadores
     e modificando metadados"""
    implements(IAudio)
    schema = schema
    portal_type    = 'Audio'
    archetype_name = 'Audio'
    security = ClassSecurityInfo()
    
    # necessario por causa de ATFile. 
    security.declareProtected(View, 'hasPIL')
    def hasPIL(self):
        """Is PIL installed?
        """
        return HAS_PIL

registerType(Audio, PROJECTNAME)
