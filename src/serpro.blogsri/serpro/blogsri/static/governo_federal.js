function getMunicipios(file_xml) {
	
var uf = jq('#uf').attr('value');
jq('#municipio').html("");

jq.ajax({
    type: "GET",
    url: file_xml,
    dataType: "xml",
    success: function(xml){
		        jq(xml).find(uf).each(function(){
					var municipio = jq(this).attr('municipio');
					var cod_ibge = jq(this).attr('cod_ibge');
					var cod_siafi = jq(this).attr('cod_siafi');					
					jq('#municipio').append("<option value='"+cod_ibge+"'>"+municipio+"</option>");				
				});   
	         },
	error: function() {
               alert("Ocorreu um erro inesperado durante o processamento.");
           },	
	
	});

}

function carregaFichaPac(){
    var municipio = jq('#municipio').attr('value');
	if (municipio == "") {
		alert('Selecione o município');
		return false;
	}
	else {
		window.open('http://www.portalfederativo.gov.br/bin/view/Inicio/Municipio'+municipio, '_blank');
		return false;
	}
}

