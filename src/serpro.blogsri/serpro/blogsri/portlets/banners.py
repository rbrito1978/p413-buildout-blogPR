# -*- coding: utf-8 -*-

from plone.memoize.instance import memoize
from zope.interface import implements

from plone.app.portlets.portlets import base
from plone.portlets.interfaces import IPortletDataProvider

from zope import schema
from zope.formlib import form
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from serpro.blogsri import MessageFactory as _

from Products.CMFCore.utils import getToolByName

class IBanners(IPortletDataProvider):
    """A portlet

    It inherits from IPortletDataProvider because for this portlet, the
    data that is being rendered and the portlet assignment itself are the
    same.
    """

    # TODO: Add any zope.schema fields here to capture portlet configuration
    # information. Alternatively, if there are no settings, leave this as an
    # empty interface - see also notes around the add form and edit form
    # below.



    name = schema.TextLine(
            title=u"titulo",
            description=u"descricao",
            required=False)


class Assignment(base.Assignment):
    """Portlet assignment.

    This is what is actually managed through the portlets UI and associated
    with columns.
    """

    implements(IBanners)

    # TODO: Set default values for the configurable parameters here

    name = u""

    # TODO: Add keyword parameters for configurable parameters here
    # def __init__(self, some_field=u''):
    #    self.some_field = some_field

    def __init__(self):
        pass

    def title(self):
        return 'Banners'


class Renderer(base.Renderer):
    """Portlet renderer.

    This is registered in configure.zcml. The referenced page template is
    rendered, and the implicit variable 'view' will refer to an instance
    of this class. Other methods can be added and referenced in the template.
    """

    render = ViewPageTemplateFile('banners.pt')

  
    @property
    def available(self):
        return len(self._data())
    
    def getBanners(self):
        return self._data()

    def getName(self):
        return self.data.name or _(u"Banners")

    @memoize
    def _data(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        portal_path = getToolByName(self.context, 'portal_url').getPortalPath()
        return [{'Title':item.Title, 'getRemoteUrl':item.getRemoteUrl, 'getURL':item.getURL()} 
                 for item in catalog(portal_type='Banner',
                       path={'query':'%s/banners' % portal_path, 'depth':1},
                       review_state='published',
                       sort_on='getObjPositionInParent',
                       exclude_from_nav=False)]


# NOTE: If this portlet does not have any configurable parameters, you can
# inherit from NullAddForm and remove the form_fields variable.

class AddForm(base.AddForm):
    """Portlet add form.

    This is registered in configure.zcml. The form_fields variable tells
    zope.formlib which fields to display. The create() method actually
    constructs the assignment that is being added.
    """
    form_fields = form.Fields(IBanners)

    def create(self, data):
        return Assignment(**data)


# NOTE: IF this portlet does not have any configurable parameters, you can
# remove this class definition and delete the editview attribute from the
# <plone:portlet /> registration in configure.zcml

class EditForm(base.EditForm):
    """Portlet edit form.

    This is registered with configure.zcml. The form_fields variable tells
    zope.formlib which fields to display.
    """
    form_fields = form.Fields(IBanners)
