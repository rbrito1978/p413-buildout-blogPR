# -*- coding: utf-8 -*-

from plone.memoize.instance import memoize
from plone.memoize import ram
from plone.portlets.interfaces import IPortletDataProvider
from zope.formlib import form
from zope.interface import implements
from zope import schema
from plone.app.vocabularies.catalog import SearchableTextSourceBinder
from plone.app.form.widgets.uberselectionwidget import UberSelectionWidget

from Acquisition import aq_inner
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.portlets.portlets import base

from plone.app.portlets import PloneMessageFactory as _
from serpro.blogsri import sriMessageFactory

from plone.app.layout.navigation.root import getNavigationRoot
from zope.component import getMultiAdapter


class IAudioDia(IPortletDataProvider):

    root = schema.Choice(
            title=_(u"label_navigation_root_path", default=u"Root node"),
            description=sriMessageFactory(u'help_nav_root',
                                          default=u"You may search for and choose a folder "
                                                   "to act as the root of the search. "
                                                   "Leave blank to use the Plone site root."),
            required=False,
            source=SearchableTextSourceBinder({'is_folderish': True},
                                              default_query='path:'))


class Assignment(base.Assignment):
    implements(IAudioDia)
    
    root = None
    
    def __init__(self, root=None):
        self.root = root

    @property
    def title(self):
        return sriMessageFactory(u"audio_of_the_day")


class Renderer(base.Renderer):
    render = ViewPageTemplateFile('audio_dia.pt')

    @property
    def available(self):
        return len(self._data())
    
    def getAudio(self):
        return self._data()
    
    def getUrlMais(self):
        context = aq_inner(self.context)
        portal_state = getMultiAdapter((context, self.request),name=u'plone_portal_state')        
        root = self.data.root
        if root is not None:
            return '%s%s' % (portal_state.portal_url(),root)
        return portal_state.portal_url()
        

    @memoize
    def _data(self):
        context = aq_inner(self.context)
        catalog = getToolByName(context, 'portal_catalog')
        path = getNavigationRoot(context, relativeRoot=self.data.root)
        return [{'Title':item.Title,'getURL':item.getURL()}
                for item in catalog(portal_type=['Audio'],
                                    review_state='published',
                                    path=path,
                                    sort_on='effective',
                                    sort_order='reverse',
                                    sort_limit=1,
                                    exclude_from_nav=False)
                ]


class AddForm(base.AddForm):
    form_fields = form.Fields(IAudioDia)
    form_fields['root'].custom_widget = UberSelectionWidget
    label = sriMessageFactory(u"Add Audio of the day Portlet")
    description = sriMessageFactory(u"This portlet list the audio of the day.")

    def create(self, data):
        return Assignment(root=data.get('root',None))


class EditForm(base.EditForm):
    form_fields = form.Fields(IAudioDia)
    form_fields['root'].custom_widget = UberSelectionWidget
    label = sriMessageFactory(u"Edit Audio of the day Portlet")
    description = sriMessageFactory(u"This portlet list the audio of the day.")