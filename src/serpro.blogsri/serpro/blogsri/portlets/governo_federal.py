from plone.portlets.interfaces import IPortletDataProvider
from zope.component import getMultiAdapter
from zope.formlib import form
from zope.interface import implements
from zope import schema

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.portlets import PloneMessageFactory as _
from plone.app.portlets.portlets import base

from serpro.blogsri import sriMessageFactory


class IGovernoFederalPortlet(IPortletDataProvider):
    """ Portlet Governo Federal na sua cidade
    """


class Assignment(base.Assignment):
    implements(IGovernoFederalPortlet)

    def __init__(self):
        """"""

    @property
    def title(self):
        return sriMessageFactory(u"governo_federal")


class Renderer(base.Renderer):

    render = ViewPageTemplateFile('governo_federal.pt')

    def __init__(self, context, request, view, manager, data):
        base.Renderer.__init__(self, context, request, view, manager, data)

        portal_state = getMultiAdapter((context, request), name=u'plone_portal_state')
        self.navigation_root_url = portal_state.navigation_root_url()

    def file_xml(self):
        return '%s/++resource++serpro_blogsri_static/ibge.xml' % self.navigation_root_url


class AddForm(base.AddForm):
    form_fields = form.Fields(IGovernoFederalPortlet)
    label = _(u"Add Governo Federal Portlet")
    description = _(u"Governo Federal na sua cidade.")

    def create(self, data):
        return Assignment()


class EditForm(base.EditForm):
    form_fields = form.Fields(IGovernoFederalPortlet)
    label = _(u"Edit Governo Federal Portlet")
    description = _(u"Governo Federal na sua cidade.")