# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName
from zope.interface import Interface
from zope.component import getMultiAdapter
from serpro.curtir.config import BOOKMARKS
from DateTime import DateTime

class IVideosView(Interface):
    """View de audio"""
    
    def getVideos(self):
        """ retorna os videos publicados """

    def getBookmarks(self):
        """ retorna os bookmarks do serpro.curtir """


class VideosView(BrowserView):    
    
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.plone_portal_state = getMultiAdapter((self.context,self.request), name='plone_portal_state')
        self.plone_tools = getMultiAdapter((self.context,self.request), name='plone_tools') 

    def getVideos(self):
        """ retorna os videos publicados """
        catalog = self.plone_tools.catalog()
             
        lang = self.plone_portal_state.language()
        
        path = '/'.join(self.context.getPhysicalPath())
        
        query = {}
        query['portal_type']=['RTInternalVideo','RTRemoteVideo']
        query['review_state']='published'
        query['path']=path
        query['sort_on']='effective'
        query['sort_order']='reverse'
        query['exclude_from_nav']=False
        
        videos = []        
        for video in catalog(query):
            item = {}
            item['Title']=video.Title
            item['getURL']=video.getURL()
            item['Description']=video.Description
            item['hasImage']=video.hasImage
            item['getImageCaption']=video.Title
            if video.effective > DateTime('1900-01-01'):
                if lang == 'en':
                    item['effective']=video.effective.strftime('%A, %B %d, %Y at %H:%M')
                elif lang == 'es':
                    item['effective']=video.effective.strftime('%A, %d de %B %Y a las %H:%M')
                else:
                    item['effective']=video.effective.strftime('%A, %d de %B de %Y às %H:%M')
            else:
                item['effective']=video.effective
            videos.append(item)
        return videos
            
  
  
        
    def getBookmarks(self, video={}):
        """
        Retorna os bookmarks selecionados
        """ 
        if len(video.keys()) == 0:
            return []
                             
        portal_properties = self.plone_tools.properties()
        
        if not hasattr(portal_properties,'serpro_curtir_properties'):
            return []        
        
        portal_title = self.plone_portal_state.portal_title()
        portal_url = self.plone_portal_state.portal_url()
        
        object_title = video['Title']
        object_url = video['getURL']
        description = video['Description']
        
        
        bookmarks_id = portal_properties.serpro_curtir_properties.bookmarks_id
        
        bookmarks = []
        for b_id in bookmarks_id:
            if BOOKMARKS.has_key(b_id):
                bookmark = BOOKMARKS[b_id]
                bookmarks.append({'title':bookmark['title'],
                                  'url':bookmark['url'].replace('{url}',object_url)\
                                                       .replace('{title}',object_title)\
                                                       .replace('{portal}',portal_title)\
                                                       .replace('{description}',description),
                                  'icon':'%s/%s' %(portal_url,bookmark['icon'])
                                  })   
        return bookmarks