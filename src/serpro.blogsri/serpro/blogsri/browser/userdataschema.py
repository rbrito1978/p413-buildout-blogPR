# -*- coding: utf-8 -*-
# usado como no examplo em collective.example.userdata
# http://plone.org/products/collective.examples.userdata
from zope import schema
from zope.schema import ValidationError
from zope.interface import implements

from Products.CMFPlone import PloneMessageFactory as _
from Products.CMFDefault.formlib.schema import FileUpload

from plone.app.users.userdataschema import checkEmailAddress

from plone.app.users.userdataschema import IUserDataSchemaProvider
from plone.app.users.userdataschema import IUserDataSchema

from serpro.blogsri import sriMessageFactory


class UserDataSchemaProvider(object):
    implements(IUserDataSchemaProvider)

    def getSchema(self):
        """
        """
        return IEnhancedUserDataSchema


class InvalidMimeType(ValidationError):
        __doc__ = sriMessageFactory('message_invalid_file_type',u"Sorry, invalid file type. Use jpg, jpeg, png or gif.")
        
def valida_portrait(portrait):
    if portrait:
        for ftype in ['image/jpg','image/jpeg', 'image/gif', 'image/png']:
            if portrait.headers['content-type'].find(ftype) != -1:
                return True
        raise InvalidMimeType(portrait)
        return False

class IEnhancedUserDataSchema(IUserDataSchema):
    """ Usa todos os campos do user data schema padrao, e adiciona validacao para
    o campo portrait.
    """
    fullname = schema.TextLine(
        title=_(u'label_full_name', default=u'Full Name'),
        description=_(u'help_full_name_creation',
                      default=u"Enter full name, e.g. John Smith."),
        required=False)

    email = schema.ASCIILine(
        title=_(u'label_email', default=u'E-mail'),
        description=u'',
        required=True,
        constraint=checkEmailAddress)

    home_page = schema.TextLine(
        title=_(u'label_homepage', default=u'Home page'),
        description=_(u'help_homepage',
                      default=u"The URL for your external home page, "
                      "if you have one."),
        required=False)

    description = schema.Text(
        title=_(u'label_biography', default=u'Biography'),
        description=_(u'help_biography',
                      default=u"A short overview of who you are and what you "
                      "do. Will be displayed on your author page, linked "
                      "from the items you create."),
        required=False)

    location = schema.TextLine(
        title=_(u'label_location', default=u'Location'),
        description=_(u'help_location',
                      default=u"Your location - either city and "
                      "country - or in a company setting, where "
                      "your office is located."),
        required=False)

    portrait = FileUpload(title=_(u'label_portrait', default=u'Portrait'),
        description=_(u'help_portrait',
                        default=u''
                      ),
        constraint=valida_portrait,
        required=False)

    pdelete = schema.Bool(
        title=_(u'label_delete_portrait', default=u'Delete Portrait'),
        description=u'',
        required=False)
    