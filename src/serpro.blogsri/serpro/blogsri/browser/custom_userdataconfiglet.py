# -*- coding: utf-8 -*-
from zope.formlib import form
from zope.event import notify

from Products.CMFPlone import PloneMessageFactory as _
from Products.statusmessages.interfaces import IStatusMessage

from plone.app.controlpanel.events import ConfigurationChangedEvent
from plone.protect import CheckAuthenticator
from plone.app.form.validators import null_validator
from plone.app.users.browser.personalpreferences import UserDataConfiglet

class CustomUserDataConfiglet(UserDataConfiglet):
    """
    Esta classe substitui a plone.app.users.browser.UserDataConfiglet, 
    modificando apenas o handle_cancel_action, para corrigir um bug que ocorre ao 
    cancelar uma edicao de usuario via @@user-information. 
    """
    @form.action(_(u'label_save', default=u'Save'), name=u'save')
    def handle_edit_action(self, action, data):
        CheckAuthenticator(self.request)

        if form.applyChanges(self.context, self.form_fields, data,
                             self.adapters):
            IStatusMessage(self.request).addStatusMessage(_("Changes saved."),
                                                          type="info")
            notify(ConfigurationChangedEvent(self, data))
            self._on_save(data)
        else:
            IStatusMessage(self.request).addStatusMessage(_("No changes made."),
                                                          type="info")

    @form.action(_(u'label_cancel', default=u'Cancel'),
                 validator=null_validator,
                 name=u'cancel')
    def handle_cancel_action(self, action, data):
        IStatusMessage(self.request).addStatusMessage(_("Changes canceled."),
                                                      type="info")
        # a seguir, a simples correcao: adicinar '?userid='+self.userid na string da url
        # a redirecionar o usr quando do clique no botao cancelar.
        self.request.response.redirect(self.request['ACTUAL_URL']+'?userid='+self.userid)
        return ''
