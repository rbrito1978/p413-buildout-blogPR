# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName
from zope.interface import Interface
from zope.component import getMultiAdapter
from serpro.curtir.config import BOOKMARKS
from DateTime import DateTime

class IAudioView(Interface):
    """View de audio"""
    
    def getAudios(self):
        """ retorna os audios publicados """

    def getBookmarks(self):
        """ retorna os bookmarks do serpro.curtir """


class AudioView(BrowserView):    
        
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def getAudios(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        query = dict()
        query['portal_type'] = 'Audio'
        query['review_state'] = 'published'
        query['sort_on']='effective'
        query['sort_order']='descending'
        return [{'Title':item.Title, 
                 'Description':item.Description, 
                 'getURL':item.getURL()} for item in catalog(query)]
        
    def getBookmarks(self, audio={}):
        """
        Retorna os bookmarks selecionados
        """ 
        if len(audio.keys()) == 0:
            return []
        
        context = self.context
        request = self.request        
        plone_tools = getMultiAdapter((context,request), name='plone_tools')               
        portal_properties = plone_tools.properties()        
        if not hasattr(portal_properties,'serpro_curtir_properties'):
            return []
        
        portal_state = getMultiAdapter((context,request), name='plone_portal_state') 
        context_state = getMultiAdapter((context,request), name='plone_context_state')
        
        portal_title = portal_state.portal_title()
        portal_url = portal_state.portal_url()
        
        object_title = audio['Title']
        object_url = audio['getURL']
        description = audio['Description']
        
        bookmarks_id = portal_properties.serpro_curtir_properties.bookmarks_id
        
        bookmarks = []
        for b_id in bookmarks_id:
            if BOOKMARKS.has_key(b_id):
                bookmark = BOOKMARKS[b_id]
                bookmarks.append({'title':bookmark['title'],
                                  'url':bookmark['url'].replace('{url}',object_url)\
                                                       .replace('{title}',object_title)\
                                                       .replace('{portal}',portal_title)\
                                                       .replace('{description}',description),
                                  'icon':'%s/%s' %(portal_url,bookmark['icon'])
                                  })   
        return bookmarks