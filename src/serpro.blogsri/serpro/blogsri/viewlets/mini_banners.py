from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName
from plone.app.layout.viewlets.common import ViewletBase

class MiniBannersViewlet(ViewletBase):
    render = ViewPageTemplateFile('mini_banners.pt')

    def update(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        portal_url = getToolByName(self.context, 'portal_url')
        portalPath = portal_url.getPortalPath()
        self.mini_banners = catalog(portal_type="Banner",
				    path={'query':portalPath+'/banners_sociais', 'depth':1},)
        
    	
