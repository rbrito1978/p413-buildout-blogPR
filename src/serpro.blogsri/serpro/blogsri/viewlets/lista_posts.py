# -*- coding: utf-8 -*-

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName
from zope.component import getMultiAdapter
from plone.app.layout.viewlets.common import ViewletBase
from serpro.curtir.config import BOOKMARKS
from DateTime import DateTime

class ListaPostsViewlet(ViewletBase):
    render = ViewPageTemplateFile('lista_posts.pt')

    def getPosts(self):
        context = self.context
        request = self.request
        
        plone_portal_state = getMultiAdapter((context,request), name='plone_portal_state') 
        lang = plone_portal_state.language()
        
        if context.portal_type == 'Topic':
            queryMethod = context.queryCatalog
        else:
            queryMethod = context.getFolderContents
        query = {}
        query['review_state'] = 'published'
        query['sort_on'] = 'effective'
        query['sort_order'] = 'reverse'
        
        posts = []        
        for post in queryMethod(query,full_objects=False):
            item = {}
            item['portal_type']=post.portal_type
            item['Title']=post.Title
            item['getURL']=post.getURL()
            item['Description']=post.Description
            item['hasImage']=post.hasImage and post.portal_type != 'File'
            item['getImageCaption']=post.getImageCaption or post.Title
            if post.effective > DateTime('1900-01-01'):
                if lang == 'en':
                    item['effective']=post.effective.strftime('%A, %B %d, %Y at %H:%M')
                elif lang == 'es':
                    item['effective']=post.effective.strftime('%A, %d de %B %Y a las %H:%M')
                else:
                    item['effective']=post.effective.strftime('%A, %d de %B de %Y às %H:%M')
            else:
                item['effective']=post.effective
            posts.append(item)
        return posts


    def getBookmarks(self, post={}):
        """
        Retorna os bookmarks selecionados
        """ 
        if len(post.keys()) == 0:
            return []
        
        context = self.context
        request = self.request
        
        plone_tools = getMultiAdapter((context,request), name='plone_tools')  
             
        portal_properties = plone_tools.properties()
        
        if not hasattr(portal_properties,'serpro_curtir_properties'):
            return []
        
        portal_state = getMultiAdapter((context,request), name='plone_portal_state')
        
        portal_title = portal_state.portal_title()
        portal_url = portal_state.portal_url()
        
        object_title = post['Title']
        object_url = post['getURL']
        description = post['Description']
        
        
        bookmarks_id = portal_properties.serpro_curtir_properties.bookmarks_id
        
        bookmarks = []
        for b_id in bookmarks_id:
            if BOOKMARKS.has_key(b_id):
                bookmark = BOOKMARKS[b_id]
                bookmarks.append({'title':bookmark['title'],
                                  'url':bookmark['url'].replace('{url}',object_url)\
                                                       .replace('{title}',object_title)\
                                                       .replace('{portal}',portal_title)\
                                                       .replace('{description}',description),
                                  'icon':'%s/%s' %(portal_url,bookmark['icon'])
                                  })   
        return bookmarks