# -*- coding: utf-8 -*-

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.layout.viewlets import content


class DocumentActionsViewlet(content.DocumentActionsViewlet):

    index = ViewPageTemplateFile("document_actions.pt")
