# -*- coding: utf-8 -*-

"""Common configuration constants
"""

PROJECTNAME = 'serpro.blogsri'


ADD_PERMISSIONS = {
    # -*- extra stuff goes here -*-
    'Audio': 'serpro.blogsri: Add Audio',
}
# Configurações serpro.curtir.
CURTIR_PROPERTY_SHEET = 'serpro_curtir_properties'
CURTIR_BOOKMARKS_IDS = ['facebook', 'twitter']
CURTIR_PORTAL_TYPES_IDS = ['Audio','News Item','Document','Event','Image',\
                           'File','Link','RTRemoteVideo','RTInternalVideo']

# Configurações serpro.maisacessoados
MAISACESSADOS_PROPERTY_SHEET = 'serpro_maisacessados_properties'
MAISACESSADOS_PORTAL_TYPES_ID = ['Folder','Topic','Audio','News Item','Document','Event','Image',\
                           'File','Link','RTRemoteVideo','RTInternalVideo','Banner','PlonePopoll']
MAISACESSADOS_REJECT_IDS = ['blog','posts']


#Validação
 
# Indica se os arquivos dentro de um "ZIP" devem ser validados também seguindo
# as mesmas regras.
ZIP_INTERNAL_VALIDATION = True

# Validações de extensões. Arquivos do tipo zip, só devem conter esses tipos
# dentro deles.
IMAGE_EXTENSIONS = [{'extension': 'jpeg', 'mime-type':'image/jpeg'},
                    {'extension': 'jpg', 'mime-type':'image/jpeg'},
                    {'extension': 'gif', 'mime-type':'image/gif'},
                    {'extension': 'png', 'mime-type':'image/png'}]

VIDEO_EXTENSIONS = [{'extension': 'flv', 'mime-type': 'video/x-flv'}]


DOCUMENT_EXTENSIONS = [{'extension': 'rtf', 'mime-type': 'text/rtf'},
                       {'extension': 'pdf', 'mime-type': 'application/pdf'},
                       {'extension': 'doc', 'mime-type': 'application/msword'},
                       {'extension': 'docx', 'mime-type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'},
                       {'extension': 'odt', 'mime-type': 'application/vnd.oasis.opendocument.text'},
                       {'extension': 'sxw', 'mime-type': 'application/vnd.sun.xml.writer'},
                       {'extension': 'ods', 'mime-type': 'application/vnd.oasis.opendocument.spreadsheet'},
                       {'extension': 'xls', 'mime-type': 'application/vnd.ms-excel'},
                       {'extension': 'xlsx', 'mime-type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
                       {'extension': 'ppt', 'mime-type': 'application/vnd.ms-powerpoint'},
                       {'extension': 'pptx', 'mime-type': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'},
                       {'extension': 'pps', 'mime-type': 'application/vnd.ms-powerpoint'},
                       {'extension': 'ppsx', 'mime-type': 'application/vnd.openxmlformats-officedocument.presentationml.slideshow'}]

AUDIO_EXTENSIONS = [{'extension': 'mp3',
                     'mime-type': ['audio/mpeg', 'audio/x-mpeg', 'audio/mp3',
                     'audio/x-mp3', 'audio/mpeg3', 'audio/x-mpeg3',
                     'audio/mpg', 'audio/x-mpg', 'audio/x-mpegaudio']}]

FILE_EXTENSIONS = [{ 'extension': 'zip',
                     'mime-type': ['application/x-compressed',
                                   'application/x-zip-compressed',
                                   'application/zip',
                                   'application/octet-stream']}]

EXTENSIONS = IMAGE_EXTENSIONS + DOCUMENT_EXTENSIONS + VIDEO_EXTENSIONS + \
    AUDIO_EXTENSIONS + FILE_EXTENSIONS