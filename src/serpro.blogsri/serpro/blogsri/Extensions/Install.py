# -*- coding: utf-8 -*-
#
# File: Install.py
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import log 
from zope.component import getUtility
from plone.app.users.userdataschema import IUserDataSchemaProvider
import transaction

def beforeUninstall(portal, reinstall=False, product=None, cascade=[]):
    """ Prevent created content from being deleted on uninstall
    """
    if 'portalobjects' in cascade:
        cascade.remove('portalobjects')

    return True, cascade



def uninstall(portal, reinstall=False):
    # Remove utilities when really uninstalled
    if not reinstall:
        my_utility = getUtility(IUserDataSchemaProvider)
        portal.getSiteManager().unregisterUtility(my_utility, IUserDataSchemaProvider)
        del my_utility
        transaction.commit()
        log("Uninstalled utility")
    
    return "Ran all uninstall steps."