# -*- coding: utf-8 -*-
from zope import schema
from zope.formlib import form
from zope.interface import Interface
from zope.schema import ValidationError

from Products.ATContentTypes.content.base import ATCTMixin
from Products.CMFPlone import PloneMessageFactory as _
from Products.CMFDefault.formlib.schema import FileUpload
from Products.statusmessages.interfaces import IStatusMessage

# incluindo a validacao anti preenchimento apenas com espacos
# no titulo
def validate_title(self, value):
    if value and not value.strip(' '):
        return  u'Título é obrigatório. Favor corrigir.'
ATCTMixin.validate_title = validate_title

