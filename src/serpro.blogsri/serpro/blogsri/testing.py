from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting
from plone.app.testing import applyProfile

from zope.configuration import xmlconfig

class SerproBlogsri(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE, )

    def setUpZope(self, app, configurationContext):
        # Load ZCML for this package
        import serpro.blogsri
        xmlconfig.file('configure.zcml',
                       serpro.blogsri,
                       context=configurationContext)


    def setUpPloneSite(self, portal):
        applyProfile(portal, 'serpro.blogsri:default')

SERPRO_BLOGSRI_FIXTURE = SerproBlogsri()
SERPRO_BLOGSRI_INTEGRATION_TESTING = \
    IntegrationTesting(bases=(SERPRO_BLOGSRI_FIXTURE, ),
                       name="SerproBlogsri:Integration")