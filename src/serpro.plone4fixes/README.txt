.. contents::

Introdução
==========
Este módulo tem o objetivo de conter pequenos "fixes" para o Plone 4, seja na
forma de "patch" ou de implementação direta utilizando os padrões Plone.

Os fixes podem ser de natureza da corporação (o Serpro segue algum padrão
diferente do Plone, e tem de aplicar isso em todos os projetos), decisões de
acessibilidade/usabilidade decididas pelo setor responsável na empresa que não
condizem com a solução dada pelo Plone, ou mesmo fixes que devido ao ciclo de
vida de software na empresa, demorariam para serem aplicados ou só seriam
disponibilizados em novos projetos.

Esse módulo é específico para Plone 4. Se desejar utilizar em Plone 3, utilize
serpro.plone3fixes.

Para ver uma lista de todos os fixes criados, favor verificar o arquivo
CHANGES.txt.
