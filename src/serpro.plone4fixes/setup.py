from setuptools import setup, find_packages

version = '0.10.2'

long_description = (
    open('README.txt').read()
    + '\n' +
    'Contributors\n'
    '============\n'
    + '\n' +
    open('CONTRIBUTORS.txt').read()
    + '\n' +
    open('CHANGES.txt').read()
    + '\n')

setup(name='serpro.plone4fixes',
      version=version,
      description="""Contem pequenos 'fixes' para Plone 4, modificacoes
          necessarias de acordo com o Serpro mas que geralmente nao vao pro
          'core' do Plone.""",
      long_description=long_description,
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Development Status :: 3 - Alpha",
        "Framework :: Plone :: 4.0",
        "Framework :: Plone :: 4.1",
        "License :: Other/Proprietary License",
        "Natural Language :: Portuguese (Brazilian)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Giovanni Monteiro Calanzani',
      author_email='giovanni.calanzani@serpro.gov.br',
      url='https://www.serpro.gov.br/',
      license='other',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      namespace_packages=['serpro'],
      include_package_data=True,
      zip_safe=False,
      paster_plugins=["ZopeSkel"],
      install_requires=[
          'setuptools',
          'collective.monkeypatcher',
          # -*- Extra requirements: -*-
      ],
      extras_require={'test': ['plone.app.testing']},
      entry_points="""
      # -*- Entry points: -*-
  	  [z3c.autoinclude.plugin]
  	  target = plone
      """,
      )
