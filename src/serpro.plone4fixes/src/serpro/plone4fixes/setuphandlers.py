# -*- coding: utf-8 -*-

from Products.CMFCore.utils import getToolByName

from serpro.plone4fixes.config import PROPERTIES, PROPERTY_SHEET_NAME,\
    PROPERTY_SHEET_TITLE


def postInstall(context):
    setPortalProperties(context, PROPERTY_SHEET_NAME, PROPERTIES,
        PROPERTY_SHEET_TITLE)

def setPortalProperties(context, property_sheet_name, properties,
    property_sheet_title=''):
    """Cria uma property sheet ou apenas uma propriedade na tool
    portal_properties. Se aquela propriedade já existir, não faz nada.

    @properties: [{'name':'teste', 'value': True, 'type': 'boolean'},
                  {'name':'teste2', 'value':' novo', 'type': 'string'}]"""

    portal_properties = getToolByName(context, 'portal_properties')

    if getattr(portal_properties, property_sheet_name, None) is None:
        portal_properties.addPropertySheet(property_sheet_name,
            property_sheet_title)

        property_sheet = getattr(portal_properties, property_sheet_name)

        for property in properties:
            if not property_sheet.hasProperty(property['name']):
                property_sheet._setProperty(property['name'],
                    property['value'], property['type'])
