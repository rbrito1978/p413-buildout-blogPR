# -*- coding: utf-8 -*-

from plone.indexer.decorator import indexer

from zope.interface import Interface


@indexer(Interface)
def hasImage(obj):
    """
    Cria um indice com valor False ou True indicando se o usuario incluiu imagem
    """
    result = False
    if hasattr(obj,'getImage'):
        type = obj.getImage().__class__.__name__
        if type != 'str':
            result = True
    return result