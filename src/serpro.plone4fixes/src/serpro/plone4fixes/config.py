# -*- coding: utf-8 -*-

# O ip 10.200.140.115 é do ambiente de teste. Foi adicionano aqui para que
# a viewlet personal bar aparecer no ambiente de teste.
DEVELOPMENT_HOSTNAMES = ['localhost', '127.0.0.1', '10.200.140.115']
HOSTNAMES_PROPERTY_ID = 'hostnames'
PROPERTIES = [{'name': HOSTNAMES_PROPERTY_ID, 'value': [], 'type': 'lines'}]
PROPERTY_SHEET_NAME = 'serpro_plone4fixes_properties'
PROPERTY_SHEET_TITLE = u'Propriedades de serpro.plone4fixes'
