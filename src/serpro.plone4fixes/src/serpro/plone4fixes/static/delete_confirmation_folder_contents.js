/*
 * delete_confirmation_folder_contents
 * 
 * Copyright (c) 2012 Serpro
 * Esse javascript tem como função 'simular' um modal que ocorre normalmente
 * com os overlays de plone.app.jquerytools. A estrutura do mesmo não foi
 * utilizada (ou seja, o framework de popups não pôde ser utilizado, e esse
 * javascript cria, manualmente, as divs modal e de overlay), pois todos os  
 * overlays do framework esperam um iframe, um image ou uma requisição ajax.
 * Nesse caso em específico procurei apenas manter o padrão visual, do
 * contrário teria sido necessário usar um alert tradicional de browser fugindo
 * assim da identidade visual desse tipo de popup.
 *
 */

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,c,b){$.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "),function(d){a(d)});a("focusin","focus"+b);a("focusout","blur"+b);$.addOutsideEvent=a;function a(g,e){e=e||g+b;var d=$(),h=g+"."+e+"-special-event";$.event.special[e]={setup:function(){d=d.add(this);if(d.length===1){$(c).bind(h,f)}},teardown:function(){d=d.not(this);if(d.length===0){$(c).unbind(h)}},add:function(i){var j=i.handler;i.handler=function(l,k){l.target=k;j.apply(this,arguments)}}};function f(i){$(d).each(function(){var j=$(this);if(this!==i.target&&!j.has(i.target).length){j.triggerHandler(e,[i.target])}})}}})(jQuery,document,"outside");

//Funcionalidade adaptada dos overlays para alerta na visão de folder_contents.
(function($) {
    $(document).ready(function(){

        var DIV_OVERLAY_ID = 'exposeMask';
        var DIV_POPUP_ID = 'pb_1';

        function plone4fixes_exposeMask(){
            //Retorna a div de overlay que fica por debaixo do modal.
            return '<div id="' + DIV_OVERLAY_ID + '" style="position: absolute; top: 0px; left: 0px; width: 1585px; height: 1042px; z-index: 9998; background-color: rgb(255, 255, 255); display: block; opacity: 0.4; "></div>';
        }

        function plone4fixes_get_objects_ids_to_delete(selected_paths){
            //Retorna os ids dos objetos que serão deletados, já para serem 
            //com html no popup de aviso.
            var ids = [];
            var lis = '';
            for (var i = 0; i < $(selected_paths).length; i++) {
                lis+='<li>';
                lis+=$("label[for=" + selected_paths[i].id + "]").text();
                lis+='</li>';
            };
            return ['<ul',
                lis,
            '</ul>'].join('\n');
        }

        function plone4fixes_pb1(top_index, left_index, selected_paths){
            //Retorna o modal com as opções de excluir ou cancelar e que mostra
            //os ids que serão removidos.
            return ['<div id="' + DIV_POPUP_ID + '" class="overlay overlay-ajax " style="width: 50%; z-index: 9999; top: ' + top_index +  'px; left: ' + left_index + 'px; position: absolute; display: block; ">',
                    '<div class="close">',
                        '<span>Close</span>',
                    '</div>',
                    '<div class="pb-ajax">',
                        '<div>',
                            '<dl class="portalMessage info" id="kssPortalMessage" style="display:none">',
                                '<dt>Info</dt>',
                                '<dd></dd>',
                            '</dl>',
                            '<h1 class="documentFirstHeading">Você realmente quer apagar este(s) item(s)?</h1>',
                            '<div id="content-core">',
                                plone4fixes_get_objects_ids_to_delete(selected_paths),
                                '<div class="formControlsDelete">',
                                   '<input class="destructive" type="submit" value="Excluir">',
                                   '<input class="standalone" type="submit" value="Cancelar" name="form.button.Cancel">',
                                '</div>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</div>'].join('\n');
        }

        function plone4fixes_killmodal(){
            //Deleta as janelas de overlay e modal retornando a tela ao estado
            //inicial.
            $("#" + DIV_OVERLAY_ID).remove();
            $("#" + DIV_POPUP_ID).remove();
        }

        function plone4fixes_createhiddeninput(input, form){
            //XXX: Um input hidden com o nome do input "Excluir" é gerado para
            //poder executar a ação de exclusão no submit do formulário. Isso
            //foi necessário porque não encontrei uma forma melhor de evitar a
            //propagação do evento do formulário usando event.preventDefault
            //mas ao mesmo tempo voltando e simulando um novo clique no botão
            //'Excluir'.
            $('<input>').attr({
                type: 'hidden',
                id: 'xxx_delete',
                name: $(input).attr("name"),
                value: $(input).attr("value")
            }).appendTo($(form));
        }

        //Está no formulário do folder_contents, portanto pode seguir
        //com a implementação do modal. No layout padrão, geralmente
        //esse overlay gerado pelo plone.app.jquerytools é gerado
        //depois de kss-spinner.
        var folderContentsForm = $("form[name=folderContentsForm]");
        if($(folderContentsForm) != undefined){

            var botao_excluir = $("input[name=folder_delete:method]");
            $(botao_excluir).click(function(event){
                
                //Verifica se marcou algum item. Se não tiver marcado, deixa
                //a validação natural da view seguir seu curso.
                var selected_paths = $('input[name=paths:list]:checked');
                if($(selected_paths).length == 0){
                    return true;
                }
                else{

                    //Evita o submit normal do formulário.
                    event.preventDefault();

                    //Pega as coordenadas de onde deve mostrar o overlay e o
                    //modal.
                    var w = $(window);
                    var top_index = Math.floor($(this).offset().top + w.scrollTop())/2;
                    var left_index = Math.floor($(this).offset().left + w.scrollLeft())/2;

                    //Mostra o overlay, depois da div 'kss-spinner'.
                    $("#kss-spinner").after(plone4fixes_exposeMask()+plone4fixes_pb1(top_index, left_index, selected_paths))

                    //Remove a classe submitting, para evitar a validação de  
                    //formsubmithelpers.js falando que já foi feito submit no formulário.
                    jq(this).attr("class", "context");

                    //Aceitou excluir
                    $("div.formControlsDelete input.destructive").click(function(){
                        plone4fixes_killmodal();
                        //Segue o processo normal de submit. Leia a
                        //documentação de createhiddeninput para poder entender
                        //o motivo da criação desse input hidden.
                        plone4fixes_createhiddeninput(botao_excluir, folderContentsForm);
                        $(folderContentsForm).trigger('submit');
                    });

                    //Cancelou
                    $("div.formControlsDelete input[name=form.button.Cancel], #" + DIV_POPUP_ID  + " div.close").click(function(){
                        plone4fixes_killmodal();
                    });

                    // Esconder o modal se clicar fora dele. Utiliza o plugin
                    //jQuery outside events, no topo desse arquivo.
                    $("#" + DIV_POPUP_ID).bind( "clickoutside", function(event){
                        plone4fixes_killmodal();
                    });
                }
                return false;
            });
        }
    });
})(jQuery);
