# -*- coding: utf-8 -*-

from AccessControl import getSecurityManager

import logging
logger = logging.getLogger('serpro.plone4fixes: personal_bar')

from urlparse import urlparse

from Products.CMFCore.permissions import ManagePortal

from Products.CMFCore.utils import getToolByName

from plone.app.layout.viewlets.common import PersonalBarViewlet as\
    OriginalPersonalBarViewlet

from serpro.plone4fixes.config import DEVELOPMENT_HOSTNAMES,\
    HOSTNAMES_PROPERTY_ID, PROPERTY_SHEET_NAME


def updateHostnames(context, urls):
    """Atualiza a property de hostnames que a viewlet personal_bar verifica
    para ser renderizada."""
    portal_properties = getToolByName(context, 'portal_properties')
    sheet = getattr(portal_properties, PROPERTY_SHEET_NAME, None)
    if sheet is not None:
        hostnames = getattr(sheet, HOSTNAMES_PROPERTY_ID, None)
        if hostnames is not None:
            hostnames = list(hostnames)
            for url in urls:
                if url not in hostnames:
                    hostnames.append(url)
            sheet._updateProperty(HOSTNAMES_PROPERTY_ID, hostnames)


class PersonalBarViewlet(OriginalPersonalBarViewlet):
    """Sobrescreve a PersonalBarViewlet original de plone.app.layout, para
    poder customizar o método 'render' e só renderizá-la se estiver num
    hostname de gestão previamente cadastrado."""

    def _get_hostnames(self):
        """Retorna uma lista de hostnames válidos para renderizar a viewlet."""
        portal_properties = getToolByName(self.context, 'portal_properties')
        hostnames_properties = getattr(portal_properties, PROPERTY_SHEET_NAME)
        h = list(hostnames_properties.getProperty(HOSTNAMES_PROPERTY_ID, None))
        if h is not None:
            hostnames = []
            h.extend(DEVELOPMENT_HOSTNAMES)
            for hostname in h:
                hostnames.append(self._get_hostname_from_url(hostname).strip())
            return hostnames
        else:
            return DEVELOPMENT_HOSTNAMES

    def _get_hostname_from_url(self, hostname):
        """Retorna um hostname 'limpo', seja do request ou da lista de
        hostnames válidos."""
        hostname_obj = urlparse(hostname)

        # Se netloc e scheme é vazio, pegue o path, pois pode ter colocado sem
        # http, etc.
        if not hostname_obj.netloc and not hostname_obj.scheme:
            host = hostname_obj.path
        else:
            host = hostname_obj.netloc

        # Remove porta, se houver.
        return host.split(":")[0]

    def render(self):
        """Renderiza a viewlet apenas se o hostname do site estiver cadastrado
        na property de hostnames ou se for em ambiente local (desenvolvimento).

        Essa customização iniciou-se através de plone.app.layout-2.0.11.
        """
        try:
            # Se for Manager, mostra de qualquer jeito a viewlet.
            if getSecurityManager().checkPermission(ManagePortal,
                self.context):
                return super(OriginalPersonalBarViewlet, self).render()
            if self.request:
                actual_url = self.request.get('ACTUAL_URL', None)
                hostname = self._get_hostname_from_url(actual_url)
                if hostname in self._get_hostnames():
                    return super(OriginalPersonalBarViewlet, self).render()
            return ""
        except:
            # XXX: Tratamentos de exceções dessa forma devem ser proibidos, mas
            # infelizmente se ocorre um erro durante a renderização da viewlet,
            # o site inteiro sai do ar. Não encontrei outra forma de evitar
            # isso a não ser com esse try:except.
            msg = """A random error happened. This log is done so it avoids a
                complete site error (since when viewlets get an error when
                rendering instead of a local problem like portlets)."""
            logger.debug(msg)
            return ""
