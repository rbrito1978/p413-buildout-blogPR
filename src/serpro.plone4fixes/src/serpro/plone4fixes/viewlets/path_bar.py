# -*- coding: utf-8 -*-

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.layout.viewlets import common

from Products.CMFPlone import PloneMessageFactory as _


class PathBarViewlet(common.PathBarViewlet):
    """Breadcrumb customizado.
    """
    render = ViewPageTemplateFile('path_bar.pt')

    def isPageTemplate(self):
        """Retorna se está numa tela que é template e não um objeto.

        Como a funcionalidade de 'search' é uma template pt, não é possível
        atribuir a interface IHideFromBreadcrumbs à mesma: assim, preciso
        verificar, no request, se está vindo dessas telas. 'sitemap' e
        'accessibility-info' entram no mesmo problema.

        Para saber se vem de alguma dessas telas, a verificação é feita pela
        variável PATH_INFO, que vem da seguinte forma do request:

	'/clientes/projeto/search_form'
        """
        try:
            if self.request:
                path_info = self.request.get('PATH_INFO', None)
                if path_info is not None:
                    search_sufix = path_info.split("/")[-1]
                    # XXX: Pergunta primeiro pelo search_form e não pelo search,
                    # senão seria sempre True.
                    if 'search_form' in search_sufix:
                        return _(u'heading_advanced_search')
                    if 'search' in search_sufix:
                        return _(u'Search')
                    if 'sitemap' in search_sufix:
                        return _(u'label_site_map')
                    if 'accessibility-info' in search_sufix:
                        return _(u'heading_accessibility_info')
            return ''
        except:
            return ''
