from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting
from plone.app.testing import applyProfile

from zope.configuration import xmlconfig

class SerproPlone4Fixes(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE, )

    def setUpZope(self, app, configurationContext):
        # Load ZCML for this package
        import serpro.plone4fixes
        xmlconfig.file('configure.zcml',
                       serpro.plone4fixes,
                       context=configurationContext)


    def setUpPloneSite(self, portal):
        applyProfile(portal, 'serpro.plone4fixes:default')

SERPRO_PLONE4FIXES_FIXTURE = SerproPlone4Fixes()
SERPRO_PLONE4FIXES_INTEGRATION_TESTING = \
    IntegrationTesting(bases=(SERPRO_PLONE4FIXES_FIXTURE, ),
                       name="SerproPlone4Fixes:Integration")