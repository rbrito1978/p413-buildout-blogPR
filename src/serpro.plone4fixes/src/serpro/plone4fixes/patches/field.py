# -*- coding: utf-8 -*-

def patched_validate_required(self, instance, value, errors):
    """Patch para validate_required: valida se o valor fornecido não são apenas
    espaços em branco: basicamente, adicionei 'strip()' no valor que ele recebe
    para validar, e aí chama o método original pra fazer o resto."""

    try:
        value = value.strip()
    except AttributeError:
        # Outros tipos vêm no value, portanto o 'strip' só é válido se for do
        # tipo string.
        pass

    # Veja 'preserveOriginal' em collective.monkeypatcher para entender de onde
    # vêm '_old_' em self.
    return self._old_validate_required(instance, value, errors)
